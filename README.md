# Fitmaker

`fitmaker` is a python module for statistical inference on physics beyond the Standard Model (SM).
It contains a database of high energy physics measurements and a fitting framework that quantifies the comopatibility of a dataset with parameters of scenarios beyond the SM.  
The current version focuses on fitting the Wilson coefficients of the Standard Model Effective Field Theory, and was used to produce the results of:

J. Ellis, M. Madigan, K. Mimasu, V. Sanz, T. You;  
*"Top, Higgs, Diboson and Electroweak Fit to the Standard Model Effective Field Theory"*  
[arXiv:2012.02779](https://arxiv.org/abs/2012.02779)  

The observable database collects measurements Electroweak precision tests and W<sup>+</sup>W<sup>-</sup> production at LEP, and top, Higgs and Electroweak measurements from Tevatron and the LHC.  

## Requirements
* python 3.X
* numpy
* scipy
* pandas

**optional**  
For plotting functions:  
* matplotlib
* contourpy
For running notebooks:  
* jupyterlab
* notebook
* seaborn

For the nested sampling:
* pymultinest

## Installation
From the directory containing these `setup.py` and `setup.cfg`, run 

`pip install -e .`

to install fitmaker and its requirements including the optional ones listed above (those listed in setup.py).

Then you should be able to use the fitmaker package from any location without specifying the path to fitmaker.  This is a basic test script you can run to make sure it has been installed.

```
from fitmaker.theories.SMEFT_fit_full import SMEFT
print(SMEFT.external_parameters)
```
