import setuptools

setuptools.setup(
    name="fitmaker",
    version="0.0.1",
    author="E. Bagnaschi, J. Ellis, M. Madigan, Victor Maura Breick, K. Mimasu, Alexandre Salas-Bernardez, V. Sanz, T. You",
    author_email="",
    description = "Module for statistical inference on physics beyond the Standard Model",
    long_description = "file: README.md",
    long_description_content_type="text/markdown",
    classifiers=[
        "Programming Language :: Python :: 3",
        "Topic :: Scientific/Engineering",
        "Topic :: Scientific/Engineering :: Physics",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
    ],
    url="https://kenmimasu.gitlab.io/fitrepo",
    package_dir={"": "."},
    packages=setuptools.find_packages(where="."),
    install_requires=[
        "matplotlib",
        "numpy",
        "scipy",
        "pandas",
        "contourpy",
        "jupyterlab",
        "notebook",
        "seaborn"
        ],
    python_requires=">=3.0",
)

