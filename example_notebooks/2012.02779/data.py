import sys
sys.path.append('../')
from fitmaker.fitlib.observable import ObsGroup, Obs
odir = '../fitmaker/observables/'

Zpole = ObsGroup.init_from_json(odir+'EWPO/Zpole.json')
Wmass = ObsGroup.init_from_json(odir+'EWPO/Wmass.json')

EWPO_data = ObsGroup({'observable_group_name':"EWPO_data", 'description':"Z pole & W mass data"})
EWPO_data.add_obs(
  Zpole,
  Wmass
)

Diboson_LEP_data = ObsGroup.init_from_json(odir+'Diboson/LEP2_Diboson.json')

Diboson_LHC_data_old = Obs.init_from_json(odir+'Diboson/mu_WW_enumunu_pt120_Run2.json')

Diboson_LHC_data_new_lastbin = Obs.init_from_json(odir+'Diboson/fidmu_WW_enumunu_pt300_Run2_ATLAS13.json')

Diboson_LHC_data_new = Obs.init_from_json(odir+'Diboson/fidmu_WW_enumunu_ptl_ATLAS13.json')
Diboson_LHC_Zjj_data = Obs.init_from_json(odir+'Diboson/mu_dsig_dphijj_Zjj_ATLAS_13TeV.json')

Diboson_LHC_WW_lastbin = Obs.init_from_json(odir+'Diboson/fidmu_WW_enumunu_pt300_Run2_ATLAS13.json')
Diboson_LHC_WW = Obs.init_from_json(odir+'Diboson/fidmu_WW_enumunu_ptl_ATLAS13.json')
Diboson_LHC_WZ_ATLAS = Obs.init_from_json(odir+'Diboson/mu_dsig_dpTZ_WZ_ATLAS_13TeV.json')
Diboson_LHC_WZ_CMS = Obs.init_from_json(odir+'Diboson/mu_dsig_dpTZ_norm_WZ_CMS_13TeV.json')


Diboson_LHC_WZ = ObsGroup({'observable_group_name':"Diboson_LHC_WZ", 'description':"LHC WZ diboson data"})
Diboson_LHC_WZ.add_obs(
    Diboson_LHC_WZ_ATLAS,
    Diboson_LHC_WZ_CMS
)

Diboson_LHC_data_new = ObsGroup({'observable_group_name':"Diboson_LHC_data_new", 'description':"LHC WW and WZ diboson data"})
Diboson_LHC_data_new.add_obs(
    Diboson_LHC_WW,
    Diboson_LHC_WZ_ATLAS,
    Diboson_LHC_WZ_CMS
)

Diboson_data_old = ObsGroup({'observable_group_name':"Diboson_data_old", 'description':"LEP & Old LHC Diboson data"})
Diboson_data_old.add_obs(
  Diboson_LEP_data,
  Diboson_LHC_data_old
)

Diboson_data = ObsGroup({'observable_group_name':"Diboson_data", 'description':"LEP & LHC Diboson data"})
Diboson_data.add_obs(
  Diboson_LEP_data,
  Diboson_LHC_data_new,
  Diboson_LHC_Zjj_data
)

Higgs_Run1_SS = ObsGroup.init_from_json(odir + 'Higgs/Run_1/LHC_Run1_Higgs_SignalStrengths.json')
Higgs_Run2_SS_old = ObsGroup.init_from_json(odir + 'Higgs/Run_2/LHC_Run2_Higgs_SignalStrengths.json')
Higgs_Run2_CMS_SS = ObsGroup.init_from_json(odir + 'Higgs/new/CMS_Run2_Higgs_SignalStrengths.json')
Higgs_Run2_ATLAS_SS = ObsGroup.init_from_json(odir + 'Higgs/new_ATLAS/ATLAS_sigStrength/ATLAS_Run2_Higgs_SignalStrengths.json')
Higgs_Run2_ATLAS_STXS = ObsGroup.init_from_json(odir+'Higgs/new_ATLAS/ATLAS_STXS_fine/ATLAS_Run2_STXS1p0_H_ZZ_4l_comb.json')
Higgs_Run2_ATLAS_Za_SS   = Obs.init_from_json(odir+'Higgs/new_ATLAS/mu_H_Za_Run2.json')
Higgs_Run2_ATLAS_mumu_SS = Obs.init_from_json(odir+'Higgs/new_ATLAS/mu_pp_H_mumu_ATLAS_Run2.json')

Higgs_data_old = ObsGroup({'observable_group_name':"Higgs_data_old", 'description':"Higgs signal strength data from old fit"})
Higgs_data_old.add_obs(
  Higgs_Run1_SS,
  Higgs_Run2_SS_old
)

Higgs_data_new_SS = ObsGroup({'observable_group_name':"Higgs_data_new_SS", 'description':"Updated Higgs signal strength"})
Higgs_data_new_SS.add_obs(
  Higgs_Run1_SS,
  Higgs_Run2_CMS_SS,
  Higgs_Run2_ATLAS_Za_SS,
  Higgs_Run2_ATLAS_mumu_SS,
  Higgs_Run2_ATLAS_SS
)

Higgs_data_new = ObsGroup({'observable_group_name':"Higgs_data_new", 'description':"Updated Higgs signal strength and STXS data"})
Higgs_data_new.add_obs(
  Higgs_Run1_SS,
  Higgs_Run2_CMS_SS,
  Higgs_Run2_ATLAS_Za_SS,
  Higgs_Run2_ATLAS_mumu_SS,
  Higgs_Run2_ATLAS_STXS
)

Higgs_LEP_old = ObsGroup({'observable_group_name':"Higgs_LEP_old", 'description':"Old Higgs + LEP"})
Higgs_LEP_old.add_obs(
  EWPO_data,
  Higgs_data_old
)

Higgs_LEP_new = ObsGroup({'observable_group_name':"Higgs_LEP_new", 'description':"New Higgs + LEP"})
Higgs_LEP_new.add_obs(
  EWPO_data,
  Higgs_data_new
)

Higgs_Diboson_old = ObsGroup({'observable_group_name':"Higgs_Diboson_old", 'description':"Old Higgs + Diboson"})
Higgs_Diboson_old.add_obs(
  Diboson_data_old,
  Higgs_data_old
)

Higgs_Diboson_new = ObsGroup({'observable_group_name':"Higgs_Diboson_new", 'description':"New Higgs + Diboson"})
Higgs_Diboson_new.add_obs(
  Diboson_data,
  Higgs_data_new
)

Higgs_LEP_Diboson_old = ObsGroup({'observable_group_name':"Higgs_LEP_Diboson_old", 'description':"old EWPO, Diboson & Higgs data"})
Higgs_LEP_Diboson_old.add_obs(
  EWPO_data,
  Diboson_data_old,
  Higgs_data_old
)

Higgs_LEP_Diboson_new = ObsGroup({'observable_group_name':"Higgs_LEP_Diboson_new", 'description':"new EWPO, Diboson & Higgs data"})
Higgs_LEP_Diboson_new.add_obs(
  EWPO_data,
  Diboson_data,
  Higgs_data_new
)

top_data_old = ObsGroup.init_from_json('../fitmaker/observables/top/top_data_old.json')

# separated top data
top_ttbar_Run1 = ObsGroup.init_from_json(odir+'top/ttbar_Run1.json')
top_ttbar_Run2 = ObsGroup.init_from_json(odir+'top/ttbar_Run2.json')

top_ttbar_Asy = ObsGroup.init_from_json(odir+'top/ttbar_asymmetries.json')

top_ttbar_whel = Obs.init_from_json(odir+'top/ttbar/w_helicity_fractions/ttbar_whel_8TeV_comb.json')

top_singletop_Run1 = ObsGroup.init_from_json(odir+'top/singletop_Run1.json')
top_singletop_Run2 = ObsGroup.init_from_json(odir+'top/singletop_Run2.json')

top_ttV_Run1 = ObsGroup.init_from_json(odir+'top/ttV_Run1.json')
top_ttV_Run2 = ObsGroup.init_from_json(odir+'top/ttV_Run2.json')
# same but with ttH sig.str.
top_ttX_Run2= ObsGroup.init_from_json(odir+'top/ttX_Run2.json')

top_data_new = ObsGroup({'observable_group_name':"top_data_new", 'description':"new top data"})
top_data_new.add_obs(
  top_ttbar_Run1,
  top_ttbar_Run2,
  top_ttbar_Asy ,
  top_ttbar_whel,
  top_singletop_Run1,
  top_singletop_Run2,
  top_ttV_Run1,
  top_ttV_Run2
)


all_data = ObsGroup({'observable_group_name':"all_data", 'description':"EWPO, Diboson, Higgs & top data"})
all_data.add_obs(
  EWPO_data,
  Diboson_data,
  Higgs_data_new,
  top_data_new
)
# remove overlapping ttH measurements used for top-only fit
# all_data = all_data.remove_obs('mu_ttH_13TeV_CMS','mu_ttH_13TeV_ATLAS')

all_data_old = ObsGroup({'observable_group_name':"all_data_old", 'description':"EWPO, Diboson, old Higgs & SMEFiT top data"})
all_data_old.add_obs(
  EWPO_data,
  Diboson_data_old,
  Higgs_data_old,
  top_data_old
)
# remove overlapping ttH measurements used for top-only fit
all_data_old = all_data_old.remove_obs('mu_ttH_13TeV_CMS','mu_ttH_13TeV_ATLAS')

if __name__=='__main__':
    higgs_meas = [o.measurement_name for o in Higgs_data_new.flatten().observables]

    print([m for m in higgs_meas if 'mumu' in m])
    print([m for m in higgs_meas if 'Za' in m])
