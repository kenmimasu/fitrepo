coeffs_tex = {
    'CHWB':r'C_{HWB}', 
    'CHD' :r'C_{HD}',
    'Cll' :r'C_{ll}',
    'CHl3':r'C_{Hl}^{(3)}',
    'CHl1':r'C_{Hl}^{(1)}',
    'CHe' :r'C_{He}',
    'CHq3':r'C_{Hq}^{(3)}',
    'CHq1':r'C_{Hq}^{(1)}',
    'CHQP':r'C_{HQ}^{(+)}',
    'CHQ3':r'C_{HQ}^{(3)}',
    'CHQ1':r'C_{HQ}^{(1)}',
    'CHu' :r'C_{Hu}',
    'CHt':r'C_{Ht}',
    'CHd' :r'C_{Hd}',
    'CW'  :r'C_{W}',
    'CHbox':r'C_{HBox}',
    'CHG':r'C_{HG}',
    'CHW':r'C_{HW}',
    'CHB':r'C_{HB}',
    'CG':r'C_{G}',
    'CtH':r'C_{tH}',
    'CtaH':r'C_{\tau H}',
    'CmuH':r'C_{\mu H}',
    'CbH':r'C_{bH}',
    'CtG':r'C_{tG}',
    'CtW':r'C_{tW}',
    'CtB':r'C_{tB}',
    'CQq31':r'C_{Qq}^{3,1}',
    'CQq38':r'C_{Qq}^{3,8}',
    'CQq18':r'C_{Qq}^{1,8}',
    'CQu8':r'C_{Qu}^{8}',
    'CQd8':r'C_{Qd}^{8}',
    'Ctq8':r'C_{tq}^{8}',
    'Ctu8':r'C_{tu}^{8}',
    'Ctd8':r'C_{td}^{8}'
} 

import sys
sys.path.append('../')
from fitmaker.fitlib.fitter import FitterChiSquare
from fitmaker.fitlib.fitter import FitterAnalyticalChiSquare
import pandas as pd
import numpy as np

def linear_fit_df(obsgroup, theory, params, Lambda=1000., rename = {}):
    '''
    Performs analytical linear fit, collecting results in pandas DataFrame.
    Returns FitterAnalyticalChiSquare instance & results DataFrame.
    '''
    fitter = FitterAnalyticalChiSquare(
      arg_obsgroup=obsgroup, 
      arg_theory=theory, 
      arg_theorykwargs={'Lambda':Lambda}
    )
    no_marg = fitter.singular_fisher_information

    res = [
      [
        rename.get(c,c), 
        fitter.bestfit[c], 
        np.nan if no_marg else fitter.standard_deviation(c, marginalise=True), 
        np.nan if no_marg else fitter.get_interval(c, marginalise=True)[0], 
        np.nan if no_marg else fitter.get_interval(c, marginalise=True)[1], 
        fitter.get_bestfit(c,marginalise=False)[0], 
        fitter.standard_deviation(c, marginalise=False),
        fitter.get_interval(c, marginalise=False)[0], 
        fitter.get_interval(c, marginalise=False)[1]
      ]
      for c in params
    ]
    
    df = pd.DataFrame(
      data=res, 
      columns=[
        'Coefficient', 
        'marginalised best-fit', 
        'marginalised s.d.',
        'marginalised lower',
        'marginalised upper',
        'individual best-fit',
        'individual s.d.',
        'individual lower',
        'individual upper'
    ]
    )
    
    df['marginalised interval'] = [
      '[ {:+.2g}, {:+.2g} ]'.format(a,b) 
      for a,b in zip(
        df['marginalised lower'],
        df['marginalised upper']
      )
    ]
    
    df['individual interval'] = [
      '[ {:+.2g}, {:+.2g} ]'.format(a,b) 
      for a,b in zip(
        df['individual lower'],
        df['individual upper']
      )
    ]
    
    return fitter, df