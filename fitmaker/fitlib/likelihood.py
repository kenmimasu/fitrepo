"""
``fitlib.likeihood``
=====================

Base class, LikelihoodBase used to define likelihood functions used by fitmaker.

Likelihoods
-----------
ChiSquareLikelihood
    Likelihood defined using a Chi^2 function.
AsymmErrLikelihood
    Chi^2-like likelihood that allows for asymmetric errors. 

"""
import numpy as np
from numpy.linalg import LinAlgError

from .core import FMCallable


class LikelihoodBase(FMCallable):
    """Base class for likelihood function object. Subclasses require the definition
    of likelihood_function() and/or _get_likelihood_function() methods.
    Never instantiated, only subclassed. Calling `likelihood_function` will 
    raise NotImplementerError.
    """
    def __init__(self, *args, **kwargs):
        """
        Parameters
        ----------
        args : tuple or list
            Positional arguments passed to FMCallable constructor.
        kwargs : dict
            Keyword arguments passed to FMCallable constructor.

        """
        super(LikelihoodBase, self).__init__(
          self._get_likelihood_function(*args, **kwargs)
        )

    def likelihood_function(self, *args, **kwargs):
        """
        Raises
        ------
        NotImplementedError
            Forces user to subclass LikelihoodBase to define likelihood 
            function.

        """
        raise NotImplementedError('likelihood_function() method not implemented '
        'for {}.'.format(self.__class__.__name__)
        )

    def _get_likelihood_function(self, *args, **kwargs):
        """
        Parameters
        ----------
        args : tuple or list
            Generic positional argument signature.
        kwargs : dict
            Generic keyword argument signature.
        
        Returns
        -------
        Callable
            `likelihood_function` attribute of LikelihoodBase subclass.
            
        """
        
        return self.likelihood_function

class ChiSquareLikelihood(LikelihoodBase):
    """Chi^2 likelihood constructed from a particular set of predictions, 
    measurements, errors and a correlation matrix.
    
    Attributes
    ----------
    observables : list
        Measured values for the observables.
    errors : list
        1-sigma uncertainties associated to each observable.
    correlation_matrix : numpy.array
        2D array representing the correlation matrix associated with `errors`.
    covariance_matrix : numpy.array
        2D array representing the covariance matrix constructed from `errors`.
        and `correlation_matrix`.
    covariance_matrix_inv : numpy.array
        Inverse of covariance matrix.
    prediction : FMCallable
        Function returning a prediction for the list of measurements in 
        `observables`.
    
    Raises
    ------
    ChiSquareLikelihoodError
        When the covariance matrix cannot be inverted by `numpy.linalg.inv`.
    
    """
    def __init__(self, prediction_func, observables, errors, correlation_matrix):
        """
        Parameters
        ----------
        prediction_func : FMCallable
            Function returning a prediction for the list of measurements in 
            `observables`.
        observables : list
            Measured values for the observables.
        errors : list
            1-sigma uncertainties associated to each observable.
        correlation_matrix : numpy.array
            2D array representing the correlation matrix associated with 
            `errors`.
    
        """
        self._check_arguments(observables, errors, correlation_matrix)

        self.observables, self.errors = observables, errors
        self.correlation_matrix = correlation_matrix

        self.covariance_matrix = correlation_matrix*np.outer(errors, errors)
        try:
            self.covariance_matrix_inv = np.linalg.inv(self.covariance_matrix)
        except LinAlgError as e:
            raise ChiSquareLikelihoodError('Error inverting covariance matrix: '+str(e))

        self.prediction = prediction_func

        super(ChiSquareLikelihood, self).__init__()
    #
    def _get_likelihood_function(self):
        """Construct chi^2 likelihood function from input attributes. 
        
        Returns
        -------
        ChiSquare : function
            Chi^2 likelihood function.
    
        """
        def ChiSquare(*args, **kwargs):
            """Chi^2 likelihood function."""
            dx = self.observables - self.prediction(*args, **kwargs)
            return dx.dot(self.covariance_matrix_inv).dot(dx.T)

        return ChiSquare

    def add_uncorrelated_error(self, errors):
        """Add an uncorrelated error source to the information, redefining and 
        re-inverting the covariance matrix.
        
        Parameters
        ----------
        errors :  array-like
            new error source, must have the same length as `self.observables`.
    
        """
        self._check_same_shape(self.observables, errors, 'observables', 'new error')
        self.covariance_matrix += np.diag(errors**2)
        self.covariance_matrix_inv = np.linalg.inv(self.covariance_matrix)

    @classmethod
    def _check_arguments(cls, observables, errors, correlation_matrix):
        """Check type and consistency in the shapes of the input data to the 
        constructor.
        
        Parameters
        ----------
        observables : list
            Measured values for the observables.
        errors : list
            1-sigma uncertainties associated to each observable.
        correlation_matrix : numpy.array
            2D array representing the correlation matrix associated with 
            `errors`.
    
        """
        labels = {
          'observables':observables,
          'errors':errors,
          'correlation_matrix':correlation_matrix
        }

        for lab, arr in labels.items():

            # check numpy.ndarray typing for arguments.
            cls._check_is_numpy_ndarray(arr, lab)

            # check dimensions array arguments
            if lab == 'correlation_matrix':
                cls._check_is_n_dimensional(2, arr, lab)
            else:
                cls._check_is_n_dimensional(1, arr, lab)

        # check shapes of array arguments
        cls._check_same_shape(observables, errors, 'observables', 'errors')
        cls._check_matrix_shape(observables, errors, correlation_matrix, name='correlation')

    @staticmethod
    def _check_is_numpy_ndarray(array, label):
        """Check if an array is a `numpy.ndarray`, raising custom error if not.

        Parameters
        ----------
        array : array-like
            Array to check.
        label : str
            Label to include in the error message.
        
        Raises
        ------
        ChiSquareLikelihoodError
            If `array` is not a `numpy.ndarray`.
        
        """
        if not isinstance(array, np.ndarray):
            msg = '{} is not of type numpy.ndarray'.format(label)
            raise ChiSquareLikelihoodError(msg)

    @staticmethod
    def _check_is_n_dimensional(dim, array, label):
        """Check if an array has a particular number of dimensions, raising 
        custom error if not.

        Parameters
        ----------
        dim : int
            Required dimensionality of `array`
        array : array-like
            Array to check.
        label : str
            Label to include in the error message.
        
        Raises
        ------
        ChiSquareLikelihoodError
            If `array` is not `dim`-dimensional.
        
        """
        if array.ndim != dim:
            msg = '{} is not a {} dimensional array'.format(label, dim)
            raise ChiSquareLikelihoodError(msg)

    @staticmethod
    def _check_same_shape(arr1, arr2, lab1, lab2):
        """Check if two arrays have the same shape, raising custom error if not.

        Parameters
        ----------
        arr1, arr2 : numpy.ndarray
            Arrays to compare.
        lab1, lab2 : str
            Labels to include in the error message.
        
        Raises
        ------
        ChiSquareLikelihoodError
            If shapes of `arr1` and `arr2` don't match.
        
        """
        if arr1.shape != arr2.shape:
            msg = ('Shape mismatch between {0} {1.shape} and {2} '
                   '{3.shape}.').format( lab1, arr1, lab2, arr2)
            raise ChiSquareLikelihoodError(msg)

    @staticmethod
    def _check_matrix_shape(arr1, arr2, matrix, name=''):
        """Check the shape of a 2D array.
        
        Check if 2D array has the right shape to be multiplied from the left 
        and the right by `arr1` and `arr2`, respectively.

        Parameters
        ----------
        arr1, arr2 : numpy.ndarray
            Arrays to extract expected shape of `matrix` from.
        matrix : numpy.ndarray
            2D array to check shape of.
        
        Raises
        ------
        ChiSquareLikelihoodError
            If the `matrix` does not have the expected shape.
        
        """
        required_shape = arr1.shape[0], arr2.shape[0]
        if matrix.shape != required_shape:
            nstr = name if name else 'unnamed'
            raise ChiSquareLikelihoodError(
              'Shape mismatch for {} matrix ( {} instead of {} ) '.format(
                 name, matrix.shape, required_shape
               )
            )

class ChiSquareLikelihoodError(Exception):
    """Custom Exception class for ChiSquareLikelihood."""
    pass

class AsymmErrLikelihood(LikelihoodBase):
    """Likelihood constructed from a particular set of predictions, 
    measurements, errors and a correlation matrix.  The AsymmErrLikelihood
    allows for asymmetric uncertainties on an observable.
    Follows the form 'Variable Gaussian (2)' of R. Barlow, arxiv:physics/0406120
    as used by Lilith, Kraml. et al, arxiv:1908.03952.
    
    Attributes
    ----------
    observables : list
        Measured values for the observables.
    errors : list
        2D list representing the lower and upper 1-sigma uncertainties associated to each observable.
    correlation_matrix : numpy.array
        2D array representing the correlation matrix associated with `errors`.
    prediction : FMCallable
        Function returning a prediction for the list of measurements in 
        `observables`.

    Raises
    ------
    AsymmErrLikelihoodError
        When the covariance matrix cannot be inverted by `numpy.linalg.inv`.

    """
    def __init__(self, prediction_func, observables, errors, correlation_matrix):

        """
        Parameters
        ----------
        prediction_func : FMCallable
            Function returning a prediction for the list of measurements in
            `observables`.
        observables : list
            Measured values for the observables.
        errors : list
            2D list of the upper and lower 1-sigma uncertainties associated to each observable.
        correlation_matrix : numpy.array
            2D array representing the correlation matrix associated with
            `errors`.

        """
        
        # self._check_arguments(observables, errors, correlation_matrix)
        
        self.observables, self.errors = observables, errors
        self.correlation_matrix = correlation_matrix
        self.prediction = prediction_func

        super(AsymmErrLikelihood, self).__init__()
    #
    def _get_likelihood_function(self):
        """Construct Variable Gaussian (2) likelihood function from input attributes.

        Returns
        -------
        AsymmErr : function
            Variable Gaussian (2) -2*log*likelihood function.

        """

        def AsymmErr(*args, **kwargs):
            """-2*Log*likelihood function, Eq.6 of arxiv:1908.03952"""
            dx = self.observables - self.prediction(*args, **kwargs)
            sigma = np.diag(
               np.sqrt(np.multiply(self.errors[:,0],self.errors[:,1]) + 
               np.multiply(np.abs(self.errors[:,0]-self.errors[:,1]),np.abs(dx)))
            )
            self.covariance_matrix = (sigma.dot(self.correlation_matrix)).dot(sigma)
            self.covariance_matrix_inv = np.linalg.inv(self.covariance_matrix)
            return dx.dot(self.covariance_matrix_inv).dot(dx.T)

        return AsymmErr

    @classmethod
    def _check_arguments(cls, observables, errors, correlation_matrix):
        """Check type and consistency in the shapes of the input data to the
        constructor.

        Parameters
        ----------
        observables : list
            Measured values for the observables.
        errors : list
            2D list of upper and lower 1-sigma uncertainties associated to each observable.
        correlation_matrix : numpy.array
            2D array representing the correlation matrix associated with
            `errors`.

        """

        labels = {
          'observables':observables,
          'errorsHigh':errors[:,0],
          'errorsLow':errors[:,1],
          'correlation_matrix':correlation_matrix
        }

        for lab, arr in labels.items():

            # check numpy.ndarray typing for arguments.
            cls._check_is_numpy_ndarray(arr, lab)

            # check dimensions array arguments
            if lab == 'correlation_matrix':
                cls._check_is_n_dimensional(2, arr, lab)
            else:
                cls._check_is_n_dimensional(1, arr, lab)

        # check shapes of array arguments
        cls._check_same_shape(observables, errors[:,0], errors[:,1], 'observables', 'errorsHigh', 'errorsLow')
        cls._check_matrix_shape(observables, errors[:,0], errors[:,1], correlation_matrix, name='correlation')

    @staticmethod
    def _check_is_numpy_ndarray(array, label):
        """Check if an array is a `numpy.ndarray`, raising custom error if not.

        Parameters
        ----------
        array : array-like
            Array to check.
        label : str
            Label to include in the error message.

        Raises
        ------
        AsymmErrLikelihoodError
            If `array` is not a `numpy.ndarray`.

        """
        if not isinstance(array, np.ndarray):
            msg = '{} is not of type numpy.ndarray'.format(label)
            raise AsymmErrLikelihoodError(msg)

    @staticmethod
    def _check_is_n_dimensional(dim, array, label):
        """Check if an array has a particular number of dimensions, raising
        custom error if not.

        Parameters
        ----------
        dim : int
            Required dimensionality of `array`
        array : array-like
            Array to check.
        label : str
            Label to include in the error message.

        Raises
        ------
        AsymmErrLikelihoodError
            If `array` is not `dim`-dimensional.

        if array.ndim != dim:
            msg = '{} is not a {} dimensional array'.format(label, dim)
            raise AsymmErrLikelihoodError(msg)
        """

    @staticmethod
    def _check_same_shape(arr1, arr2, arr3,  lab1, lab2, lab3):
        """Check if two arrays have the same shape, raising custom error if not.

        Parameters
        ----------
        arr1, arr2 : numpy.ndarray
            Arrays to compare.
        lab1, lab2 : str
            Labels to include in the error message.

        Raises
        ------
        AsymmErrLikelihoodError
            If shapes of `arr1` and `arr2` don't match.

        """
        if arr1.shape != arr2.shape:
            msg = ('Shape mismatch between {0} {1.shape} and {2} '
                   '{3.shape}.').format( lab1, arr1, lab2, arr2)
            raise AsymmErrLikelihoodError(msg)
        if arr1.shape != arr3.shape:
            msg = ('Shape mismatch between {0} {1.shape} and {2} '
                   '{3.shape}.').format( lab1, arr1, lab3, arr3)
            raise AsymmErrLikelihoodError(msg)
        if arr3.shape != arr2.shape:
            msg = ('Shape mismatch between {0} {1.shape} and {2} '
                   '{3.shape}.').format( lab3, arr3, lab2, arr2)
            raise AsymmErrLikelihoodError(msg)

    @staticmethod
    def _check_matrix_shape(arr1, arr2, arr3, matrix, name=''):
        """Check the shape of a 2D array.

        Check if 2D array has the right shape to be multiplied from the left
        and the right by `arr1` and `arr2`, respectively.

        Parameters
        ----------
        arr1, arr2 : numpy.ndarray
            Arrays to extract expected shape of `matrix` from.
        matrix : numpy.ndarray
            2D array to check shape of.

        Raises
        ------
        AsymmErrLikelihoodError
            If the `matrix` does not have the expected shape.

        """
        required_shape = arr1.shape[0], arr2.shape[0]
        if matrix.shape != required_shape:
            nstr = name if name else 'unnamed'
            raise AsymmErrLikelihoodError(
              'Shape mismatch for {} matrix ( {} instead of {} ) '.format(
                 name, matrix.shape, required_shape
               )
            )

class AsymmErrLikelihoodError(Exception):
    """Custom Exception for AsymErrLikelihood."""
    pass
