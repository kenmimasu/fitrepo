"""
``fitmaker.fitlib.formatting``
==============================

Some utility functions for formatting outputs.

String formatting
-----------------
reformat_numerical_array
    Cleans up whitespace in str representations of lists/arrays.
pretty_json_string
    Clean up whitespace in json string format. 
number_and_error
    String representation of a number and its uncertainty.

Numerical formatting
--------------------
signif
    Rounding numbers or `numpy.array`s to a number of significant digits. 
truncate_by_error
    Rounding numbers to significant digits determined by their uncertainty.

"""
import re
import numpy as np

def reformat_numerical_array(s):
    """Clean up whitespace to make nicer str representations of numerical 
    arrays in `to_json()` methods.
    
    Parameters
    ----------
    s : re.match 
        Matched pattern for a str representing a numerical array.
        
    Returns
    -------
    str
        Better formatted version of the numerical array.
    
    """
    sub1 = re.sub(r'\s+',r' ', s.group(0))
    # return re.sub(r'\],',r'],\n',sub1)
    return re.sub(r'\],(?!\s*\n)',r'],\n',sub1)

def pretty_json_string(s):
    """Clean up whitespace to make nicer str representations of numerical 
    arrays in string representations of json data.
    
    Parameters
    ----------
    s : str
        String representation of json data.
    
    Returns
    -------
    str
        Better formatted version of `s`.
    
    """
    pretty_jstr = re.sub(
      r'\s*\[.*?\],{0,1}', 
      reformat_numerical_array, s, 
      flags=re.DOTALL
    )
    return re.sub(r'\[\s*?\[',r'[\n  [', pretty_jstr)

def signif(x, p):
    """Round a number or `numpy.array`, `x` to `p` significant digits. 
    
    Parameters
    ----------
    x : float or `numpy.array`
        Value(s) to round.
    p : int
        Number of significant digits.
    
    Returns
    -------
    float or `numpy.array`
        Rounded result.
    
    """
    x = np.asarray(x)
    x_positive = np.where(np.isfinite(x) & (x != 0), np.abs(x), 10**(p-1))
    mags = 10 ** (p - 1 - np.floor(np.log10(x_positive)))
    return np.round(x * mags) / mags

def truncate_by_error(number, error):
    """Round a number or `numpy.array`, `number` to a number of significant 
    digits, determined by the uncertainty, `error`, on it.
    
    The number of significant digits kept corresponds to the number of decimal 
    places to which the quantity is known, according to `error`.
    
    Parameters
    ----------
    number : float or `numpy.array`
        Value(s) to round.
    error : float or `numpy.array`
        Uncertainty on `number`.
    
    Returns
    -------
    float or `numpy.array`
        Rounded result.
    
    """
    if error ==0.: return number
    error = signif(error, 1)
    order = np.floor(np.log10(error))
    try:
        return np.round(number, -np.int(order))
    except OverflowError as e:
        print(number, order)
        raise e

def number_and_error(number, error):
    """Print a string representation of `number` and its uncertainty, `error`.
    
    The number is rounded to the significant digits to which it is known 
    according to `truncate_by_error(number, error)`, and the first digit of the 
    uncertainty is placed in brackets alongside the value.
    
    e.g.
    >>> number_and_error(0.123, 0.004)
    '0.123(4)'
    
    Parameters
    ----------
    number : float
        Value(s) to round.
    error : float
        Uncertainty on `number`.
    
    Returns
    -------
    str
        String representation of `number` with `error`.
    
    """
    number = truncate_by_error(number, error)
    return '{:}({})'.format(number, ('{:.0e}'.format(error))[0])
