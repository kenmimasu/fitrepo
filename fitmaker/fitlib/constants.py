"""
``fitlib.constances``
=====================

Place to collect global constants that may be used by fitmaker

Defined constants
-----------------
pi
    Shortcut for numpy.pi
GeV2pb
    Conversion constant to go between GeV and picobarn to convert cross 
    sections expressed in natural units.

"""
from numpy import pi
GeV2pb = 0.389379e9