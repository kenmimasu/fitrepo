"""
``fitmaker.fitlib``
=====================

Main modules for fitmaker analysis.

modules
-------
theory
    Defines `TheoryBase` class to use as a parent class for users-defined 
    theories.
observable
    `Obs` and `ObsGroup` objects to contain information about (collections of) 
    experimental measurements to confront theories with.
likelihood
    
fitter
core
constants
util
plotter


"""