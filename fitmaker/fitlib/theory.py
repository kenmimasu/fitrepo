"""
``fitlib.theory``
=================

The theory module defines the base class, `TheoryBase`, from which all 
``fitmaker`` theory classes should inherit. It also defines the callable 
classes that are associated to, or used alongside `TheoryBase` instances.
The two decorators `internal_parameter` and `prediction` are important for 
defining your own theory class by subclassing TheoryBase.

Callabes
--------
`InternalParameter`
    Callable that returns the value of a theory parameter that depends on the 
    values of other parameters.
`TheoryPrediction`
    Callable that returns the prediction of a given theory for a particular 
    observable.
`MultiPrediction`
    Callable that returns a flattened array of predictions of a given theory 
    for a particular list of observables.
`BoundPrediction` 
    Callable that returns predictions from a particular theory that can be 
    initialised as a stand-alone object.

Decorators
----------
`internal_parameter`
    Decorator for tagging functions as being associated to internal parameters 
    when defining a TheoryBase subclass.
`prediction`
    Decorator for tagging functions as being a prediction for a particular 
    observable when defining a TheoryBase subclass.

"""

import warnings
warnings.filterwarnings('ignore')
import numpy as np
from collections import OrderedDict

################################################################################
# Base Theory metaclass implementation

from .core import create_ParamMap_subclass, ParameterMapError, ParameterMapKeyError, ParameterMapDuplicateKeysError
from .util import duplicates

class TheoryMeta(type):
    """Metaclass for Theory class to treat model specific functions as class
    attributes and manage inheritance.

    Key attributes passed onto the TheoryBase class
    -----------------------------------------------
    
    external_parameters : list
        List of strings defining parameter names that are needed to 
        intitialise an instance of the Theory class. Typically they are defined 
        in the class body or inherited from the parent class.
    internal_parameters : list
        List of strings defining parameter names that are internal, i.e., 
        that depend on the values of the parameters in external_parameters.
        The class will have a corresponding attribute that is a callable type 
        with a `_depends_on` attribute. These should be identified in the class 
        definition by using the @internal_parameter decorator on a member 
        function or other callable object, which also specifies on which 
        parameters the internal_parameter depends. 
    predictions : dict
        Dictionary mapping observables to the corresponding functions that 
        yield predictions for them. Keys are strings corresponding to a 
        unique observable name and values are callable objects that return 
        the predicted value of the observable given the current state 
        (values of the external/internal parameters) of the Theory class 
        instance. These should be identified in the class 
        definition by using the @prediction decorator on a member 
        function or other callable object, which also specifies the observable 
        to which the prediction corresponds.
    dependencies : dict
        Mapping of external parameters to dependent, internal parameters, 
        ordered according to the order in which the internal parameter 
        functions/callables should be evaluated when the value of the external 
        parameter is updated.
    _constant_internal_parameters : list 
        Special list of internal parameters that do not depend on anything and 
        on which nothing else depends. Their value is kept constant and never 
        updated.
    
    """
    def __new__(metaname, classname, baseclasses, attrs):
        """Manages assignment of the special attributes defined in the class 
        body.
        
        Parameters
        ----------
        classname : str
            Name of class being instantiated.
        
        baseclasses : list of TheoryBase or its derived classes
            Base classes from which current class inherits.
        
        attrs : list of str
            List of class attributes.

        Returns
        -------
        type
            Instance of basic metaclass `type`
        
        """
        
        attrs['classname'] = classname

        # initialise theory attributes
        if 'external_parameters' not in attrs:
            # external_parameters is optional
            attrs['external_parameters'] = []

        # Populate internal_parameters & predictions from class definition.
        attrs['internal_parameters'] = []
        attrs['predictions'] = {}
        for k, v in attrs.items():
            if hasattr(v, '_depends_on'):
                attrs['internal_parameters'].append(k)

            if hasattr(v, '_observable'):
                if v._observable in attrs['predictions']:
                    raise TheoryMetaError(
                      classname,
                      'Duplicate prediction for {}.'.format(v._observable)
                    )
                attrs['predictions'][v._observable] = k

        return super(TheoryMeta, metaname).__new__(metaname, classname,
                                                   baseclasses, attrs)

    def __init__(self, classname, baseclasses, attrs):
        """Manages inheritance of external, internal parameters and predictions.
        Creates ParameterMap instances for external & internal parameters.
        Resolves dependency graph for internal parameters to determine their
        evaluation order after external parameter update.
        
        Parameters
        ----------
        classname : str
            Name of class being instantiated.
        baseclasses : list of TheoryBase or its derived classes
            Base classes from which current class inherits.
        attrs : list of str
            List of class attributes.
        
        """
        self._baseclasses = baseclasses

        #  N/A for TheoryBase which inherits from object
        if classname == 'TheoryBase':
            pass
        else:
            # add all unique external_parameters from base classes
            baseclass_externals = self._get_baseclass_external_parameters()
            self.external_parameters.extend(baseclass_externals)

            # add all non-overloaded internal_parameters from base classes and
            # inherit internal_parameter method
            baseclass_internals = self._get_baseclass_internal_parameters()
            for ip, ip_method in baseclass_internals.items():
                self.internal_parameters.append(ip)
                setattr(self, ip, ip_method)

            # internal parameters take precedence, remove overlap.
            # allows for overloading external parameter with internal parameter
            # through inheritance.
            for ip in self.internal_parameters:
                if self._has_external_param(ip):
                    self.external_parameters.remove(ip)

            self.parameters = self.external_parameters + self.internal_parameters

            # append all non-overloaded predictions from base classes
            baseclass_predictions = self._get_baseclass_predictions()
            self.predictions.update(
              {k:v['func'] for k,v in baseclass_predictions.items()}
            )
            try:
                # create ParameterMap subclass with external_parameters as keys
                self.ext_param_class = create_ParamMap_subclass(
                  '{}_ExternalParameters'.format(self.classname),
                  self.external_parameters
                )
            except ParameterMapError as e:
                msg = 'Error in defining external parameters: ' + str(e)
                raise TheoryMetaError(classname, msg)

            try:
                # create ParameterMap subclass with internal_parameters as keys
                self.int_param_class  = create_ParamMap_subclass(
                  '{}_InternalParameters'.format(self.classname),
                  self.internal_parameters
                )
            except ParameterMapError as e:
                msg = 'Error in defining internal parameters: ' + str(e)
                raise TheoryMetaError(classname, msg)

            # define dependency evaluation order for each external parameter
            # i.e. in what order to update internal parameters if one modifies
            # an external parameter

            # construct dependency graph
            self._dependency_graph, self._constant_internal_parameters = (
              self._construct_dependency_graph()
            )

            # dependency evaluation order when modifying one external parameter
            self._dependencies = { k:[] for k in self.external_parameters }
            for k, v in self._dependencies.items():
                self._resolve_dependencies(k, v, [], self._dependency_graph)
                v.remove(k)

            # dependency evaluation order when modifying all external parameters
            self._set_all_order = self._get_init_evaluation_order(
                                    self.external_parameters,
                                    exclude = self._constant_internal_parameters
                                  )
            # dependency evaluation order for internal parameters that depend
            # on nothing
            no_direct_deps = [p for p in self._constant_internal_parameters
                              if not getattr(self,p)._depends_on]

            init_order = self._get_init_evaluation_order(
                           no_direct_deps,
                           exclude = self.external_parameters
                         )

            # avoid dupes
            init_order = [p for p in init_order if p not in self._set_all_order]
            # dependency evaluation order for initialising
            init_all_order = no_direct_deps + init_order + self._set_all_order

            # sanity check:  self._init_all_order should be a reordered version
            # of self.internal_parameters]
            if set(init_all_order).difference(set(self.internal_parameters)):
                raise TheoryMetaError(classname, 'Internal parameter dependency bug!')
            else:
                # internal_parameters ordered according to dependency evaluation
                self.internal_parameters = init_all_order

    # methods for inheriting parameters & predictions from base classes
    def _get_baseclass_external_parameters(self):
        """Handles the inheritance of external parameters from base classes.
        Duplicate names raise a warning.
        
        Returns
        -------
        list
            List of external parameters to add to the current class.
        
        """
        # loop through external parameters of base classes
        to_add = []
        for bc in self._baseclasses:
            for ep in bc.external_parameters:
                # append if not already defined in derived class
                if not self._has_external_param(ep):
                    if ep not in to_add:
                        to_add.append(ep)
                    else:
                        str_baseclasses = '({})'.format(
                          ', '.join([c.classname for c in self._baseclasses])
                        )
                        message = (
                          'Duplicate external parameter "{}" found when inheriting '
                          '{} from {}.'.format(ep, self.classname, str_baseclasses)
                        )
                        warnings.warn(message, TheoryMetaInheritanceWarning)
                # dont warn if duplicates found.
                else:
                    pass

        return to_add

    def _get_baseclass_internal_parameters(self):
        """Handle inheriting internal parameters.
        Duplicate names raise a warning.
        Returns a mapping of internal parameters to add and the corresponding
        method.
        
        Returns
        -------
        dict
            Mapping of internal parameter names and callable class attributes 
            that evaluate their values.
        
        """
        # loop through internal parameters of base classes
        to_add = {}
        for bc in self._baseclasses:
            for ip in bc.internal_parameters:
                # add if not already defined in derived class
                if not self._has_internal_param(ip):
                    # add if not already added
                    if ip not in to_add:
                        to_add[ip] = bc
                    # warn if attempted to add duplicate
                    else:
                        message = (
                          'Duplicate internal parameter "{}" found when inheriting '
                          '{} from {}. Will inherit definition & dependencies from '
                          '{}.'.format(ip, self.classname, bc.classname, to_add[ip].classname)
                        )
                        warnings.warn(message, TheoryMetaInheritanceWarning)
                # dont warn if already defined i.e. it has been overloaded
                else:
                    pass

        return {ip: getattr(bc, ip) for ip,bc in to_add.items()}

    def _get_baseclass_predictions(self):
        """Handle inheriting predictions from base classes.
        Duplicate names raise a warning.
        
        Returns
        -------
        dict
            Mapping of observable names and callable class attributes 
            that evaluate their values, and the base class from which the 
            method is inherited. The structure of the mapping is as follows:
            {  
              'prediction to inherit': {
                'func': str (method name),
                'baseclass': TheoryBase or derived class (baseclass from which 
                  it will be inherited)
              },
            }.
        
        """
        # loop through predictions of base classes
        to_add = {}
        for bc in self._baseclasses:
            for pr, prfunc in bc.predictions.items():
                # add if not already defined in derived class
                if not self._has_prediction(pr):
                    # add if not already added, giving an attribute to denote
                    # which baseclass it was inherited from.
                    if pr not in to_add:
                        to_add[pr] = {'baseclass':bc.classname, 'func':prfunc}
                    # warn if attempted to add duplicate
                    else:
                        message = (
                          'Duplicate prediction for "{}" found when inheriting  {} '
                          'from {}. Will inherit definition from'
                          ' {}.'.format(pr, self.classname, bc.classname,
                                        to_add[pr]['baseclass'])
                        )
                        warnings.warn(message, TheoryMetaInheritanceWarning)

                # dont warn if already defined i.e. it has been overloaded
                else:
                    pass

        return to_add

    # methods for dependency evaluation order
    def _construct_dependency_graph(self):
        """Construct dependency graph for internal parameters.
        Nodes represent parameters and directed edges connect a parameter
        to all internal parameters that directly depend on it, as defined by the
        arguments of the @internal_parameter decorator.
        Returns a mapping from model parameters to a list of dependencies and a
        list of internal parameters that do not depend on any external
        parameters.
        
        Returns
        -------
        tuple
            Two-element tuple corresponding to a mapping that describes the 
            dependency graph of the internal and external parameters and a list 
            of constant internal parameters that do not depend on anything and 
            on which nothing else depends.
        
        """

        dep_graph = { k:[] for k in self.external_parameters+self.internal_parameters }
        const_int_params = []
        for ip in self.internal_parameters:
            depends_on = []
            self._trace_external_param_dependencies(ip, depends_on, [])

            if not depends_on:
                const_int_params.append(ip)

            for dp in getattr(self, ip)._depends_on:
                dep_graph[dp].append(ip)

        return dep_graph, const_int_params

    def _trace_external_param_dependencies(self, param, deplist, seen):
        """Recursively trawl dependencies of a parameter to find external 
        parameters it depends on.
        
        Parameters
        ----------
        param : str
            Name of internal parameter.
        deplist : list
            list of dependencies identified
        seen : list 
            Parameters that have already been seen in the recursive search
        
        """
        seen.append(param)

        deps = getattr(self, param)._depends_on
        for dep in deps:
            if dep in seen:
                raise CircularDependencyError(dep)
            if dep in self.external_parameters:
                deplist.append(dep)
            elif dep not in self.parameters:
                msg = "While tracing dependencies for {}.".format(param)
                raise UnknownParameterError(self.classname, dep, msg)
            else:
                self._trace_external_param_dependencies(dep, deplist, seen)

        seen.remove(param)

    def _get_init_evaluation_order(self, params, exclude=[]):
        """Resolve dependency evaluation order for internal parameters.
        Uses the _resolve_dependencies() method.
        Used in _set_all_internal_parameters().
        Returns .
        
        Parameters
        ----------
        params : list
            List of external parameters.
        exclude : list, default=[]
            List of parameters to exclude.
        
        Returns
        -------
        list
            List of internal parameter names in the order they should be 
            evaluated reflecting the structure of the dependency graph.
        
        """
        dep_graph = dict(self._dependency_graph)
        
        # auxillary super node pointing to params
        dep_graph['all'] = params

        set_all_order = []
        self._resolve_dependencies('all', set_all_order, [], dep_graph)

        set_all_order.remove('all')

        for ep in params+exclude:
            try:
                set_all_order.remove(ep)
            except:
                pass

        return set_all_order

    @staticmethod
    def _resolve_dependencies(param, resolved, unresolved, graph):
        """Recursive function for resolving dependencies from a directed graph.

        Parameters
        ----------
        param : str
            Name of parameter whose dependencies need resolving.
        resolved : list
            List of parameters whose dependencies have already been resolved.
        unresolved : list
            List of parameters whose dependencies still need resolving.
        graph: dict
            Mapping representing a directed graph of dependencies.
                
        """
        unresolved.append(param)

        for dep in graph[param]:
            if dep not in resolved:
                if dep in unresolved:
                    raise CircularDependencyError(dep)
                TheoryMeta._resolve_dependencies(dep, resolved, unresolved, graph)

        resolved.insert(0, param)
        unresolved.remove(param)


    def __repr__(self):
        return '<Theory class {}>'.format(self.classname)

    def classinfo(self):
        """User function to print basic class information."""
        ip_dep_info = [
          '{}({})'.format(ip, ', '.join(getattr(self,ip)._depends_on))
          for ip in self.internal_parameters
        ]

        return (
        'Theory class {}\n'
        '  Derived from: {}\n'
        '  External parameters: {}\n'
        '  Internal parameters: {}\n'
        '  Predictions: {}\n'
        ).format(
          self.classname,
          ', '.join([x.classname for x in self._baseclasses]),
          ', '.join(self.external_parameters),
          ', '.join(ip_dep_info),
          ', '.join(self.predictions.keys())
        )

################################################################################
# Theory metaclass Exceptions

class TheoryMetaError(Exception):
    """Base exception class for TheoryMeta."""
    def __init__(self, theory, message):
        """
        Parameters
        ----------
        theory : str
            Name of theory class to print in the error message.
        message : str
            Error message to print.

        """
        message ='in {} definition: {}'.format(theory, message)
        super(TheoryMetaError, self).__init__(message)

class TheoryMetaWarning(Warning):
    """Base warning class for TheoryMeta. Never raised."""
    pass

class TheoryMetaInheritanceWarning(Warning):
    """Warning class for inheritance management in TheoryMeta."""
    pass

class UnknownParameterError(TheoryMetaError):
    """Exception when encountering undeclared parameter in dependency resolution."""
    def __init__(self, theory, parameter, message):
        """
        Parameters
        ----------
        theory : str
            Name of theory class to print in the error message.
        parameter : str
            Name of unknown parameter to print in the error message.
        message : str
            Error message to append to.

        """
        message += ' Unknown parameter "{}".'.format(parameter)
        super(UnknownParameterError, self).__init__(theory, message)

class CircularDependencyError(Exception):
    """Unique Exception class for circular dependency"""
    pass

################################################################################
# Base Theory class implementation

import itertools

class TheoryBase(object, metaclass=TheoryMeta):
    """Base class for theory.
    Dict-like container that can be indexed by key to access parameter values.
    Has two distinct ParameterMap (defined in core.py) instances as attributes:
        _external_parameter_map and _internal_parameter_map.
    Either `args` or `kwargs` should be used to set the initial values of the 
    external parameters.
    
    Attributes
    ----------
    _external_parameter_map : ParameterMap
        contains the inputs of the theory as specified by the user-implemented 
        external_parameters class attribute. Elements of external_parameters 
        are propagated down inheritance chains.
    _internal_parameter_map : ParameterMap
        contains the derived parameters of the theory. It is populated 
        according to user-defined functions 'tagged' by the @internal_parameter 
        decorator.
    predictions : dict
        Mapping from observable names to member function names that return the 
        predictions. It is populated according to user-defined functions
        'tagged' by the @prediction decorator, which takes the observable name 
        as an argument.
    
    """

    def __init__(self, *args, **kwargs):
        """
        Parameters
        ----------
        *args : tuple, optional
            Positional arguments used to instantiate external parameter values.
        **kwargs : dict, optional
            Keyword arguments used to instantiate external parameter values.

        """
        if type(self)==TheoryBase:
            raise TheoryInitError(
              'TheoryBase',
              'TheoryBase must be subclassed, not instantiated directly.'
            )

        self._external_parameter_map = self.ext_param_class()
        self._internal_parameter_map = self.int_param_class()

        try:
            self._external_parameter_map.update(*args, **kwargs)
        except ParameterMapError as p:
            msg = f'Failed to update external parameter map, raised: {p}'
            raise TheoryInitError(self.classname, msg)

        self._init_internal_parameters()

    def __repr__(self):
        return ('<Theory class {} instance>').format(self.classname)

    def info(self):
        """String representing basic info about class instance.
        
        Returns
        -------
        str
        """
        try:
            return (
               'Theory class {} instance.\n'
               '  Parameters:\n'
               '  External:\n'
               '    {}\n'
               '  Internal:\n'
               '    {}\n'
            ).format(self.classname, self._external_parameter_map,
                                     self._internal_parameter_map)
        except AttributeError as e:
            print(e)
            # EB: check
            return self.TheoryMeta.classinfo(self.__class__)

    @classmethod
    def _has_external_param(cls, key):
        """Check if class has an external parameter named `key`

        Parameters
        ----------
        key : str
            Name of parameter to check for.

        Returns
        -------
        bool
        
        """
        return key in cls.external_parameters

    @classmethod
    def _has_internal_param(cls, key):
        """Check if class has an internal parameter named `key`

        Parameters
        ----------
        key : str
            Name of parameter to check for.

        Returns
        -------
        bool
        
        """
        return key in cls.internal_parameters

    @classmethod
    def _has_prediction(cls, observable):
        """Check if class has an prediction for `observable`

        Parameters
        ----------
        observable : str
            Name of observable to check for.

        Returns
        -------
        bool
        
        """
        return observable in cls.predictions

    @classmethod
    def _get_prediction_method(cls, *observables):
        """Returns the unbound method labelled as a prediction for 
        `observables`.

        Parameters
        ----------
        observables : tuple
            Names of observables to return prediction methods for.
        
        Returns
        -------
        Callable or MultiPrediction
            If observable is length 1, returns the associated callable, 
            otherwise returns a MultiPrediction instance.
        """
        if len(observables)==1:
            if not cls._has_prediction(observables[0]):
                raise TheoryNoPredictionError(cls.classname, observables[0])
            return getattr(cls, cls.predictions[observables[0]])
        else:
            return cls._get_multi_prediction_method(*observables)

    @classmethod
    def _get_multi_prediction_method(cls, *observables):
        """Collects methods labelled as a predictions for the list of arguments
        `observables`, returning a new unbound method that returns a 1D numpy
        array of corresponding predictions.

        Parameters
        ----------
        observables : tuple
            Names of observables to return prediction methods for.
        
        Returns
        -------
        MultiPrediction
            Callable that returns a 1D numpy array of predictions for 
            `observables`.
        
        """
        predictions = []
        for obs in observables:
            if not cls._has_prediction(obs):
                raise TheoryNoPredictionError(cls.classname, obs)
            else:
                predictions.append(getattr(cls, cls.predictions[obs]))

        return MultiPrediction(*predictions)

    @classmethod
    def _get_internal_parameter_method(cls, key):
        """Returns the unbound method labelled as evaluating an internal 
        parameter `key`.
        
        Parameters
        ----------
        key : str
            Names of internal parameter to return method for.
        
        Returns
        -------
        Callable
            Callable that returns the value of the internal parameter.
        
        """
        if not cls._has_internal_param(key):
           raise ParameterNameError(cls.classname, key, parameter_type='internal')
        try:
            return getattr(cls, key)
        except AttributeError:
            msg = 'Failed to access internal parameter method {}'.format(key)
            raise InternalParameterError(cls.classname, msg)

    @classmethod
    def _add_prediction(cls, obs, func):
        """Adds new prediction method `func` for observable `obs`.
        
        Parameters
        ----------
        obs : str
            Name of observable to add prediction for.
        func : Callable
            Method that returns the prediction for the observable.
        
        """
        setattr(cls, func.__name__, func)
        cls.predictions[obs] = func.__name__

    # indexing functions
    def __getitem__(self, key):
        """Allow for indexing internal and external parameter by name.
        
        Parameters
        ----------
        key : str
            Name of internal or external parameter to get value of.
        
        """
        if self._has_external_param(key):
            return self._external_parameter_map[key]
        elif self._has_internal_param(key):
            return self._internal_parameter_map[key]
        else:
            raise ParameterNameError(self.classname, key)

    def __setitem__(self, key, value):
        """Allow for setting by parameter name, only external parameters can be 
        set.
        
        Parameters
        ----------
        key : str
            Name of external parameter to set value of.
        value : float
            Value for external parameter.
        
        """
        try:
            self._external_parameter_map[key] = value
        except ParameterMapKeyError as p:
            raise ParameterNameError(self.classname, key, parameter_type='external')

        for dep in self._dependencies[key]:
            self._set_internal_parameter(dep)

    def _set_internal_parameter(self, *keys):
        """Evaluate internal parameter. Modifies _internal_parameters directly
        so as to bypass __setitem__() restriction to external parameters only.
        
        Parameters
        ----------
        keys : tuple
            Names of internal parameters to evaluate.
        
        """
        for key in keys:
            method = self._get_internal_parameter_method(key)
            self._internal_parameter_map[key] = method(self)

    def _set_all_internal_parameters(self):
        """Evaluate all internal parameters in order determined by dependency 
        resolution (self._set_all_order).
        """
        self._set_internal_parameter(*self._set_all_order)

    def _init_internal_parameters(self):
        """Evaluate all internal parameters. Initialises once and for all
        internal parameters that do not depend on anything i.e. are constants.
        Method is only used after self._external_parameter_map.update is called 
        such that all initial values of the internal parameters that depend on 
        the external parameters are correct. This method then tidies up the 
        values of the internal parameters that e.g. depend on other internal 
        parameters or those that depend on nothing.
        """
        self._set_internal_parameter(*self.internal_parameters)

    def update(self, *args, **kwargs):
        """Update _external_parameters using ParameterMap.update() and update 
        the dependent internal parameters(). Either `args` or `kwargs` 
        should be used.
        
        Parameters
        ----------
        *args : tuple, optional
            Positional arguments used to set external parameter values.
        **kwargs : dict, optional
            Keyword arguments used to set external parameter values.
        
        """

        try:
            updated = self._external_parameter_map.update(*args, **kwargs)
        except ParameterMapError as p:
            raise TheoryUpdateError(self.classname, str(p))

        # set of affected internal parameters
        dependent_internal_params = []
        for dp in [x for y in updated for x in self._dependencies[y]]:
            if dp not in dependent_internal_params:
                dependent_internal_params.append(dp)

        # update values
        if len(updated) == 1:
        # if only one external parameter is updated, evaluate according to
        # dependency resolution
            self._set_internal_parameter(*self._dependencies[list(updated)[0]])
        else:
        # otherwise just update all
            self._set_all_internal_parameters()

    def predict(self, *observables):
        """Returns prediction for observables.
        
        Parameters
        ----------
        observables : tuple
            Names of observables to return predictions for.
        
        Returns
        -------
        Return type of prediction method (float, np.array, list,...)
            Value of prediction.
        
        """
        return self._get_prediction_method(*observables)(self)

    def predict_baseclass(self, baseclass, *observables):
        """Returns prediction for observable of a parent Theory class.
        Useful when a derived Theory predicts a modification to and observable
        prediction of the base class.
        Forces self.__class__ to be a subclass of baseclass.
        
        Parameters
        ----------
        baseclass : TheoryBase derived class
            Parent class to get prediction from.
        observables : tuple
            Names of observables to return predictions for.
        
        Returns
        -------
        Return type of prediction method (float, np.array, list,...)
            Value of prediction.
        
        """
        if not issubclass(self.__class__, baseclass):
            raise TheoryParentPredictError(self.classname, baseclass.__name__, observables)
        return baseclass._get_prediction_method(*observables)(self)

    def ext_param_array(self, *params):
        """Return an array of external parameter values.
        
        Parameters
        ----------
        params : tuple
            Names of external parameters to return values of.
        
        Returns
        -------
        numpy.array
            Values of external parameters.
        
        """
        return self._external_parameter_map.as_np(*params)

    def int_param_array(self, *params):
        """Return an array of internal parameter values.
        
        Parameters
        ----------
        params : tuple
            Names of internal parameters to return values of.
        
        Returns
        -------
        numpy.array
            Values of internal parameters.
        
        """
        return self._internal_parameter_map.as_np(*params)

    def param_array(self, *params):
        """Return an array of external or internal parameter values.
        
        Parameters
        ----------
        params : tuple
            Names of external or internal parameters to return values of.
        
        Returns
        -------
        numpy.array
            Values of internal parameters.
        
        """
        return np.array([self[p] for p in params]).astype(float)

    @classmethod
    def make_subset_class(cls, params, classname=None):
        """Return a subclass keeping a subset of a theory's external parameters. 

        All unspecified external parameters are removed from the subclass by 
        converting them to InternalParameters that return 0. Useful to avoid 
        manually writing class definitions for theory classes that are just 
        subsets of another one.
        
        Parameters
        ----------
        params : list or tuple of str
            Subset of class external_parameters that the new subclass should 
            keep as external.
        classname : str, default=None
            Name for subclass. If unspecified, will revert to original class 
            name, suffixed by '_'.join(params).
            
        Returns
        -------
        TheoryMeta
            Subclass of current class.
        
        Raises
        ------
        UnknownParameterError
            If any of `params` are not in in `external_parameters` of theory 
            class.
        
        """
        
        for p in params:
            if p not in cls.external_parameters:
                msg = f'Parameter "{p}" not in {cls}.external_parameters.'
                raise UnknownParameterError(cls, p, msg)

        # parameters not specified in `params` converted to trivial
        # InternalParameter instances
        attrs = {
          c:InternalParameter(lambda self: 0.) for c in 
          cls.external_parameters if c not in params 
        }
        
        if classname is None:
            classname = cls.classname+'_'+'_'.join(params)

        # Classes are instances of metaclasses
        submodel = TheoryMeta(
          classname, # name
          (cls,), # inherits from class
          attrs # new internal parameters
        )
    
        return submodel

################################################################################
# Theory class Exceptions

class TheoryError(Exception):
    """Base exception class for theory.py module."""
    def __init__(self, theory, message):
        """
        Parameters
        ----------
        theory : str
            Name of theory class to print in the error message.
        message : str
            Error message to print.

        """
        message ='in {}: {}'.format(theory, message)
        super(TheoryError, self).__init__(message)

class TheoryMethodError(TheoryError):
    """Exception raised in method of TheoryBase object, never raised."""
    method = ''
    def __init__(self, theory, message):
        """
        Parameters
        ----------
        theory : str
            Name of theory class to print in the error message.
        message : str
            Error message to print.

        """
        detail = 'in {}(): {}'.format(self.method, message)
        super(TheoryMethodError, self).__init__(theory, detail)

class TheoryInitError(TheoryMethodError):
    method = '__init__'
    """Exception raised in __init__() of TheoryBase object"""

class TheoryUpdateError(TheoryMethodError):
    method = 'update'
    """Exception raised in update() of TheoryBase object"""

class TheoryNoPredictionError(TheoryMethodError):
    """Exception raised in predict() method of TheoryBase object, never raised."""
    method = 'predict'
    def __init__(self, theory, observable):
        """
        Parameters
        ----------
        theory : str
            Name of theory class to print in the error message.
        observable : str
            Observable for which prediction is missing.

        """
        detail = 'No prediction for {}'.format(observable)
        super(TheoryNoPredictionError, self).__init__(theory, detail)

class TheoryParentPredictError(TheoryMethodError):
    """Exception raised in predict_parentclass() method of TheoryBase object, never raised."""
    method = 'predict'
    def __init__(self, theory, baseclass, observable):
        """
        Parameters
        ----------
        theory : str
            Name of theory class to print in the error message.
        baseclass : str
            Name of parent class to print in the error message.
        observable : str
            Observable for which prediction is missing.

        """
        detail = 'Attempt to access prediction for "{}" from non-baseclass, {}'.format(observable,baseclass)
        super(TheoryParentPredictError, self).__init__(theory, detail)

class ParameterError(TheoryError):
    """Exception class for parameter error."""
    pass

class ParameterNameError(ParameterError):
    """Exception class for parameter name error."""
    def __init__(self, theory, parameter_name, parameter_type=None):
        """
        Parameters
        ----------
        theory : str
            Name of theory class to print in the error message.
        parameter_name : str
            Name of parameter to print in the error message.
        parameter_type : str
            internal or external.

        """
        if parameter_type is None:
            message = 'Invalid parameter name "{}"'.format(parameter_name)
        else:
            fmt = 'Invalid {} parameter name "{}"'.format
            message = fmt(parameter_type, parameter_name)

        super(ParameterNameError, self).__init__(theory, message)

class InternalParameterError(ParameterError):
    """Error when failed to set internal parameter."""
    pass

################################################################################
# Decorators used to 'tag' methods of the Theory class

from .core import FMCallable
from .util import decorator_with_arguments

# FMCallable that  evaluates an internal parameter of the theory
class InternalParameter(FMCallable):
    """Callable object that evaluates an internal parameter. For wrapping
    user-defined functions tagged with @internal_parameter decorator.
    Accepts any number of positional arguments or the keyword argument
    "depends_on". They are intended to keep track of dependencies of the
    internal parameter on external parameters of the theory such that, when the
    parameters are updated, internal parameters are modified accordingly.
    
    Attributes
    ----------
    _depends_on : tuple
        Parameters on which this internal parameter depends.
    
    """
    def __init__(self, func, *dependencies, **kwargs):
        """
        Parameters
        ----------
        func : Callable
            Method that returns the value of the internal parameter.
        dependencies : str, optional
            Parameters on which this internal parameter depends.
        kwargs : dict, optional
            Keyword arguments passed to the decorator. Only `_depends_on` is 
            looked for, all others are ignored.
        
        """

        # append dependencies name as a data member of the function object
        depends_on = kwargs.get('depends_on', [])
        dependencies = list(dependencies)
        for d in depends_on:
            dependencies.append(d)

        self._depends_on = tuple(dependencies)
        super(InternalParameter, self).__init__(func)

    def __repr__(self):
        return '<InternalParameter function {}()>'.format(self.name)

@decorator_with_arguments
def internal_parameter(func, *dependencies, **kwargs):
    """Decorator that tags a function as returning the value of an internal
    parameter. Wraps the function with an instance of PredictionType.
    
    Parameters
    ----------
    func : Callable
        Method that returns the value of the internal parameter.
    dependencies : str, optional
        Parameters on which this internal parameter depends.
    kwargs : dict, optional
        Keyword arguments passed to the decorator. Only `_depends_on` is 
        looked for, all others are ignored.
    
    Returns
    -------
    InternalParameter
    
    """
    return InternalParameter(func, *dependencies, **kwargs)

# FMCallable that returns a prediction for an observable
class TheoryPrediction(FMCallable):
    """Callable objects that return predictions. For wrapping user-defined
    functions tagged with @prediction decorator.
    """
    def __init__(self, func, observable='', **kwargs):
        """
        Parameters
        ----------
        func : Callable 
            Unbound TheoryBase method returning the prediction.
        observable : str, default=''
            Observable name.
        kwargs : dict, optional
            Unused.
        
        """
        if not observable:
            raise TheoryPredictionError('N/A', 'Empty observable name')
        else:
            self._observable = observable

        super(TheoryPrediction, self).__init__(func)

    def __repr__(self):
        rep = '< {} wrapping {}(): prediction for "{}" >'.format
        return rep(self.__class__.__name__,  self.name,  self._observable)

@decorator_with_arguments
def prediction(func, observable, PredictionType=TheoryPrediction, **kwargs):
    """Decorator that tags a function as returning a prediction for an observable.
    Wraps the function with an instance of PredictionType, passing on any
    further keyword arguments.
    
    Parameters
    ----------
    func : Callable
        Method that returns the predicted value of `observable`.
    observable : str
        Name of observable associated to the prediction.
    PredictionType : optional, default=TheoryPrediction
        TheoryPrediction or a class derived from it with the same __init__ 
        signature.
        
    kwargs : dict, optional
        Keyword arguments passed to the PredictionType instantiation.
    
    Returns
    -------
    type(PredictionType)
    
    """
    return PredictionType(func, observable, **kwargs)

################################################################################
# Other useful prediction-generating callables

# FMCallable that returns a numpy array of predictions for a set of observables
class MultiPrediction(FMCallable):
    """Callable objects that returns a numpy array of predictions.
    Constructed from multiple TheoryPrediction instances.
    Return object of TheoryBase._get_prediction_method() when more than one
    argument is passed.
    
    Attributes
    ----------
    predictions : tuple or list
        Container for prediction methods.
    observables : tuple or list
        Container for observable names.
        
    """
    def __init__(self, *predictions):
        """
        Parameters
        ----------
        predictions : tuple
            Sequence of TheoryPrediction instances or callables with an 
            _observable attribute.
        
        """
        if not predictions:
            raise TheoryPredictionError('N/A', 'No predictions given')
        else:
            self.predictions = predictions
            self.observables = [x._observable for x in predictions]

        # define new function as a numpy array of TheoryPrediction functions
        def multifunc(inst):
            """Inner function to return array of prediction values."""
            prediction_funcs = []
            for r in [f(inst) for f in self.predictions]:
                # avoid having a mix of list and scalar for easy flattening
                if hasattr(r, '__iter__'):
                    prediction_funcs.append(r)
                else:
                    prediction_funcs.append([r])

            return np.array( list( itertools.chain(*prediction_funcs) ) )

        super(MultiPrediction, self).__init__(multifunc)

    def __repr__(self):
        rep = '< {} for: {} >'.format
        return rep(self.__class__.__name__,  ', '.join(self.observables))

from copy import deepcopy

# FMCallable that binds prediction method to a TheoryBase instance
class BoundPrediction(FMCallable):
    """Wrapper for TheoryPrediction bound to an internal instance of a TheoryBase
    subclass. Intercepts the arguments of __call__, feeding them to update()
    method of the instance and passing it to self.func.

    Prediction method is obtained through instance._get_prediction_method(*observables)
    which returns either TheoryPrediction or MultiPrediction depending on whether
    one or more arguments are passed.

    >> mypoint = MyTheory(1.,2.,3.)
    >> prediction =  BoundPrediction(mypoint, 'myobservable')

    Attributes
    ----------
    _instance : TheoryBase child class
        instance of TheoryBase subclass to which the prediction method is bound.
    _prediction: Callable
        Method that returns the value of the prediction.
    
    """
    def __init__(self, theoryclass, *observables, **kwargs):
        """
        Parameters
        ----------
        theoryclass : TheoryBase child class
            TheoryBase subclass to instantiate and bind prediction method to.
        observables : tuple
            Set of observables the method should predict.
        kwargs : dict, optional
            Keyword arguments to pass to the subclass initialiser.

        """
        self._instance = theoryclass(**kwargs)
        self._prediction = theoryclass._get_prediction_method(*observables)
        super(BoundPrediction, self).__init__(self._prediction)

    def __call__(self, *args, **kwargs):
        """Call the prediction function given certain values of the external 
        parameters for the TheoryBase subclass instance.
        
        Parameters
        ----------
        args : tuple, optional
            Values of the external parameters to set for the TheoryBase 
            subclass instance.
        kwargs : dict, optional
            Key value pairs corresponding to the external parameters names and 
            their values to set for the TheoryBase subclass instance.
        
        """
        self._instance.update(*args, **kwargs)
        try:
            return self._func(self._instance)
        except Exception as e:
            import traceback
            raise Exception(traceback.format_exc())

    def __repr__(self):
        rep = '< {}: {} bound to {} >'.format
        return rep(self.__class__.__name__, self._prediction, self._instance)

################################################################################
# TheoryPrediction Exceptions

class TheoryPredictionError(Exception):
    """Base exception class for TheoryPrediction class."""
    def __init__(self, prediction, message):
        """
        Parameters
        ----------
        prediction : str
            Name of the prediction to put in the error message. 
        message : str
            Basic message to append `prediction` information to.
        
        """
        message = 'In prediction for {}: {}'.format(prediction, message)
        super(TheoryPredictionError, self).__init__(message)

class TheoryPredictionNotCallableError(TheoryPredictionError):
    """TheoryPrediction Exception for when non-callable is passed to constructor."""
    def __init__(self, prediction):
        """
        Parameters
        ----------
        prediction : str
            Name of the prediction to put in the error message. 
        
        """
        msg = 'non-callable object passed to constructor.'
        super(TheoryPredictionNotCallableError,
          self).__init__(prediction, msg)

###############################################################################
if __name__=='__main__':
    # classes for testing
    class TheoryA(TheoryBase):

        external_parameters = ['a1', 'a2']

        @internal_parameter(depends_on=('a1','a2'))
        def a3(self):
            return self['a1'] + self['a2']

        @prediction('earth_r')
        def radius_of_the_earth_km(self):
            return self['a1']*6.
    #
    class TheoryB(TheoryBase):

        external_parameters = ['b1', 'b2']

        @internal_parameter('b1','b2')
        def b3(self):
            return self['b1'] - self['b2']

        @prediction('sun_r')
        def radius_of_the_sun_km(self):
            return self['b1']*6.e7

        @prediction('dist')
        def distributions(self):
            return [self['b1']*float(i) for i in range(5)]

    # class TheoryA2(TheoryA):
    #
    #     external_parameters = ['b1']
    #
    #     @internal_parameter('a1','b1')
    #     def c1(self):
    #         return self['a1'] + self['b1']*100
    #
    #     @prediction('earth_age')
    #     def age_of_the_earth(self):
    #         return self['c1']*1.e7
    #
    #     @prediction('earth_r')
    #     def radius_of_the_earth_km(self):
    #         pred_A = self.predict_baseclass(TheoryA, 'earth_r')
    #         return pred_A + self['a1']*6.
    #
    # class TheoryA3(TheoryA):
    #
    #     external_parameters = ['b1']
    #
    #     @internal_parameter(depends_on=())
    #     def a1(self):
    #         return 1.
    #
    #     @internal_parameter('a1','b1')
    #     def c1(self):
    #         return self['a1'] + self['b1']*100
    #
    #     @prediction('earth_age')
    #     def age_of_the_earth(self):
    #         return self['c1']*1.e7
    #
    #     @prediction('earth_r')
    #     def radius_of_the_earth_km(self):
    #         pred_A = self.predict_baseclass(TheoryA, 'earth_r')
    #         return pred_A + self['a1']*6.
    #
    # class TheoryAB(TheoryA, TheoryB):
    #     pass
    #
    # class TheoryA2B(TheoryA2, TheoryB):
    #     @prediction('earth_r')
    #     def radius_of_the_earth_km(self):
    #         pred_A2 = self.predict_baseclass(TheoryA2, 'earth_r')
    #         return pred_A2 + self['a1']*6.
    #
    # class TheoryDep(TheoryBase):
    #     """Toy class to test dependency resolution."""
    #     external_parameters = ['D','E','F']
    #
    #     @internal_parameter(depends_on=('B','D'))
    #     def A(self):
    #         print 'Evaluating A'
    #         return self['D']/self['B']
    #
    #     @internal_parameter(depends_on=('C','E'))
    #     def B(self):
    #         print 'Evaluating B'
    #         return self['C'] + self['E']
    #
    #     @internal_parameter(depends_on=('D','E'))
    #     def C(self):
    #         print 'Evaluating C'
    #         return self['D'] + self['E']
    #
    #     @internal_parameter(depends_on=('F','C'))
    #     def G(self):
    #         print 'Evaluating G'
    #         return self['F']

    # class TheoryNoDep(TheoryBase):
    #     """Toy class to test dependency resolution."""
    #     external_parameters = ['E','F','G']
    #
    #     @internal_parameter('A','B','C')
    #     def D(self):
    #         print 'Evaluating D'
    #         return self['A']+ self['B'] +self['C']
    #
    #     @internal_parameter('B')
    #     def C(self):
    #         print 'Evaluating C'
    #         return self['B']
    #
    #     @internal_parameter('A')
    #     def B(self):
    #         print 'Evaluating B'
    #         return self['A']
    #
    #     @internal_parameter()
    #     def A(self):
    #         print 'Evaluating A'
    #         return 10.




    # class TheoryOne(TheoryBase):
    #     external_parameters = ['a1']
    #     pass

    # # TODO:
    # # SM example with EWPO predictions
    # # AC example inheriting from SM
    #
    # test initialisation
    # myguy = TheoryA(1.,2.)
    # print myguy
    # myguy = TheoryA(a1=1., a2=2.)
    # print myguy
    # myguy = TheoryA({'a1':1.,'a2':2.})
    # print myguy
    # myguy = TheoryA([('a1',1.),('a2',2.)])
    # print myguy.ext_param_array
    #
    # # test update method
    # myguy = TheoryA(1.,2.)
    # myguy.update((3.,4.))
    # print myguy
    # myguy.update({'a1':5.,'a2':6.})
    # print myguy
    # myguy.update(**{'a1':5.,'a2':6.})
    # print myguy
    # myguy.update(a1=54.)
    # print myguy
    # #
    # # test setting and getting
    # myguy = TheoryA(1.,2.)
    # myguy['a1']=100.
    # print myguy
    # myguy['a2']=100.
    # print myguy
    # print myguy['a1']
    # print myguy['a2']
    # print myguy['a3']
    # try:
    #     myguy['a3']=100.
    # except ParameterNameError as p:
    #     print '[Pass!]:', p
    #
    # # test inheritance
    # print TheoryA3
    # myguy = TheoryA3(1.,2.)
    # print myguy
    # print TheoryAB.external_parameters
    # print TheoryAB.internal_parameters
    #
    # print TheoryA2B
    # guy = TheoryA2B(1,2,3,4)
    # print guy
    # guy['a2'] = 15.
    # print guy
    # guy.update(3.,4.,5.,6.)
    # print guy
    # guy.update(**{'a1':5.,'b2':6.})
    # print guy
    # print guy.predict('sun_r')
    #
    # # test dependency resolution
    # guy = TheoryDep(1.,2.,3.)
    # print ''
    # guy.update(2.,3.,4.)
    # print ''
    # guy.update(F=6.)
    # print TheoryNoDep._constant_internal_parameters
    # print TheoryNoDep._set_all_order
    # guy = TheoryNoDep(1.,2.,3.)

    # test array prediction
    # print TheoryB.info()
    # guy = TheoryB(1.,2.)
    # print guy.predict('dist','sun_r')

    # def fun(*args):
    #     print args
    #
    # fun(1)
    print(TheoryB.classinfo())

    guy = TheoryB(1.,2.)

    multi = MultiPrediction(*[guy._get_prediction_method(p) for p in guy.predictions])

    # multi2 = guy._get_multi_prediction_method(*guy.predictions)
    # # print guy.info()
    method = BoundPrediction(TheoryB, 'dist', 'sun_r')

    print(method(1.,2.))
    # print method._instance.info()
    # print multi

    # guy.update(17.8,99.2)
    # print multi(guy)
    # print multi(guy)
    # print multi2(guy)
    #
    # guy.update(3.,4.)
    # print multi(guy)
    # print multi2(guy)

    pass
