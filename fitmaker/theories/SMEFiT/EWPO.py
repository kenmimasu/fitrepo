import numpy as np
from numpy import sqrt
from ...fitlib.constants import pi, GeV2pb 
from ...fitlib.theory import internal_parameter, prediction

from ..SM.MW_MZ_GF_5F.EWPO import Tree_Fixed as SM_MW_MZ_GF_5F_Fixed, LEP_EWWG as SM_MW_MZ_GF_5F_EWPO
from ..SM.MW_MZ_GF_5F.diboson import LEP as SM_MW_MZ_GF_5F_Diboson_LEP

from ..EFT import EFTBase, EFTPrediction, EFTInternalParameter

from .inputs import SMEFiT, SMEFiT_U2_2_U3_3, SMEFiT_U3_5
from . import data_path
# EWPO from jsons provided by SMEFiT
json_path = data_path+'EWPO/'


# Predictions given additively as shifts of best predictions
# O_best + dO_EFT

class SMEFiT_EWPO(EFTBase, SMEFiT):
    '''
    Model of SM plus EWPO predictions used by the SMEFiT collab.
    '''
    mu_aEWM1_MZ_MSbar = EFTPrediction.from_json(
      json_path+'mu_aEWM1_MZ_MSbar.json'
    )

SMEFiT_EWPO.add_predictions_from_json(json_path+'SMEFiT_EWPO.json')

class SMEFiT_EWPO_lin(EFTBase, SMEFiT):
    '''
    Model of SM plus EWPO predictions used by the SMEFiT collab.
    '''
    mu_aEWM1_MZ_MSbar = EFTPrediction.from_json(
      json_path+'mu_aEWM1_MZ_MSbar.json', linear_only=True
    )
    
SMEFiT_EWPO_lin.add_predictions_from_json(json_path+'SMEFiT_EWPO.json', linear_only=True)

#  Reduction to U(2)^2 x U(3)^3
class SMEFiT_EWPO_U2_2_U3_3(SMEFiT_EWPO, SMEFiT_U2_2_U3_3):
    '''
    Model of SM plus EWPO predictions used by the SMEFiT collab.
    Reduction to U(2)^2 x U(3)^3 flavor symmetry.
    '''
    @prediction('Rl_0')
    def Rl_0(self):
        return self.predict('Re_0')
    
    @prediction('Al')
    def Al(self):
        return self.predict('Ae')
    
    @prediction('AFB_l')
    def AFB_l(self):
        return self.predict('AFB_e')
        
class SMEFiT_EWPO_lin_U2_2_U3_3(SMEFiT_EWPO_lin, SMEFiT_U2_2_U3_3):
    '''
    Model of SM plus EWPO predictions used by the SMEFiT collab.
    Linear only.
    Reduction to U(2)^2 x U(3)^3 flavor symmetry.
    '''
    @prediction('Rl_0')
    def Rl_0(self):
        return self.predict('Re_0')
    
    @prediction('Al')
    def Al(self):
        return self.predict('Ae')
    
    @prediction('AFB_l')
    def AFB_l(self):
        return self.predict('AFB_e')
        
#  Reduction to U(3)^5
class SMEFiT_EWPO_U3_5(SMEFiT_EWPO_U2_2_U3_3, SMEFiT_U3_5):
    '''
    Model of SM plus EWPO predictions used by the SMEFiT collab.
    Reduction to U(3)^5 flavor symmetry.
    '''
    pass

class SMEFiT_EWPO_lin_U3_5(SMEFiT_EWPO_lin_U2_2_U3_3, SMEFiT_U3_5):
    '''
    Model of SM plus EWPO predictions used by the SMEFiT collab.
    Linear only.
    Reduction to U(3)^5 flavor symmetry.
    '''
    pass

# Prediction given multiplicatively as relative scalings of best predictions
# O_best*( 1 + dO_EFT/O_SM )

class SMEFiT_EWPO_best(EFTBase, SMEFiT):
    '''
    Model of SM plus EWPO predictions used by the SMEFiT collab.
    '''
    mu_aEWM1_MZ_MSbar = EFTPrediction.from_json(
      json_path+'mu_aEWM1_MZ_MSbar.json', linear_only=True
    )
    
    
SMEFiT_EWPO_best.add_predictions_from_json(json_path+'SMEFiT_EWPO_best.json')

class SMEFiT_EWPO_best_lin(EFTBase, SMEFiT):
    '''
    Model of SM plus EWPO predictions used by the SMEFiT collab.
    '''
    mu_aEWM1_MZ_MSbar = EFTPrediction.from_json(
      json_path+'mu_aEWM1_MZ_MSbar.json', linear_only=True
    )
    
SMEFiT_EWPO_best_lin.add_predictions_from_json(json_path+'SMEFiT_EWPO_best.json', linear_only=True)

#  Reduction to U(2)^2 x U(3)^3
class SMEFiT_EWPO_best_U2_2_U3_3(SMEFiT_EWPO_best, SMEFiT_U2_2_U3_3):
    '''
    SMEFT Warsaw basis for EWPO, based on "SMEFT_global_fit_warsaw.nb".
    Core shifts + map from Warsaw basis.
    '''
    @prediction('Rl_0')
    def Rl_0(self):
        return self.predict('Re_0')
    
    @prediction('Al')
    def Al(self):
        return self.predict('Ae')
    
    @prediction('AFB_l')
    def AFB_l(self):
        return self.predict('AFB_e')
        
class SMEFiT_EWPO_best_lin_U2_2_U3_3(SMEFiT_EWPO_best_lin, SMEFiT_U2_2_U3_3):
    '''
    SMEFT Warsaw basis for EWPO, based on "SMEFT_global_fit_warsaw.nb".
    Core shifts + map from Warsaw basis.
    '''
    @prediction('Rl_0')
    def Rl_0(self):
        return self.predict('Re_0')
    
    @prediction('Al')
    def Al(self):
        return self.predict('Ae')
    
    @prediction('AFB_l')
    def AFB_l(self):
        return self.predict('AFB_e')
        
#  Reduction to U(3)^5
class SMEFiT_EWPO_best_U3_5(SMEFiT_EWPO_best_U2_2_U3_3, SMEFiT_U3_5):
    '''
    SMEFT Warsaw basis for EWPO, based on "SMEFT_global_fit_warsaw.nb".
    Core shifts + map from Warsaw basis.
    '''
    pass

class SMEFiT_EWPO_best_lin_U3_5(SMEFiT_EWPO_best_lin_U2_2_U3_3, SMEFiT_U3_5):
    '''
    SMEFT Warsaw basis for EWPO, based on "SMEFT_global_fit_warsaw.nb".
    Core shifts + map from Warsaw basis.
    '''
    pass



