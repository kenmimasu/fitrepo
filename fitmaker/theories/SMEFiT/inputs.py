from ..SM.MW_MZ_GF_5F.inputs import MW_MZ_GF_5F_Fixed
from ...fitlib.theory import internal_parameter

class SMEFiT(MW_MZ_GF_5F_Fixed):
    external_parameters = [
      # Higgs kinetic & T-param
      'cdp', 'cpdc', 
      # gauge-Higgs
      'cpWB', 'cpBB', 'cpW', 'cpG', 
      # quark-current
      'cpq3i', 'cpqMi', 'cpu', 'cpd',
      'cpQ3', 'cpQM', 'cpt',
      # lepton-current
      'c3pl1', 'c3pl2', 'c3pl3',
      'cpl1', 'cpl2', 'cpl3',
      'cpe', 'cpmu', 'cpta',
      # TGC
      'cWWW',
      # GFermi
      'cll'
    ]

class SMEFiT_U2_2_U3_3(SMEFiT):
    external_parameters = [
      'c3pl', 'cpl', 'cpei'
    ]
    
    @internal_parameter(depends_on=('c3pl', ))
    def c3pl1(self):
        return self['c3pl']

    @internal_parameter(depends_on=('c3pl', ))
    def c3pl2(self):
        return self['c3pl']

    @internal_parameter(depends_on=('c3pl', ))
    def c3pl3(self):
        return self['c3pl']

    @internal_parameter(depends_on=('cpl', ))
    def cpl1(self):
        return self['cpl']
    
    @internal_parameter(depends_on=('cpl', ))
    def cpl2(self):
        return self['cpl']
    
    @internal_parameter(depends_on=('cpl', ))
    def cpl3(self):
        return self['cpl']
        
    @internal_parameter(depends_on=('cpei', ))
    def cpe(self):
        return self['cpei']
        
    @internal_parameter(depends_on=('cpei', ))
    def cpmu(self):
        return self['cpei']        

    @internal_parameter(depends_on=('cpei', ))
    def cpta(self):
        return self['cpei']

class SMEFiT_U3_5(SMEFiT_U2_2_U3_3):
    # U(3)^5
    @internal_parameter()
    def cpQ3(self):
        return self['cpq3i']    

    @internal_parameter()
    def cpQM(self):
        return self['cpqMi']  
    
    @internal_parameter()
    def cpt(self):
        return self['cpu']
        