from ...fitlib.constants import pi, GeV2pb 
from ...fitlib.theory import internal_parameter, prediction
from ..EFT import EFTBase, EFTPrediction, EFTInternalParameter
from .inputs import SMEFT_MW_MZ_GF
from .decays import V_Decays, V_Decays_lin
from . import data_path

json_path = data_path+'triboson/'
class LHC_NLO(V_Decays):
    # leptonic BRs
    mu_BR_W_lv = V_Decays.mu_BR_W_lv
    mu_BR_Z_ll = V_Decays.mu_BR_Z_ll
    
    # LHC 13 TeV VVV signal strengths
    ## WWW
    ### production
    mu_WWW_NLO_13TeV = EFTInternalParameter.from_json(json_path+'mu_WWW_NLO_13TeV.json')
    ### leptonic branching ratio
    BR_WWW_lep = mu_BR_W_lv.power(3.)
    ### production * leptonic branching ratio
    mu_WWW_NLO_lep_13TeV = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_product( mu_WWW_NLO_13TeV, BR_WWW_lep ),
      'mu_WWW_13TeV'
    )
    
    ## WWZ
    ### production
    mu_WWZ_NLO_13TeV = EFTInternalParameter.from_json(json_path+'mu_WWZ_NLO_13TeV.json')
    ### leptonic branching ratio
    BR_WWZ_lep = ( mu_BR_W_lv.power(2.) ).product( mu_BR_Z_ll )
    ### production * leptonic branching ratio
    mu_WWZ_NLO_lep_13TeV = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_product( mu_WWZ_NLO_13TeV, BR_WWZ_lep ),
      'mu_WWZ_13TeV'
    )
    
    ## WZZ
    ### production
    mu_WZZ_NLO_13TeV = EFTInternalParameter.from_json(json_path+'mu_WZZ_NLO_13TeV.json')
    ### leptonic branching ratio
    BR_WZZ_lep = ( mu_BR_Z_ll.power(2.) ).product( mu_BR_W_lv )
    ### production * leptonic branching ratio
    mu_WZZ_NLO_lep_13TeV = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_product( mu_WZZ_NLO_13TeV, BR_WZZ_lep ),
      'mu_WZZ_13TeV'
    )

    ## ZZZ
    ### production
    mu_ZZZ_NLO_13TeV = EFTInternalParameter.from_json(json_path+'mu_ZZZ_NLO_13TeV.json')
    ### leptonic branching ratio
    BR_ZZZ_lep = ( mu_BR_Z_ll.power(3.) )
    ### production * leptonic branching ratio
    mu_ZZZ_NLO_lep_13TeV = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_product( mu_ZZZ_NLO_13TeV, BR_ZZZ_lep ),
      'mu_ZZZ_13TeV'
    )
    
    ## WWa
    mu_WWa_NLO_13TeV = EFTInternalParameter.from_json(json_path+'mu_WWA_NLO_13TeV.json')
    BR_WWa_lep = mu_BR_W_lv.power(2.) 
    ### production * leptonic branching ratio
    mu_WWa_NLO_lep_13TeV = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_product( mu_WWa_NLO_13TeV, BR_WWa_lep ),
      'mu_WWa_13TeV'
    )
    
    ## WZa
    mu_WZa_NLO_13TeV = EFTInternalParameter.from_json(json_path+'mu_WZA_NLO_13TeV.json')
    ### production * leptonic branching ratio
    BR_WZa_lep = mu_BR_W_lv.product( mu_BR_Z_ll )
    mu_WZa_NLO_lep_13TeV = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_product( mu_WZa_NLO_13TeV, BR_WZa_lep ),
      'mu_WZa_13TeV'
    )
    
    ## Waa
    mu_Waa_NLO_13TeV = EFTInternalParameter.from_json(json_path+'mu_WAA_NLO_13TeV.json')
    ### production * leptonic branching ratio
    mu_Waa_NLO_lep_13TeV = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_product( mu_Waa_NLO_13TeV, mu_BR_W_lv ),
      'mu_Waa_13TeV'
    )
      

class LHC_NLO_lin(V_Decays_lin):
    # leptonic BRs
    mu_BR_W_lv = V_Decays_lin.mu_BR_W_lv
    mu_BR_Z_ll = V_Decays_lin.mu_BR_Z_ll
    
    # LHC 13 TeV VVV signal strengths
    ## WWW
    ### production
    mu_WWW_NLO_13TeV = EFTInternalParameter.from_json(json_path+'mu_WWW_NLO_13TeV.json', linear_only=True)
    ### leptonic branching ratio
    BR_WWW_lep = mu_BR_W_lv.power(3.)
    ### production * leptonic branching ratio
    mu_WWW_NLO_lep_13TeV = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_product( mu_WWW_NLO_13TeV, BR_WWW_lep, linear_only=True ),
      'mu_WWW_13TeV'
    )
    
    ## WWZ
    ### production
    mu_WWZ_NLO_13TeV = EFTInternalParameter.from_json(json_path+'mu_WWZ_NLO_13TeV.json', linear_only=True)
    ### leptonic branching ratio
    BR_WWZ_lep = ( mu_BR_W_lv.power(2.) ).product( mu_BR_Z_ll )
    ### production * leptonic branching ratio
    mu_WWZ_NLO_lep_13TeV = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_product( mu_WWZ_NLO_13TeV, BR_WWZ_lep, linear_only=True ),
      'mu_WWZ_13TeV'
    )
    
    ## WZZ
    ### production
    mu_WZZ_NLO_13TeV = EFTInternalParameter.from_json(json_path+'mu_WZZ_NLO_13TeV.json', linear_only=True)
    ### leptonic branching ratio
    BR_WZZ_lep = ( mu_BR_Z_ll.power(2.) ).product( mu_BR_W_lv )
    ### production * leptonic branching ratio
    mu_WZZ_NLO_lep_13TeV = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_product( mu_WZZ_NLO_13TeV, BR_WZZ_lep , linear_only=True),
      'mu_WZZ_13TeV'
    )

    ## ZZZ
    ### production
    mu_ZZZ_NLO_13TeV = EFTInternalParameter.from_json(json_path+'mu_ZZZ_NLO_13TeV.json', linear_only=True)
    ### leptonic branching ratio
    BR_ZZZ_lep = ( mu_BR_Z_ll.power(3.) )
    ### production * leptonic branching ratio
    mu_ZZZ_NLO_lep_13TeV = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_product( mu_ZZZ_NLO_13TeV, BR_ZZZ_lep, linear_only=True ),
      'mu_ZZZ_13TeV'
    )
    
    ## WWa
    mu_WWa_NLO_13TeV = EFTInternalParameter.from_json(json_path+'mu_WWA_NLO_13TeV.json', linear_only=True)
    BR_WWa_lep = mu_BR_W_lv.power(2.) 
    ### production * leptonic branching ratio
    mu_WWa_NLO_lep_13TeV = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_product( mu_WWa_NLO_13TeV, BR_WWa_lep, linear_only=True ),
      'mu_WWa_13TeV'
    )
    
    ## WZa
    mu_WZa_NLO_13TeV = EFTInternalParameter.from_json(json_path+'mu_WZA_NLO_13TeV.json', linear_only=True)
    ### production * leptonic branching ratio
    BR_WZa_lep = mu_BR_W_lv.product( mu_BR_Z_ll )
    mu_WZa_NLO_lep_13TeV = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_product( mu_WZa_NLO_13TeV, BR_WZa_lep, linear_only=True ),
      'mu_WZa_13TeV'
    )
    
    ## Waa
    mu_Waa_NLO_13TeV = EFTInternalParameter.from_json(json_path+'mu_WAA_NLO_13TeV.json', linear_only=True)
    ### production * leptonic branching ratio
    mu_Waa_NLO_lep_13TeV = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_product( mu_Waa_NLO_13TeV, mu_BR_W_lv, linear_only=True),
      'mu_Waa_13TeV'
    )
    
class LHC_LO(V_Decays):
    # leptonic BRs
    mu_BR_W_lv = V_Decays.mu_BR_W_lv
    mu_BR_Z_ll = V_Decays.mu_BR_Z_ll
    
    # LHC 13 TeV VVV signal strengths
    ## WWW
    ### production
    mu_WWW_LO_13TeV = EFTInternalParameter.from_json(json_path+'mu_WWW_LO_13TeV.json')
    ### leptonic branching ratio
    BR_WWW_lep = mu_BR_W_lv.power(3.)
    ### production * leptonic branching ratio
    mu_WWW_LO_lep_13TeV = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_product( mu_WWW_LO_13TeV, BR_WWW_lep ),
      'mu_WWW_13TeV'
    )
    
    ## WWZ
    ### production
    mu_WWZ_LO_13TeV = EFTInternalParameter.from_json(json_path+'mu_WWZ_LO_13TeV.json')
    ### leptonic branching ratio
    BR_WWZ_lep = ( mu_BR_W_lv.power(2.) ).product( mu_BR_Z_ll )
    ### production * leptonic branching ratio
    mu_WWZ_LO_lep_13TeV = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_product( mu_WWZ_LO_13TeV, BR_WWZ_lep ),
      'mu_WWZ_13TeV'
    )
    
    ## WZZ
    ### production
    mu_WZZ_LO_13TeV = EFTInternalParameter.from_json(json_path+'mu_WZZ_LO_13TeV.json')
    ### leptonic branching ratio
    BR_WZZ_lep = ( mu_BR_Z_ll.power(2.) ).product( mu_BR_W_lv )
    ### production * leptonic branching ratio
    mu_WZZ_LO_lep_13TeV = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_product( mu_WZZ_LO_13TeV, BR_WZZ_lep ),
      'mu_WZZ_13TeV'
    )

    ## ZZZ
    ### production
    mu_ZZZ_LO_13TeV = EFTInternalParameter.from_json(json_path+'mu_ZZZ_LO_13TeV.json')
    ### leptonic branching ratio
    BR_ZZZ_lep = ( mu_BR_Z_ll.power(3.) )
    ### production * leptonic branching ratio
    mu_ZZZ_LO_lep_13TeV = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_product( mu_ZZZ_LO_13TeV, BR_ZZZ_lep ),
      'mu_ZZZ_13TeV'
    )
    
    ## WWa
    mu_WWa_LO_13TeV = EFTInternalParameter.from_json(json_path+'mu_WWA_LO_13TeV.json')
    BR_WWa_lep = mu_BR_W_lv.power(2.) 
    ### production * leptonic branching ratio
    mu_WWa_LO_lep_13TeV = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_product( mu_WWa_LO_13TeV, BR_WWa_lep ),
      'mu_WWa_13TeV'
    )
    
    ## WZa
    mu_WZa_LO_13TeV = EFTInternalParameter.from_json(json_path+'mu_WZA_LO_13TeV.json')
    ### production * leptonic branching ratio
    BR_WZa_lep = mu_BR_W_lv.product( mu_BR_Z_ll )
    mu_WZa_LO_lep_13TeV = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_product( mu_WZa_LO_13TeV, BR_WZa_lep ),
      'mu_WZa_13TeV'
    )
    
    ## Waa
    mu_Waa_LO_13TeV = EFTInternalParameter.from_json(json_path+'mu_WAA_LO_13TeV.json')
    ### production * leptonic branching ratio
    mu_Waa_LO_lep_13TeV = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_product( mu_Waa_LO_13TeV, mu_BR_W_lv ),
      'mu_Waa_13TeV'
    )
      

class LHC_LO_lin(V_Decays_lin):
    # leptonic BRs
    mu_BR_W_lv = V_Decays_lin.mu_BR_W_lv
    mu_BR_Z_ll = V_Decays_lin.mu_BR_Z_ll
    
    # LHC 13 TeV VVV signal strengths
    ## WWW
    ### production
    mu_WWW_LO_13TeV = EFTInternalParameter.from_json(json_path+'mu_WWW_LO_13TeV.json', linear_only=True)
    ### leptonic branching ratio
    BR_WWW_lep = mu_BR_W_lv.power(3.)
    ### production * leptonic branching ratio
    mu_WWW_LO_lep_13TeV = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_product( mu_WWW_LO_13TeV, BR_WWW_lep, linear_only=True ),
      'mu_WWW_13TeV'
    )
    
    ## WWZ
    ### production
    mu_WWZ_LO_13TeV = EFTInternalParameter.from_json(json_path+'mu_WWZ_LO_13TeV.json', linear_only=True)
    ### leptonic branching ratio
    BR_WWZ_lep = ( mu_BR_W_lv.power(2.) ).product( mu_BR_Z_ll )
    ### production * leptonic branching ratio
    mu_WWZ_LO_lep_13TeV = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_product( mu_WWZ_LO_13TeV, BR_WWZ_lep, linear_only=True ),
      'mu_WWZ_13TeV'
    )
    
    ## WZZ
    ### production
    mu_WZZ_LO_13TeV = EFTInternalParameter.from_json(json_path+'mu_WZZ_LO_13TeV.json', linear_only=True)
    ### leptonic branching ratio
    BR_WZZ_lep = ( mu_BR_Z_ll.power(2.) ).product( mu_BR_W_lv )
    ### production * leptonic branching ratio
    mu_WZZ_LO_lep_13TeV = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_product( mu_WZZ_LO_13TeV, BR_WZZ_lep , linear_only=True),
      'mu_WZZ_13TeV'
    )

    ## ZZZ
    ### production
    mu_ZZZ_LO_13TeV = EFTInternalParameter.from_json(json_path+'mu_ZZZ_LO_13TeV.json', linear_only=True)
    ### leptonic branching ratio
    BR_ZZZ_lep = ( mu_BR_Z_ll.power(3.) )
    ### production * leptonic branching ratio
    mu_ZZZ_LO_lep_13TeV = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_product( mu_ZZZ_LO_13TeV, BR_ZZZ_lep, linear_only=True ),
      'mu_ZZZ_13TeV'
    )
    
    ## WWa
    mu_WWa_LO_13TeV = EFTInternalParameter.from_json(json_path+'mu_WWA_LO_13TeV.json', linear_only=True)
    BR_WWa_lep = mu_BR_W_lv.power(2.) 
    ### production * leptonic branching ratio
    mu_WWa_LO_lep_13TeV = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_product( mu_WWa_LO_13TeV, BR_WWa_lep, linear_only=True ),
      'mu_WWa_13TeV'
    )
    
    ## WZa
    mu_WZa_LO_13TeV = EFTInternalParameter.from_json(json_path+'mu_WZA_LO_13TeV.json', linear_only=True)
    ### production * leptonic branching ratio
    BR_WZa_lep = mu_BR_W_lv.product( mu_BR_Z_ll )
    mu_WZa_LO_lep_13TeV = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_product( mu_WZa_LO_13TeV, BR_WZa_lep, linear_only=True ),
      'mu_WZa_13TeV'
    )
    
    ## Waa
    mu_Waa_LO_13TeV = EFTInternalParameter.from_json(json_path+'mu_WAA_LO_13TeV.json', linear_only=True)
    ### production * leptonic branching ratio
    mu_Waa_LO_lep_13TeV = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_product( mu_Waa_LO_13TeV, mu_BR_W_lv, linear_only=True),
      'mu_Waa_13TeV'
    )

