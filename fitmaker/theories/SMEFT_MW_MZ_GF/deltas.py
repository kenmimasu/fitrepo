import numpy as np
from numpy import sqrt
from ...fitlib.constants import pi, GeV2pb 
from ...fitlib.theory import internal_parameter, prediction

# from ..SM.MW_MZ_GF_5F.inputs import MW_MZ_GF_5F_Fixed as SM_MW_MZ_GF_5F_Fixed
from ..SM.MW_MZ_GF_5F.EWPO import Tree_Fixed as SM_MW_MZ_GF_5F_Fixed, LEP_EWWG as SM_MW_MZ_GF_5F_EWPO

from ..EFT import EFTBase
from .inputs import MW_MZ_GF_5F_U2_2_U3_3

# Generic 'core shifts'
class Deltas(SM_MW_MZ_GF_5F_Fixed):
    '''
    Model of SM plus EW parameter and coupling shifts, based on 
    SMEFT_global_fit_warsaw.nb.
    Modified for b-quark specific couplings in U(2)^2 x U(3)^5 flavor scenario.
    '''
    external_parameters = [
      # EW parameter
      'dGF', 'dMZ2', 'dMW2', 'dv', 'dsw2', 'daEW',
      # dGF = delta(GF)/GF (dimensionless)
      # daEW = delta(aEW)/aEW == 2 delta(ee)/ee
      
      # Z couplings
      'dgZ', 'dgVl', 'dgAl', 'dgVv', 'dgAv',
      'dgVu', 'dgAu', 'dgVd', 'dgAd', 
      'dgVb', 'dgAb', # b-specific
      # W couplings
      'dgVWl', 'dgVWq',
      # TGCs
      'dg1A', 'dg1Z', 'dkA', 'dkZ', 'dlamA', 'dlamZ'
    ]
    
    @internal_parameter('daEW')
    def dee(self):
        return 2.*self['daEW']
    
    @internal_parameter('dgVWl')
    def dgAWl(self):
        return self['dgVWl']
        
    @internal_parameter('dgVWq')
    def dgAWq(self):
        return self['dgVWq']
    
    @internal_parameter(depends_on=('Gam_Z_fac', 'sw', 'dgAl', 'dgVl'))
    def dGam_Z_ll(self):
        cgA, cgV = -1., ( -1. + 4.*self['sw']**2 )
        return self['Gam_Z_fac']/6.*(cgA*self['dgAl'] +  cgV*self['dgVl'])
    
    @internal_parameter(depends_on=('Gam_Z_fac', 'dgAv', 'dgVv'))
    def dGam_Z_vv(self):
        return self['Gam_Z_fac']/6.*(self['dgAv'] +  self['dgVv'])
    
    @internal_parameter(depends_on=('Gam_Z_fac', 'sw', 'dgAu', 'dgVu'))
    def dGam_Z_uu(self):
        cgA, cgV = 1., ( 1. - (8./3.)*self['sw']**2 )
        return self['Gam_Z_fac']/2.*(cgA*self['dgAu'] +  cgV*self['dgVu'])
    
    @internal_parameter(depends_on=('Gam_Z_fac', 'sw', 'dgAd', 'dgVd'))
    def dGam_Z_dd(self):
        cgA, cgV = -1., ( -1. + (4./3.)*self['sw']**2 )
        return self['Gam_Z_fac']/2.*(cgA*self['dgAd'] +  cgV*self['dgVd'])
    
    @internal_parameter(depends_on=('Gam_Z_fac', 'sw', 'dgAb', 'dgVb'))
    def dGam_Z_bb(self):
        cgA, cgV = -1., ( -1. + (4./3.)*self['sw']**2 )
        return self['Gam_Z_fac']/2.*(cgA*self['dgAb'] +  cgV*self['dgVb'])
    
    @internal_parameter(depends_on=('dGam_Z_uu', 'dGam_Z_dd', 'dGam_Z_bb'))
    def dGam_Z_had(self):
        return 2.*(self['dGam_Z_uu'] + self['dGam_Z_dd']) + self['dGam_Z_bb']
    
    @internal_parameter(depends_on=('dGam_Z_had', 'dGam_Z_ll', 'dGam_Z_vv'))
    def dGam_Z(self):
        return 3.*self['dGam_Z_ll'] + 3.*self['dGam_Z_vv'] + self['dGam_Z_had']
    
    @internal_parameter(depends_on=('MW', 'dMW2', 'Gam_W_qq_SM', 'dgVWq'))
    def dGam_W_qq(self):
        return self['Gam_W_qq_SM']*(4.*self['dgVWq'] + self['dMW2']/self['MW']**2/2.)
        
    @internal_parameter(depends_on=('MW', 'dMW2', 'Gam_W_lv_SM', 'dgVWl'))
    def dGam_W_lv(self):
        return self['Gam_W_lv_SM']*(4.*self['dgVWl'] + self['dMW2']/self['MW']**2/2.)
    
    @internal_parameter(depends_on=('MW','dMW2','Gam_W_SM', 'dgVWl', 'dgVWq'))
    def dGam_W(self):
        return self['Gam_W_SM']*(
          (4./3.)*self['dgVWl'] + (8./3.)*self['dgVWq'] +
          self['dMW2']/self['MW']**2/2.
        )
    
    
    
# Map of Warsaw basis coefficients to core shifts defined in 1701.06424
class SMEFT_Deltas(EFTBase, Deltas, MW_MZ_GF_5F_U2_2_U3_3):
    '''
    SMEFT Warsaw basis for EWPO, based on "1701.06424". Maps
    Wilson coefficients at linear order to the parameter and coupling shifts
    of SM_deltas.
    '''

    # EW param shifts
    @internal_parameter(depends_on=('GF', 'CHl3', 'Cll'))
    def dGF(self):
        # Eqn 3.4
        return (self['CHl3']-self['Cll']/2.)/self['GF']/self.Lambda**2

    @internal_parameter(depends_on=('MZ','GF','cw','sw','CHD','CHWB'))
    def dMZ2(self):
        # Eqn 3.5
        aHD = self['MZ']**2/(2.*sqrt(2.)*self['GF'])
        aHWB = self['MZ']**2*sqrt(2.)/self['GF']*self['cw']*self['sw']
        return (aHD*self['CHD'] +  aHWB*self['CHWB'])/self.Lambda**2

    @internal_parameter()
    def dMW2(self):
        # Eqn 3.6
       return 0.

    @internal_parameter(depends_on=('GF','dGF'))
    def dv(self):
        return self['dGF']/self['GF']/2.

    # @internal_parameter(depends_on=('vev','c2w','s2w','sw','gp','CHWB','CHD','Cll','CHl3'))
    # def dgp(self):
    #     fac = self['gp']*self['vev']**2/self['c2w']/4.
    #     Cbar = self['CHD'] + 4.*self['CHl3'] - 2.*self['Cll']
    #     return fac*( self['sw']**2*Cbar + 2.*self['s2w']*self['CHWB'] )/self.Lambda**2
    #
    # @internal_parameter(depends_on=('vev','c2w','s2w','cw','gw','CHWB','CHD','Cll','CHl3'))
    # def dgw(self):
    #     fac = -self['gw']*self['vev']**2/self['c2w']/4.
    #     Cbar = self['CHD'] + 4.*self['CHl3'] - 2.*self['Cll']
    #     return fac*( self['cw']**2*Cbar + 2.*self['s2w']*self['CHWB'] )/self.Lambda**2

    @internal_parameter(depends_on=('GF','cw','sw','CHWB','CHD'))
    def dsw2(self):
        # Eqn 3.7, sign change w.r.t 1701.06424 to match 1706.08945 def'n
        # validated against 2012.11343
        fac = -self['cw']/(sqrt(2.)*self['GF'])
        return fac*( 
          self['cw']*self['CHD']/2. + self['sw']*self['CHWB'] 
        )/self.Lambda**2

    # Z coupling shifts
    @internal_parameter(depends_on=('MZ','GF','sw','cw','dMZ2','dGF','CHWB'))
    def dgZ(self):
    # 2020/06/05: sign error fixed in second term, now matches 1706.08945
    # Validated against SMEFTsim & SMEFT_MW_MZ_GF
        return (
          - self['dGF']/sqrt(2.) - self['dMZ2']/(2.*self['MZ']**2)
          + self['sw']*self['cw']/(sqrt(2.)*self['GF'])*self['CHWB']/self.Lambda**2
        )

    @internal_parameter(depends_on=('GF','gVlSM','dgZ','dsw2','CHe','CHl1','CHl3'))
    def dgVl(self):
        return (
          self['dgZ']*self['gVlSM']
          - (self['CHe']+self['CHl1']+self['CHl3'])/(4.*sqrt(2.)*self['GF'])/self.Lambda**2
          + self['dsw2']
        )

    @internal_parameter(depends_on=('GF','gAlSM','dgZ','CHe','CHl1','CHl3'))
    def dgAl(self):
        return (
          self['dgZ']*self['gAlSM']
          + (self['CHe']-self['CHl1']-self['CHl3'])/(4.*sqrt(2.)*self['GF'])/self.Lambda**2
        )

    @internal_parameter(depends_on=('GF','gVvSM','dgZ','CHl1','CHl3'))
    def dgVv(self):
        return (
          self['dgZ']*self['gVvSM']
          - (self['CHl1']-self['CHl3'])/(4.*sqrt(2.)*self['GF'])/self.Lambda**2
        )

    @internal_parameter(depends_on=('dgVv',))
    def dgAv(self):
        return self['dgVv']

    @internal_parameter(depends_on=('GF','gVuSM','dgZ','dsw2','CHu','CHq1','CHq3'))
    def dgVu(self):
        return (
          self['dgZ']*self['gVuSM']
          + (-self['CHq1']+self['CHq3']-self['CHu'])/(4.*sqrt(2.)*self['GF'])/self.Lambda**2
          - (2./3.)*self['dsw2']
        )

    @internal_parameter(depends_on=('GF','gAuSM','dgZ','CHu','CHq1','CHq3'))
    def dgAu(self):
        return (
          self['dgZ']*self['gAuSM']
          - (self['CHq1']-self['CHq3']-self['CHu'])/(4.*sqrt(2.)*self['GF'])/self.Lambda**2
        )

    @internal_parameter(depends_on=('GF','gVuSM','dgZ','dsw2','CHt','CHQ1','CHQ3'))
    def dgVt(self):
        return (
          self['dgZ']*self['gVuSM']
          + (-self['CHQ1']+self['CHQ3']-self['CHt'])/(4.*sqrt(2.)*self['GF'])/self.Lambda**2
          - (2./3.)*self['dsw2']
        )

    @internal_parameter(depends_on=('GF','gAuSM','dgZ','CHt','CHQ1','CHQ3'))
    def dgAt(self):
        return (
          self['dgZ']*self['gAuSM']
          - (self['CHQ1']-self['CHQ3']-self['CHt'])/(4.*sqrt(2.)*self['GF'])/self.Lambda**2
        )

    @internal_parameter(depends_on=('GF','gVdSM','dgZ','dsw2','CHd','CHq1','CHq3'))
    def dgVd(self):
        return (
          self['dgZ']*self['gVdSM']
          - (self['CHq1']+self['CHq3']+self['CHd'])/(4.*sqrt(2.)*self['GF'])/self.Lambda**2
          + 1./3.*self['dsw2']
        )

    @internal_parameter(depends_on=('GF','gAdSM','dgZ','CHd','CHq1','CHq3'))
    def dgAd(self):
        return (
          self['dgZ']*self['gAdSM']
          + (-self['CHq1']-self['CHq3']+self['CHd'])/(4.*sqrt(2.)*self['GF'])/self.Lambda**2
        )

    @internal_parameter(depends_on=('GF','gVdSM','dgZ','dsw2','CHd','CHQ1','CHQ3'))
    def dgVb(self):
        return (
          self['dgZ']*self['gVdSM']
          - (self['CHQ1']+self['CHQ3']+self['CHd'])/(4.*sqrt(2.)*self['GF'])/self.Lambda**2
          + 1./3.*self['dsw2']
        )

    @internal_parameter(depends_on=('GF','gAdSM','dgZ','CHd','CHQ1','CHQ3'))
    def dgAb(self):
        return (
          self['dgZ']*self['gAdSM']
          + (-self['CHQ1']-self['CHQ3']+self['CHd'])/(4.*sqrt(2.)*self['GF'])/self.Lambda**2
        )
    # relative fine structure constant shift
    @internal_parameter(depends_on=('GF','dGF','dMZ2','MZ','cw','sw','CHWB'))
    def daEW(self):
        # Eqn 3.20 ! verified numerically !
        return - (  
          sqrt(2.)*self['dGF'] 
        + self['dMZ2']/self['MZ']**2*(self['cw']/self['sw'])**2
        + sqrt(2.)*self['cw']*self['sw']/self['GF']*self['CHWB']/self.Lambda**2
        )
        
    # W coupling shifts
    @internal_parameter(depends_on=('GF','dGF','CHl3'))
    def dgVWl(self):
        return (
          self['CHl3']/self['GF']/self.Lambda**2 - self['dGF']
        )/(2.*sqrt(2.))
        
    @internal_parameter(depends_on=('GF','dGF','CHq3'))
    def dgVWq(self):
        return (
          self['CHq3']/self['GF']/self.Lambda**2 - self['dGF']
        )/(2.*sqrt(2.))
        
    # TGC shifts
    @internal_parameter(depends_on=('GF','dGF','dsw2','sw','cw','CHWB'))
    def dg1A(self):
        return -1./sqrt(2)*(
          self['dGF'] - 1./sqrt(2)*self['dsw2']/self['sw']**2
        + 1./(2.*self['GF'])*self['cw']/self['sw']*self['CHWB']/self.Lambda**2
        )

    @internal_parameter(depends_on=('GF','dGF','dsw2','sw','cw','CHWB'))
    def dg1Z(self):
        return -1./sqrt(2)*(
          self['dGF'] + 1./sqrt(2)*self['dsw2']/self['cw']**2
        - 1./(2.*self['GF'])*self['sw']/self['cw']*self['CHWB']/self.Lambda**2
        )

    @internal_parameter(depends_on=('GF','dGF','cw','sw','CHD'))
    def dkA(self):
        return -1./(4.*sqrt(2))*(
          1./self['GF']*(self['cw']/self['sw'])**2*self['CHD']/self.Lambda**2 
        + 4.*self['dGF'] 
        )
        
    @internal_parameter(depends_on=('GF','dGF','CHD'))
    def dkZ(self):
        return 1./(4.*sqrt(2))*(
          1./self['GF']*self['CHD']/self.Lambda**2 - 4.*self['dGF'] 
        )

    @internal_parameter(depends_on=('MW','sw','gAWWSM','CW'))
    def dlamA(self):
        return 6.*self['sw']*self['MW']**2/self['gAWWSM']*self['CW']/self.Lambda**2

    @internal_parameter(depends_on=('MW','cw','gZWWSM','CW'))
    def dlamZ(self):
        return 6.*self['cw']*self['MW']**2/self['gZWWSM']*self['CW']/self.Lambda**2
