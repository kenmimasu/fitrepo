import numpy as np
from numpy import sqrt
from ...fitlib.constants import pi, GeV2pb 
from ...fitlib.theory import internal_parameter, prediction

from ..SM.MW_MZ_GF_5F.EWPO import Tree_Fixed as SM_MW_MZ_GF_5F_Fixed, LEP_EWWG as SM_MW_MZ_GF_5F_EWPO
from ..SM.MW_MZ_GF_5F.diboson import LEP as SM_MW_MZ_GF_5F_Diboson_LEP

from ..EFT import EFTBase, EFTPrediction
from .deltas import Deltas, SMEFT_Deltas
from .inputs import SMEFT_MW_MZ_GF_WB, SMEFT_MW_MZ_GF, SMEFT_MW_MZ_GF_U2_2_U3_3, SMEFT_MW_MZ_GF_U3_5

# EWPO in terms of 'core shifts'
class Deltas_EWPO(Deltas, SM_MW_MZ_GF_5F_EWPO):
    '''
    Model of SM plus EW parameter and coupling shifts, based on 
    SMEFT_global_fit_warsaw.nb. Predictions for LEP Z-pole observables in terms 
    of the shifts at linear order. Validated against independent SMEFiT 
    implementation.
    '''
    
    @prediction('mu_aEWM1_MZ_MSbar')
    def mu_aEWM1_MZ(self):
        '''Relative shift in 1/aEW(MZ) w.r.t. SM.'''
        return (1.- self['daEW'])
    
    @prediction('Gam_Z')
    def Gam_Z(self):
        '''Z width.'''
        Gam_Z_SM = self.predict_baseclass(SM_MW_MZ_GF_5F_EWPO, 'Gam_Z')
        return  Gam_Z_SM + self['dGam_Z']
    
    @prediction('sigma_had_0')
    def sigma_had_0(self):
        '''Hadronic cross section [nb].'''
        sigma_SM = self.predict_baseclass(SM_MW_MZ_GF_5F_EWPO, 'sigma_had_0')
        
        fac = GeV2pb*12.*pi/self['MZ']**2/self['Gam_Z_SM']**3/1000.
        
        dsigma = fac*(
          self['Gam_Z_had_SM']*self['Gam_Z_SM']*self['dGam_Z_ll'] +
          self['Gam_Z_ll_SM']*self['Gam_Z_SM']*self['dGam_Z_had'] -
          2.*self['Gam_Z_had_SM']*self['Gam_Z_ll_SM']*self['dGam_Z'] 
        )

        return sigma_SM + dsigma
    
    @prediction('Rl_0')
    def Rl_0(self):
        '''Hadronic to leptonic cross section ratio.'''
        Rl_SM = self.predict_baseclass(SM_MW_MZ_GF_5F_EWPO, 'Rl_0')
        
        dRl = (
          self['Gam_Z_ll_SM']*self['dGam_Z_had'] - 
          self['Gam_Z_had_SM']*self['dGam_Z_ll']
        )/self['Gam_Z_ll_SM']**2

        return Rl_SM + dRl
        
    @prediction('Rb_0')
    def Rb_0(self):
        '''b-quark to hadronic cross section ratio.'''
        Rb_SM = self.predict_baseclass(SM_MW_MZ_GF_5F_EWPO, 'Rb_0')
        
        dRb = (
          self['Gam_Z_had_SM']*self['dGam_Z_bb'] - 
          self['Gam_Z_dd_SM']*self['dGam_Z_had']
        )/self['Gam_Z_had_SM']**2

        return Rb_SM + dRb
        
    @prediction('Rc_0')
    def Rc_0(self):
        '''c-quark to hadronic cross section ratio.'''
        Rc_SM = self.predict_baseclass(SM_MW_MZ_GF_5F_EWPO, 'Rc_0')
        
        dRc = (
          self['Gam_Z_had_SM']*self['dGam_Z_uu'] - 
          self['Gam_Z_uu_SM']*self['dGam_Z_had']
        )/self['Gam_Z_had_SM']**2

        return Rc_SM + dRc
    
    @internal_parameter(depends_on=('gVlSM','gAlSM','dgAl','dgVl'))
    def dAl(self):
        gV, gA = self['gVlSM'],  self['gAlSM']
        fac = 2.*(gV**2 - gA**2)/(gV**2 + gA**2)**2
        return fac*( gV*self['dgAl'] - gA*self['dgVl'] )
                
    @prediction('Al')
    def Al(self):
        '''lepton chiral coupling parameter.'''
        A = self.predict_baseclass(SM_MW_MZ_GF_5F_EWPO, 'Al')
        return A + self['dAl']
    
    @internal_parameter(depends_on=('gVdSM','gAdSM','dgAb','dgVb'))
    def dAb(self):
        gV, gA = self['gVdSM'],  self['gAdSM']
        fac = 2.*(gV**2 - gA**2)/(gV**2 + gA**2)**2
        return fac*( gV*self['dgAb'] - gA*self['dgVb'] )
        
    @prediction('Ab')
    def Ab(self):
        '''b-quark chiral coupling parameter.'''
        A = self.predict_baseclass(SM_MW_MZ_GF_5F_EWPO, 'Ab')
        return A + self['dAb']
    
    @internal_parameter(depends_on=('gVuSM','gAuSM','dgAu','dgVu'))
    def dAc(self):
        gV, gA = self['gVuSM'],  self['gAuSM']
        fac = 2.*(gV**2 - gA**2)/(gV**2 + gA**2)**2
        return fac*( gV*self['dgAu'] - gA*self['dgVu'] )
        
    @prediction('Ac')
    def Ac(self):
        '''c-quark chiral coupling parameter.'''
        A = self.predict_baseclass(SM_MW_MZ_GF_5F_EWPO, 'Ac')
        return A + self['dAc']
        
    @prediction('AFB_l')
    def AFB_l(self):
        '''Leptonic forward-backward asymmetry.'''
        AFB = self.predict_baseclass(SM_MW_MZ_GF_5F_EWPO, 'AFB_l')
        return AFB + (3./2.)*self.Al_SM()*self['dAl']
    
    @prediction('AFB_b')
    def AFB_b(self):
        '''b-quark forward-backward asymmetry.'''
        AFB = self.predict_baseclass(SM_MW_MZ_GF_5F_EWPO, 'AFB_b')
        return AFB + (3./4.)*(self.Al_SM()*self['dAb']+self.Ab_SM()*self['dAl'])
        
    @prediction('AFB_c')
    def AFB_c(self):
        '''c-quark forward-backward asymmetry.'''
        AFB = self.predict_baseclass(SM_MW_MZ_GF_5F_EWPO, 'AFB_c')
        return AFB + (3./4.)*(self.Al_SM()*self['dAc']+self.Ac_SM()*self['dAl'])

# EWPO in terms of Warsaw basis
class SMEFT_EWPO_old(SMEFT_Deltas, Deltas_EWPO):
    '''
    SMEFT Warsaw basis for EWPO, based on "SMEFT_global_fit_warsaw.nb".
    Core shifts + map from Warsaw basis.
    '''
    pass


# EWPO in terms of SMEFT_MW_MZ_GF/dim6top basis
class SMEFT_MW_MZ_GF_EWPO_old(SMEFT_Deltas, Deltas_EWPO, SMEFT_MW_MZ_GF_WB):
    '''
    SMEFT Warsaw basis translated to receive SMEFT_MW_MZ_GF inputs.
    '''
    pass

from . import data_path
from ..SMEFiT import data_path as data_path_SF

json_path = data_path_SF+'EWPO/'
dummy_path = data_path+'dummy/'
# EWPO dependence as determined by SMEFiT 
# (linear terms validated against above implementation)
class SMEFT_MW_MZ_GF_EWPO(EFTBase, SMEFT_MW_MZ_GF):
    '''
    SMEFT Warsaw basis translated to receive SMEFT_MW_MZ_GF inputs.
    '''
    mu_aEWM1_MZ_MSbar = EFTPrediction.from_json(
      json_path+'mu_aEWM1_MZ_MSbar.json'
    )
    alpha_MZ_MSbar = EFTPrediction.from_json(
      json_path+'alpha_MZ_MSbar.json'
    )      
    LEP1_EWPOs_2006 = EFTPrediction.from_json(
      json_path+'LEP1_EWPOs_2006.json'
    )


SMEFT_MW_MZ_GF_EWPO.add_predictions_from_json(json_path+'SMEFiT_EWPO.json')

class SMEFT_MW_MZ_GF_U2_2_U3_3_EWPO(SMEFT_MW_MZ_GF_EWPO, SMEFT_MW_MZ_GF_U2_2_U3_3):
    '''
    Lepton flavor universal version, adding corresponding observables.
    '''
    @prediction('Rl_0')
    def Rl_0(self):
        return self.predict('Re_0')
    
    @prediction('Al')
    def Al(self):
        return self.predict('Ae')
    
    @prediction('AFB_l')
    def AFB_l(self):
        return self.predict('AFB_e')

class SMEFT_MW_MZ_GF_U3_5_EWPO(SMEFT_MW_MZ_GF_U2_2_U3_3_EWPO, SMEFT_MW_MZ_GF_U3_5):
    '''
    Fully flavor universal version.
    '''
    pass
    
# linear only versions
class SMEFT_MW_MZ_GF_EWPO_lin(EFTBase, SMEFT_MW_MZ_GF):
    '''
    SMEFT Warsaw basis translated to receive SMEFT_MW_MZ_GF inputs.
    '''
    mu_aEWM1_MZ_MSbar = EFTPrediction.from_json(
      json_path+'mu_aEWM1_MZ_MSbar.json', linear_only=True
    )
    alpha_MZ_MSbar = EFTPrediction.from_json(
      json_path+'alpha_MZ_MSbar.json', linear_only=True
    )      
    LEP1_EWPOs_2006 = EFTPrediction.from_json(
      json_path+'LEP1_EWPOs_2006.json', linear_only=True
    )


SMEFT_MW_MZ_GF_EWPO_lin.add_predictions_from_json(json_path+'SMEFiT_EWPO.json', linear_only=True)

class SMEFT_MW_MZ_GF_U2_2_U3_3_EWPO_lin(SMEFT_MW_MZ_GF_EWPO_lin, SMEFT_MW_MZ_GF_U2_2_U3_3):
    '''
    Lepton flavor universal version, adding corresponding observables.
    '''
    @prediction('Rl_0')
    def Rl_0(self):
        return self.predict('Re_0')
    
    @prediction('Al')
    def Al(self):
        return self.predict('Ae')
    
    @prediction('AFB_l')
    def AFB_l(self):
        return self.predict('AFB_e')

class SMEFT_MW_MZ_GF_U3_5_EWPO_lin(SMEFT_MW_MZ_GF_U2_2_U3_3_EWPO_lin, SMEFT_MW_MZ_GF_U3_5):
    '''
    Fully flavor universal version.
    '''
    pass

