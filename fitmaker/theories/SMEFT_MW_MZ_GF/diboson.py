import numpy as np
from numpy import sqrt
from ...fitlib.constants import pi, GeV2pb 
from ...fitlib.theory import internal_parameter, prediction

from ..SM.MW_MZ_GF_5F.diboson import LEP as SM_MW_MZ_GF_5F_Diboson_LEP

from ..EFT import EFTBase, EFTInternalParameter
from .inputs import SMEFT_MW_MZ_GF
from .deltas import Deltas, SMEFT_Deltas

# LEP Diboson in terms of 'core shifts'
class Deltas_Diboson_LEP(Deltas, SM_MW_MZ_GF_5F_Diboson_LEP):
    '''
    Model of SM plus EW parameter and coupling shifts, based on
    SMEFT_global_fit_warsaw.nb. Predictions for LEP Diboson observables in terms
    of the shifts at linear order.
    '''
    # W+W- CC03 cross section predictions from paramter shifts (see 1606.06693)
    def dCC03_vec(self, i, f):
        '''
        Vector of parameter shifts needed to obtain predictions for LEP II
        W+W- (CC03) cross section measurements from 1701.06424, Table 3.
        Energies: 188.6, 191.6, 195.5, 199.5, 201.6, 204.8, 206.5, 208.0 GeV
        '''
        return np.array([
          self['dGam_W']/self['Gam_W_SM'],
          self['dgVWl'],
          (self['dgVW'+i]+self['dgVW'+f])/2.,
          self['dgVl'], self['dgAl'],
          self['dg1Z'], self['dkA'], self['dkZ'], 
          self['dlamA'], self['dlamZ'],
          self['dg1A'], self['dee']
        ])

    def dCC03_coeffs(self):
        '''
        Matrix of coefficients needed to obtain predictions for LEP II
        W+W- cross section measurements from 1701.06424, Table 3.
        Energies: 188.6, 191.6, 195.5, 199.5, 201.6, 204.8, 206.5, 208.0 GeV
        '''
        return np.array([
          [-17.,  72.,  33.4,  5.72,  0.21, -0.05, -0.57, -0.16, -0.34,  0.051, -0.41, -0.98],
          [-17.,  72.,  33.6,  6.26,  0.33, -0.07, -0.64, -0.19, -0.37,  0.045, -0.44, -1.08],
          [-17.,  73.,  33.8,  6.91,  0.50, -0.09, -0.72, -0.22, -0.41,  0.035, -0.49, -1.20],
          [-17.,  74.,  33.7,  7.52,  0.68, -0.11, -0.79, -0.26, -0.45,  0.022, -0.53, -1.33],
          [-17.,  74.,  33.7,  7.82,  0.78, -0.12, -0.83, -0.28, -0.47,  0.016, -0.55, -1.39],
          [-17.,  74.,  33.5,  8.24,  0.93, -0.14, -0.89, -0.32, -0.47,  0.005, -0.58, -1.47],
          [-17.,  75.,  33.4,  8.45,  1.01, -0.15, -0.92, -0.33, -0.51, -0.001, -0.60, -1.52],
          [-17.,  75.,  33.3,  8.62,  1.08, -0.16, -0.94, -0.35, -0.52, -0.007, -0.61, -1.55]
        ])

    @internal_parameter(depends_on=(
      'MW','dMW2','dGam_W','Gam_W_SM','dgVWl','dgVWq','dgVl','dgAl','dg1Z','dkA',
      'dkZ','dlamA','dlamZ'
    ))
    def dCC03_eelvqq(self):
        return self.dCC03_coeffs().dot(self.dCC03_vec('l','q'))

    @internal_parameter(depends_on=(
      'MW','dMW2','dGam_W','Gam_W_SM','dgVWl','dgVWq','dgVl','dgAl','dg1Z','dkA',
      'dkZ','dlamA','dlamZ'
    ))
    def dCC03_eeqqqq(self):
        return self.dCC03_coeffs().dot(self.dCC03_vec('q','q'))*1.01

    @internal_parameter(depends_on=(
      'MW','dMW2','dGam_W','Gam_W_SM','dgVWl','dgVl','dgAl','dg1Z','dkA',
      'dkZ','dlamA','dlamZ'
    ))
    def dCC03_eelvlv(self):
        return np.dot(self.dCC03_coeffs(), self.dCC03_vec('l','l'))/4.04

    @prediction('WW_xs_lvlv_ALEPH')
    def WW_xs_lvlv_ALEPH(self):
        SM = self.predict_baseclass(SM_MW_MZ_GF_5F_Diboson_LEP, 'WW_xs_lvlv_ALEPH')
        return SM + self['dCC03_eelvlv'][:-1]

    @prediction('WW_xs_lvqq_ALEPH')
    def WW_xs_lvqq_ALEPH(self):
        SM = self.predict_baseclass(SM_MW_MZ_GF_5F_Diboson_LEP, 'WW_xs_lvqq_ALEPH')
        return SM + self['dCC03_eelvqq'][:-1]

    @prediction('WW_xs_qqqq_ALEPH')
    def WW_xs_qqqq_ALEPH(self):
        SM = self.predict_baseclass(SM_MW_MZ_GF_5F_Diboson_LEP, 'WW_xs_qqqq_ALEPH')
        return SM + self['dCC03_eeqqqq'][:-1]

    @prediction('WW_xs_lvlv_OPAL')
    def WW_xs_lvlv_OPAL(self):
        SM = self.predict_baseclass(SM_MW_MZ_GF_5F_Diboson_LEP, 'WW_xs_lvlv_OPAL')
        return SM + self['dCC03_eelvlv'][:-1]

    @prediction('WW_xs_lvqq_OPAL')
    def WW_xs_lvqq_OPAL(self):
        SM = self.predict_baseclass(SM_MW_MZ_GF_5F_Diboson_LEP, 'WW_xs_lvqq_OPAL')
        return SM + self['dCC03_eelvqq'][:-1]

    @prediction('WW_xs_qqqq_OPAL')
    def WW_xs_qqqq_OPAL(self):
        SM = self.predict_baseclass(SM_MW_MZ_GF_5F_Diboson_LEP, 'WW_xs_qqqq_OPAL')
        return SM + self['dCC03_eeqqqq'][:-1]

    @prediction('WW_xs_lvlv_L3')
    def WW_xs_lvlv_L3(self):
        SM = self.predict_baseclass(SM_MW_MZ_GF_5F_Diboson_LEP, 'WW_xs_lvlv_L3')
        return SM + self['dCC03_eelvlv']

    @prediction('WW_xs_lvqq_L3')
    def WW_xs_lvqq_L3(self):
        SM = self.predict_baseclass(SM_MW_MZ_GF_5F_Diboson_LEP, 'WW_xs_lvqq_L3')
        return SM + self['dCC03_eelvqq']

    @prediction('WW_xs_qqqq_L3')
    def WW_xs_qqqq_L3(self):
        SM = self.predict_baseclass(SM_MW_MZ_GF_5F_Diboson_LEP, 'WW_xs_qqqq_L3')
        return SM + self['dCC03_eeqqqq']

    # W+W- polar angle differential cross section measurements from LEP
    def dWW_costheta_LEPII_coeffs(self):
        '''
        Matrix of coefficients needed to obtain predictions for LEP II
        W+W- cos(theta) differential cross section measurements.
        Bins 1,4,7 & 10 at 182.66 & 205.92 GeV, from 1701.06424, Table 4.
        '''
        return np.array([
          [  -1.5 ,  12.  ,   2.9 ,  4.3 ,   3.0 ,  -0.42,  -0.37,  -0.45,  -0.35,  -0.43,  -0.34,  -0.71  ],
          [  -2.8 ,  16.  ,   5.4 ,  3.7 ,   2.3 ,  -0.29,  -0.35,  -0.38,  -0.28,  -0.32,  -0.27,  -0.62  ],
          [  -5.2 ,  22.  ,  10.2 ,  1.7 ,   0.2 ,  -0.04,  -0.16,  -0.06,  -0.08,   0.03,  -0.12,  -0.29  ],
          [ -14.1 ,  40.  ,  27.5 , -7.8 ,  -9.0 ,   1.20,   0.67,   1.27,   0.68,   1.27,   0.64,   1.30  ],
          [  -0.9 ,  10.  ,   1.8 ,  4.9 ,   2.9 ,  -0.40,  -0.47,  -0.46,  -0.43,  -0.43,  -0.41,  -0.88  ],
          [  -2.0 ,  15.  ,   4.0 ,  5.1 ,   2.8 ,  -0.31,  -0.57,  -0.51,  -0.40,  -0.38,  -0.35,  -0.92  ],
          [  -4.5 ,  22.  ,   8.8 ,  3.7 ,   1.2 ,  -0.17,  -0.39,  -0.22,  -0.21,  -0.07,  -0.27,  -0.66  ],
          [ -19.9 ,  59.  ,  39.0 , -9.5 , -11.4 ,   1.48,  -0.88,   1.63,   0.93,   1.67,   0.81,   1.69  ]
        ])

    @internal_parameter(depends_on=(
      'MW','dMW2','dGam_W','Gam_W_SM','dgVWl','dgVl','dgAl','dg1Z','dkA',
      'dkZ','dlamA','dlamZ'
    ))
    def dWW_costheta_LEPII(self):
        return np.dot(self.dWW_costheta_LEPII_coeffs(), self.dCC03_vec('l','q'))

    @prediction('WW_costheta_LEPII')
    def WW_costheta_LEPII(self):
        SM = self.predict_baseclass(SM_MW_MZ_GF_5F_Diboson_LEP, 'WW_costheta_LEPII')
        return SM + self['dWW_costheta_LEPII']
        
# LEP Diboson in terms of Warsaw basis
class LEP_old(SMEFT_Deltas, Deltas_Diboson_LEP):
    '''
    SMEFT Warsaw basis for LEP Diboson, based on "SMEFT_global_fit_warsaw.nb".
    Core shifts + map from Warsaw basis.
    '''
    pass

from ..EFT import EFTBase, EFTPrediction, EFTInternalParameter
from .inputs import SMEFT_MW_MZ_GF
from .decays import V_Decays, V_Decays_lin
from . import data_path
json_path = data_path+'diboson/'

# LEP Diboson in terms of Warsaw basis
class LEP(V_Decays, SM_MW_MZ_GF_5F_Diboson_LEP):
    '''
    SMEFT Warsaw basis for LEP Diboson, based on explicit computations with 
    SMEFT_MW_MZ_GF.
    '''
    SM =  SM_MW_MZ_GF_5F_Diboson_LEP()
    # relative shift in W branching fractions
    mu_BR_W_lv = V_Decays.mu_BR_W_lv
    mu_BR_W_qq = V_Decays.mu_BR_W_qq 
    # leptonic WW BR
    BR_WW_lvlv = mu_BR_W_lv.power(2.)
    # semileptonic WW BR
    BR_WW_lvqq = EFTInternalParameter.as_product( mu_BR_W_lv, mu_BR_W_qq )
    # hadronic WW BR
    BR_WW_qqqq = mu_BR_W_qq.power(2.)

    # total e+ e- > W+ W- cross sections [fb] at 8 CoM energies, as measured by LEP experiments
    sigma_WW_1 = EFTInternalParameter.from_json(
      json_path+'sigma_WW_LEP_188.6GeV.json'
    )
    
    sigma_WW_2 = EFTInternalParameter.from_json(
      json_path+'sigma_WW_LEP_191.6GeV.json'
    )
    
    sigma_WW_3 = EFTInternalParameter.from_json(
      json_path+'sigma_WW_LEP_195.5GeV.json'
    )
    
    sigma_WW_4 = EFTInternalParameter.from_json(
      json_path+'sigma_WW_LEP_199.6GeV.json'
    )
    
    sigma_WW_5 = EFTInternalParameter.from_json(
      json_path+'sigma_WW_LEP_201.8GeV.json'
    )
    
    sigma_WW_6 = EFTInternalParameter.from_json(
      json_path+'sigma_WW_LEP_204.8GeV.json'
    )
    
    sigma_WW_7 = EFTInternalParameter.from_json(
      json_path+'sigma_WW_LEP_206.5GeV.json'
    )
    
    sigma_WW_8 = EFTInternalParameter.from_json(
      json_path+'sigma_WW_LEP_208.0GeV.json'
    )
    
    # 7 measurements by OPAL/ALEPH
    sigma_WW_OPAL_ALEPH = EFTInternalParameter.as_concatenation( 
      sigma_WW_1, sigma_WW_2, sigma_WW_3, sigma_WW_4, 
      sigma_WW_5, sigma_WW_6, sigma_WW_7
    )
    # Relative impact
    r_WW_OPAL_ALEPH = sigma_WW_OPAL_ALEPH.normalised_by_constant()

    # 8 measurements by L3      
    sigma_WW_L3 = EFTInternalParameter.as_concatenation( 
      sigma_WW_1, sigma_WW_2, sigma_WW_3, sigma_WW_4, 
      sigma_WW_5, sigma_WW_6, sigma_WW_7, sigma_WW_8
    )
    # Relative impact
    r_WW_L3 = sigma_WW_L3.normalised_by_constant()
    
    # OPAL measurements in lvlv, lvqq, qqqq channels
    # SM predictions
    sigma_WW_qqqq_OPAL_SM = SM.predict('WW_xs_qqqq_OPAL')
    sigma_WW_lvqq_OPAL_SM = SM.predict('WW_xs_lvqq_OPAL')
    sigma_WW_lvlv_OPAL_SM = SM.predict('WW_xs_lvlv_OPAL')
    
    # Include fully leptonic BR
    r_WW_lvlv_OPAL_ALEPH =  EFTInternalParameter.as_product(
      r_WW_OPAL_ALEPH , BR_WW_lvlv
    )
    # Scale by SM prediction (data not reported as signal strengths)
    WW_xs_lvlv_OPAL = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_multiply_float(
        r_WW_lvlv_OPAL_ALEPH, sigma_WW_lvlv_OPAL_SM),      
      'WW_xs_lvlv_OPAL'
    )

    # Include semi-leptonic BR
    r_WW_lvqq_OPAL_ALEPH =  EFTInternalParameter.as_product(
      r_WW_OPAL_ALEPH , BR_WW_lvqq
    )
    # Scale by SM prediction (data not reported as signal strengths)
    WW_xs_lvqq_OPAL = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_multiply_float(
        r_WW_lvqq_OPAL_ALEPH, sigma_WW_lvqq_OPAL_SM),      
      'WW_xs_lvqq_OPAL'
    )
    
    # Include hadronic BR
    r_WW_qqqq_OPAL_ALEPH =  EFTInternalParameter.as_product(
      r_WW_OPAL_ALEPH , BR_WW_qqqq
    )
    # Scale by SM prediction (data not reported as signal strengths)
    WW_xs_qqqq_OPAL = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_multiply_float(
        r_WW_qqqq_OPAL_ALEPH, sigma_WW_qqqq_OPAL_SM),      
      'WW_xs_qqqq_OPAL'
    )

    # ALEPH measurements in lvlv, lvqq, qqqq channels    
    # SM predictions
    sigma_WW_qqqq_ALEPH_SM = SM.predict('WW_xs_qqqq_ALEPH')
    sigma_WW_lvqq_ALEPH_SM = SM.predict('WW_xs_lvqq_ALEPH')
    sigma_WW_lvlv_ALEPH_SM = SM.predict('WW_xs_lvlv_ALEPH')

    # Scale by SM prediction (data not reported as signal strengths)
    WW_xs_lvlv_ALEPH= EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_multiply_float(
        r_WW_lvlv_OPAL_ALEPH, sigma_WW_lvlv_ALEPH_SM),      
      'WW_xs_lvlv_ALEPH'
    )

    # Scale by SM prediction (data not reported as signal strengths)
    WW_xs_lvqq_ALEPH= EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_multiply_float(
        r_WW_lvqq_OPAL_ALEPH, sigma_WW_lvqq_ALEPH_SM),      
      'WW_xs_lvqq_ALEPH'
    )
    
    # Scale by SM prediction (data not reported as signal strengths)
    WW_xs_qqqq_ALEPH= EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_multiply_float(
        r_WW_qqqq_OPAL_ALEPH, sigma_WW_qqqq_ALEPH_SM),      
      'WW_xs_qqqq_ALEPH'
    )

    # L3 measurements in lvlv, lvqq, qqqq channels        
    # SM predictions
    sigma_WW_qqqq_L3_SM = SM.predict('WW_xs_qqqq_L3')
    sigma_WW_lvqq_L3_SM = SM.predict('WW_xs_lvqq_L3')
    sigma_WW_lvlv_L3_SM = SM.predict('WW_xs_lvlv_L3')

    # Include fully leptonic BR
    r_WW_lvlv_L3 =  EFTInternalParameter.as_product(
      r_WW_L3 , BR_WW_lvlv
    )
    
    # Scale by SM prediction (data not reported as signal strengths)
    WW_xs_lvlv_L3= EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_multiply_float(
        r_WW_lvlv_L3, sigma_WW_lvlv_L3_SM),      
      'WW_xs_lvlv_L3'
    )
    
    # Include semi-leptonic BR
    r_WW_lvqq_L3 =  EFTInternalParameter.as_product(
      r_WW_L3 , BR_WW_lvqq
    )
    
    # Scale by SM prediction (data not reported as signal strengths)
    WW_xs_lvqq_L3= EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_multiply_float(
        r_WW_lvqq_L3, sigma_WW_lvqq_L3_SM),      
      'WW_xs_lvqq_L3'
    )
    
    # Include hadronic BR
    r_WW_qqqq_L3 =  EFTInternalParameter.as_product(
      r_WW_L3 , BR_WW_qqqq
    )
    
    # Scale by SM prediction (data not reported as signal strengths)
    WW_xs_qqqq_L3= EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_multiply_float(
        r_WW_qqqq_L3, sigma_WW_qqqq_L3_SM),      
      'WW_xs_qqqq_L3'
    )
    
    # combined angular distributions in semileptonic channel as reported in 
    # 1302.3415. 10 Bins in cos(theta_W+) at 4 different energies
    sigma_WW_ang_182GeV = EFTInternalParameter.from_json(
      json_path+'dsig_dcostheta_WW_LEP_182GeV.json'
    )
    
    sigma_WW_ang_189GeV = EFTInternalParameter.from_json(
      json_path+'dsig_dcostheta_WW_LEP_189GeV.json'
    )
    
    sigma_WW_ang_198GeV = EFTInternalParameter.from_json(
      json_path+'dsig_dcostheta_WW_LEP_198GeV.json'
    )
    
    sigma_WW_ang_206GeV = EFTInternalParameter.from_json(
      json_path+'dsig_dcostheta_WW_LEP_206GeV.json'
    )

    # reduced set of bins used in fit method to mitigate lack of correlation
    # info: bins 1,4,7 & 10 at 182.66 & 205.92 GeV, see 1701.06424.
    sigma_WW_ang = EFTInternalParameter.as_concatenation( 
      sigma_WW_ang_182GeV.subset(indices=(0,3,6,9)),
      sigma_WW_ang_206GeV.subset(indices=(0,3,6,9))
    )
    # normalised to signal strength
    r_WW_ang = sigma_WW_ang.normalised_by_constant()
    # SM prediction
    sigma_WW_ang_SM = SM.predict('WW_costheta_LEPII')
    # Scaled by published SM prediction (data not reported as signal strengths)
    WW_costheta_LEPII = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_multiply_float(
        r_WW_ang,  sigma_WW_ang_SM),      
      'WW_costheta_LEPII'
    )
    
    # Full set of angular distribution bins
    # 182.66 GeV
    r_WW_ang_182GeV = sigma_WW_ang_182GeV.normalised_by_constant()
    sigma_WW_ang_182GeV_SM = SM.predict('WW_costheta_182GeV_LEPII')
    # # Scaled by published SM prediction (data not reported as signal strengths)
    # WW_costheta_182GeV_LEPII = EFTPrediction.from_EFTMethod(
    #   EFTInternalParameter.as_multiply_float(
    #     r_WW_ang_182GeV, sigma_WW_ang_182GeV_SM),
    #   'WW_costheta_182GeV_LEPII'
    # )
    # replace constant by best SM, keeping shifts unscaled
    WW_costheta_182GeV_LEPII = EFTPrediction.from_EFTMethod(
      sigma_WW_ang_182GeV,      
      'WW_costheta_182GeV_LEPII'
    )
    WW_costheta_182GeV_LEPII.set_field('constant', sigma_WW_ang_182GeV_SM)
    
    # 189.0 GeV    
    r_WW_ang_189GeV = sigma_WW_ang_189GeV.normalised_by_constant()
    sigma_WW_ang_189GeV_SM = SM.predict('WW_costheta_189GeV_LEPII')
    # # Scaled by published SM prediction (data not reported as signal strengths)
    # WW_costheta_189GeV_LEPII = EFTPrediction.from_EFTMethod(
    #   EFTInternalParameter.as_multiply_float(
    #     r_WW_ang_189GeV,  sigma_WW_ang_189GeV_SM),
    #   'WW_costheta_189GeV_LEPII'
    # )
    # replace constant by best SM, keeping shifts unscaled
    WW_costheta_189GeV_LEPII = EFTPrediction.from_EFTMethod(
      sigma_WW_ang_189GeV,      
      'WW_costheta_189GeV_LEPII'
    )
    WW_costheta_189GeV_LEPII.set_field('constant', sigma_WW_ang_189GeV_SM)
    
    # 198.3 GeV    
    r_WW_ang_198GeV = sigma_WW_ang_198GeV.normalised_by_constant()
    sigma_WW_ang_198GeV_SM = SM.predict('WW_costheta_198GeV_LEPII')
    # # Scaled by published SM prediction (data not reported as signal strengths)
    # WW_costheta_198GeV_LEPII = EFTPrediction.from_EFTMethod(
    #   EFTInternalParameter.as_multiply_float(
    #     r_WW_ang_198GeV,  sigma_WW_ang_198GeV_SM),
    #   'WW_costheta_198GeV_LEPII'
    # )
    # replace constant by best SM, keeping shifts unscaled
    WW_costheta_198GeV_LEPII = EFTPrediction.from_EFTMethod(
      sigma_WW_ang_198GeV,      
      'WW_costheta_198GeV_LEPII'
    )
    WW_costheta_198GeV_LEPII.set_field('constant', sigma_WW_ang_198GeV_SM)
    
    # 205.9 GeV    
    r_WW_ang_206GeV = sigma_WW_ang_206GeV.normalised_by_constant()
    sigma_WW_ang_206GeV_SM = SM.predict('WW_costheta_206GeV_LEPII')
    # # Scaled by published SM prediction (data not reported as signal strengths)
    # WW_costheta_206GeV_LEPII = EFTPrediction.from_EFTMethod(
    #   EFTInternalParameter.as_multiply_float(
    #     r_WW_ang_206GeV,  sigma_WW_ang_206GeV_SM),
    #   'WW_costheta_206GeV_LEPII'
    # )
    # replace constant by best SM, keeping shifts unscaled
    WW_costheta_206GeV_LEPII = EFTPrediction.from_EFTMethod(
      sigma_WW_ang_206GeV,      
      'WW_costheta_206GeV_LEPII'
    )
    WW_costheta_206GeV_LEPII.set_field('constant', sigma_WW_ang_206GeV_SM)
    
class LEP_lin(V_Decays_lin, SM_MW_MZ_GF_5F_Diboson_LEP):
    '''
    SMEFT Warsaw basis for LEP Diboson, based on explicit computations with 
    SMEFT_MW_MZ_GF.
    '''
    # # instance of SM class for base predictions
    SM = SM_MW_MZ_GF_5F_Diboson_LEP()
    
    # relative shift in W branching fractions
    mu_BR_W_lv = V_Decays_lin.mu_BR_W_lv
    mu_BR_W_qq = V_Decays_lin.mu_BR_W_qq 
    # leptonic WW BR
    BR_WW_lvlv = mu_BR_W_lv.power(2., linear_only=True)
    # semileptonic WW BR
    BR_WW_lvqq = EFTInternalParameter.as_product( mu_BR_W_lv, mu_BR_W_qq, linear_only=True)
    # hadronic WW BR
    BR_WW_qqqq = mu_BR_W_qq.power(2., linear_only=True)
    
    # total e+ e- > W+ W- cross sections at 8 CoM energies, as measured by LEP
    sigma_WW_1= EFTInternalParameter.from_json(
      json_path+'sigma_WW_LEP_188.6GeV.json',
      linear_only=True
    )
    sigma_WW_2= EFTInternalParameter.from_json(
      json_path+'sigma_WW_LEP_191.6GeV.json',
      linear_only=True
    )
    sigma_WW_3 = EFTInternalParameter.from_json(
      json_path+'sigma_WW_LEP_195.5GeV.json',
      linear_only=True
    )
    sigma_WW_4 = EFTInternalParameter.from_json(
      json_path+'sigma_WW_LEP_199.6GeV.json',
      linear_only=True
    )
    sigma_WW_5 = EFTInternalParameter.from_json(
      json_path+'sigma_WW_LEP_201.8GeV.json',
      linear_only=True
    )
    sigma_WW_6 = EFTInternalParameter.from_json(
      json_path+'sigma_WW_LEP_204.8GeV.json',
      linear_only=True
    )
    sigma_WW_7 = EFTInternalParameter.from_json(
      json_path+'sigma_WW_LEP_206.5GeV.json',
      linear_only=True
    )
    sigma_WW_8 = EFTInternalParameter.from_json(
      json_path+'sigma_WW_LEP_208.0GeV.json',
      linear_only=True
    )
    
    # 7 measurements by OPAL/ALEPH
    sigma_WW_OPAL_ALEPH = EFTInternalParameter.as_concatenation( 
      sigma_WW_1, sigma_WW_2, sigma_WW_3, sigma_WW_4, 
      sigma_WW_5, sigma_WW_6, sigma_WW_7
    )
    # Relative impact
    r_WW_OPAL_ALEPH = sigma_WW_OPAL_ALEPH.normalised_by_constant()

    # 8 measurements by L3      
    sigma_WW_L3 = EFTInternalParameter.as_concatenation( 
      sigma_WW_1, sigma_WW_2, sigma_WW_3, sigma_WW_4, 
      sigma_WW_5, sigma_WW_6, sigma_WW_7, sigma_WW_8
    )
    # Relative impact
    r_WW_L3 = sigma_WW_L3.normalised_by_constant()
    
    # OPAL measurements in lvlv, lvqq, qqqq channels
    # SM predictions
    sigma_WW_qqqq_OPAL_SM = SM.predict('WW_xs_qqqq_OPAL')
    sigma_WW_lvqq_OPAL_SM = SM.predict('WW_xs_lvqq_OPAL')
    sigma_WW_lvlv_OPAL_SM = SM.predict('WW_xs_lvlv_OPAL')
    
    # Include fully leptonic BR
    r_WW_lvlv_OPAL_ALEPH =  EFTInternalParameter.as_product(
      r_WW_OPAL_ALEPH , BR_WW_lvlv, linear_only=True
    )
    # Scale by SM prediction (data not reported as signal strengths)
    WW_xs_lvlv_OPAL = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_multiply_float(
        r_WW_lvlv_OPAL_ALEPH, sigma_WW_lvlv_OPAL_SM, linear_only=True),      
      'WW_xs_lvlv_OPAL'
    )

    # Include semi-leptonic BR
    r_WW_lvqq_OPAL_ALEPH =  EFTInternalParameter.as_product(
      r_WW_OPAL_ALEPH , BR_WW_lvqq, linear_only=True
    )
    # Scale by SM prediction (data not reported as signal strengths)
    WW_xs_lvqq_OPAL = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_multiply_float(
        r_WW_lvqq_OPAL_ALEPH, sigma_WW_lvqq_OPAL_SM, linear_only=True),      
      'WW_xs_lvqq_OPAL'
    )
    
    # Include hadronic BR
    r_WW_qqqq_OPAL_ALEPH =  EFTInternalParameter.as_product(
      r_WW_OPAL_ALEPH , BR_WW_qqqq, linear_only=True
    )
    # Scale by SM prediction (data not reported as signal strengths)
    WW_xs_qqqq_OPAL = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_multiply_float(
        r_WW_qqqq_OPAL_ALEPH, sigma_WW_qqqq_OPAL_SM, linear_only=True),      
      'WW_xs_qqqq_OPAL'
    )

    # ALEPH measurements in lvlv, lvqq, qqqq channels    
    # SM predictions
    sigma_WW_qqqq_ALEPH_SM = SM.predict('WW_xs_qqqq_ALEPH')
    sigma_WW_lvqq_ALEPH_SM = SM.predict('WW_xs_lvqq_ALEPH')
    sigma_WW_lvlv_ALEPH_SM = SM.predict('WW_xs_lvlv_ALEPH')

    # Scale by SM prediction (data not reported as signal strengths)
    WW_xs_lvlv_ALEPH= EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_multiply_float(
        r_WW_lvlv_OPAL_ALEPH, sigma_WW_lvlv_ALEPH_SM, linear_only=True),      
      'WW_xs_lvlv_ALEPH'
    )

    # Scale by SM prediction (data not reported as signal strengths)
    WW_xs_lvqq_ALEPH= EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_multiply_float(
        r_WW_lvqq_OPAL_ALEPH, sigma_WW_lvqq_ALEPH_SM, linear_only=True),      
      'WW_xs_lvqq_ALEPH'
    )
    
    # Scale by SM prediction (data not reported as signal strengths)
    WW_xs_qqqq_ALEPH= EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_multiply_float(
        r_WW_qqqq_OPAL_ALEPH, sigma_WW_qqqq_ALEPH_SM, linear_only=True),      
      'WW_xs_qqqq_ALEPH'
    )

    # L3 measurements in lvlv, lvqq, qqqq channels        
    # SM predictions
    sigma_WW_qqqq_L3_SM = SM.predict('WW_xs_qqqq_L3')
    sigma_WW_lvqq_L3_SM = SM.predict('WW_xs_lvqq_L3')
    sigma_WW_lvlv_L3_SM = SM.predict('WW_xs_lvlv_L3')

    # Include fully leptonic BR
    r_WW_lvlv_L3 =  EFTInternalParameter.as_product(
      r_WW_L3 , BR_WW_lvlv, linear_only=True
    )
    
    # Scale by SM prediction (data not reported as signal strengths)
    WW_xs_lvlv_L3= EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_multiply_float(
        r_WW_lvlv_L3, sigma_WW_lvlv_L3_SM, linear_only=True),      
      'WW_xs_lvlv_L3'
    )
    
    # Include semi-leptonic BR
    r_WW_lvqq_L3 =  EFTInternalParameter.as_product(
      r_WW_L3 , BR_WW_lvqq, linear_only=True
    )
    
    # Scale by SM prediction (data not reported as signal strengths)
    WW_xs_lvqq_L3= EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_multiply_float(
        r_WW_lvqq_L3, sigma_WW_lvqq_L3_SM, linear_only=True),      
      'WW_xs_lvqq_L3'
    )
    
    # Include hadronic BR
    r_WW_qqqq_L3 =  EFTInternalParameter.as_product(
      r_WW_L3 , BR_WW_qqqq, linear_only=True
    )
    
    # Scale by SM prediction (data not reported as signal strengths)
    WW_xs_qqqq_L3= EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_multiply_float(
        r_WW_qqqq_L3, sigma_WW_qqqq_L3_SM, linear_only=True),      
      'WW_xs_qqqq_L3'
    )
    
    # combined angular distributions in semileptonic channel as reported in 
    # 1302.3415. 10 Bins in cos(theta_W+) at 4 different energies
    sigma_WW_ang_182GeV = EFTInternalParameter.from_json(
      json_path+'dsig_dcostheta_WW_LEP_182GeV.json',
      linear_only=True
    )
    
    sigma_WW_ang_189GeV = EFTInternalParameter.from_json(
      json_path+'dsig_dcostheta_WW_LEP_189GeV.json',
      linear_only=True
    )
    
    sigma_WW_ang_198GeV = EFTInternalParameter.from_json(
      json_path+'dsig_dcostheta_WW_LEP_198GeV.json',
      linear_only=True
    )
    
    sigma_WW_ang_206GeV = EFTInternalParameter.from_json(
      json_path+'dsig_dcostheta_WW_LEP_206GeV.json',
      linear_only=True
    )
    # reduced set of bins used in fit method to mitigate lack of correlation
    # info: bins 1,4,7 & 10 at 182.66 & 205.92 GeV, see 1701.06424.
    sigma_WW_ang = EFTInternalParameter.as_concatenation( 
      sigma_WW_ang_182GeV.subset(indices=(0,3,6,9)),
      sigma_WW_ang_206GeV.subset(indices=(0,3,6,9))
    )
    # normalised to signal strength
    r_WW_ang = sigma_WW_ang.normalised_by_constant()
    # SM prediction
    sigma_WW_ang_SM = SM.predict('WW_costheta_LEPII')
    # Scaled by published SM prediction (data not reported as signal strengths)
    WW_costheta_LEPII = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_multiply_float(
        r_WW_ang,  sigma_WW_ang_SM, linear_only=True),      
      'WW_costheta_LEPII'
    )
    
    # Full set of angular distribution bins
    # 182.66 GeV
    r_WW_ang_182GeV = sigma_WW_ang_182GeV.normalised_by_constant()
    sigma_WW_ang_182GeV_SM = SM.predict('WW_costheta_182GeV_LEPII')
    # # Scaled by published SM prediction (data not reported as signal strengths)
    # WW_costheta_182GeV_LEPII = EFTPrediction.from_EFTMethod(
    #   EFTInternalParameter.as_multiply_float(
    #     r_WW_ang_182GeV,  sigma_WW_ang_182GeV_SM, linear_only=True),
    #   'WW_costheta_182GeV_LEPII'
    # )
    # replace constant by best SM, keeping shifts unscaled
    WW_costheta_182GeV_LEPII = EFTPrediction.from_EFTMethod(
      sigma_WW_ang_182GeV,      
      'WW_costheta_182GeV_LEPII'
    )
    WW_costheta_182GeV_LEPII.set_field('constant', sigma_WW_ang_182GeV_SM)
    
    # 189.0 GeV    
    r_WW_ang_189GeV = sigma_WW_ang_189GeV.normalised_by_constant()
    sigma_WW_ang_189GeV_SM = SM.predict('WW_costheta_189GeV_LEPII')
    # # Scaled by published SM prediction (data not reported as signal strengths)
    # WW_costheta_189GeV_LEPII = EFTPrediction.from_EFTMethod(
    #   EFTInternalParameter.as_multiply_float(
    #     r_WW_ang_189GeV,  sigma_WW_ang_189GeV_SM, linear_only=True),
    #   'WW_costheta_189GeV_LEPII'
    # )
    # replace constant by best SM, keeping shifts unscaled
    WW_costheta_189GeV_LEPII = EFTPrediction.from_EFTMethod(
      sigma_WW_ang_189GeV,      
      'WW_costheta_189GeV_LEPII'
    )
    WW_costheta_189GeV_LEPII.set_field('constant', sigma_WW_ang_189GeV_SM)
    
    # 198.3 GeV    
    r_WW_ang_198GeV = sigma_WW_ang_198GeV.normalised_by_constant()
    sigma_WW_ang_198GeV_SM = SM.predict('WW_costheta_198GeV_LEPII')
    # # Scaled by published SM prediction (data not reported as signal strengths)
    # WW_costheta_198GeV_LEPII = EFTPrediction.from_EFTMethod(
    #   EFTInternalParameter.as_multiply_float(
    #     r_WW_ang_198GeV,  sigma_WW_ang_198GeV_SM, linear_only=True),
    #   'WW_costheta_198GeV_LEPII'
    # )
    # replace constant by best SM, keeping shifts unscaled
    WW_costheta_198GeV_LEPII = EFTPrediction.from_EFTMethod(
      sigma_WW_ang_198GeV,      
      'WW_costheta_198GeV_LEPII'
    )
    WW_costheta_198GeV_LEPII.set_field('constant', sigma_WW_ang_198GeV_SM)
    
    # 205.9 GeV    
    r_WW_ang_206GeV = sigma_WW_ang_206GeV.normalised_by_constant()
    sigma_WW_ang_206GeV_SM = SM.predict('WW_costheta_206GeV_LEPII')
    # # Scaled by published SM prediction (data not reported as signal strengths)
    # WW_costheta_206GeV_LEPII = EFTPrediction.from_EFTMethod(
    #   EFTInternalParameter.as_multiply_float(
    #     r_WW_ang_206GeV,  sigma_WW_ang_206GeV_SM, linear_only=True),
    #   'WW_costheta_206GeV_LEPII'
    # )
    # replace constant by best SM, keeping shifts unscaled
    WW_costheta_206GeV_LEPII = EFTPrediction.from_EFTMethod(
      sigma_WW_ang_206GeV,      
      'WW_costheta_206GeV_LEPII'
    )
    WW_costheta_206GeV_LEPII.set_field('constant', sigma_WW_ang_206GeV_SM)

from ..SMEFiT import data_path as data_path_SMEFiT
json_path_SMEFiT = data_path_SMEFiT+'diboson/'

# LHC Diboson in terms of Warsaw basis
class LHC_LO(V_Decays):
    '''
    SMEFT Warsaw basis for LHC Diboson, computed with SMEFT_MW_MZ_GF at LO.
    '''

    # leptonic BRs
    mu_BR_W_lv = V_Decays.mu_BR_W_lv
    mu_BR_W_emuv = V_Decays.mu_BR_W_emuv
    mu_BR_W_ev = V_Decays.mu_BR_W_ev
    mu_BR_W_mv = V_Decays.mu_BR_W_mv
    mu_BR_Z_ll = V_Decays.mu_BR_Z_ll
    mu_BR_Z_eemumu = V_Decays.mu_BR_Z_eemumu
    
    # LHC 13 TeV VV differential signal strengths
    
    ## WW CERN-EP-2019-055
    
    ### WW leptonic branching ratios
    # BR_WW_lep = mu_BR_W_lv.power(2.)
    mu_BR_WW_emu = EFTInternalParameter.as_product(
     mu_BR_W_ev, mu_BR_W_mv
    )
    
    ## Memu distribution
    ### production
    r_dsig_dMemu_WW_ATLAS_13TeV_nodecay = EFTInternalParameter.from_json(
      json_path+'r_dsig_dMemu_WW_ATLAS_13TeV_LO.json'
    )
    ### production * leptonic branching ratio
    mu_dsig_dMemu_WW_ATLAS_13TeV = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_product( 
        r_dsig_dMemu_WW_ATLAS_13TeV_nodecay, 
        mu_BR_WW_emu
      ),
      'mu_dsig_dMemu_WW_ATLAS_13TeV'
    )
    
    ## PTemu distribution
    ### production
    r_dsig_dPTemu_WW_ATLAS_13TeV_nodecay = EFTInternalParameter.from_json(
      json_path+'r_dsig_dPTemu_WW_ATLAS_13TeV_LO.json'
    )
    ### production * leptonic branching ratio
    mu_dsig_dPTemu_WW_ATLAS_13TeV = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_product( 
        r_dsig_dPTemu_WW_ATLAS_13TeV_nodecay, 
        mu_BR_WW_emu
      ),
      'mu_dsig_dPTemu_WW_ATLAS_13TeV'
    )
    
    ## PTl1 distribution
    ### production
    r_dsig_dPTl1_WW_ATLAS_13TeV_nodecay = EFTInternalParameter.from_json(
      json_path+'r_dsig_dPTl1_WW_ATLAS_13TeV_LO.json'
    )
    ### production * leptonic branching ratio
    mu_dsig_dPTl1_WW_ATLAS_13TeV = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_product( 
        r_dsig_dPTl1_WW_ATLAS_13TeV_nodecay, 
        mu_BR_WW_emu
      ),
      # 'mu_dsig_dPTl1_WW_ATLAS_13TeV'
      'fidmu_WW_enumunu_ptl_ATLAS13'
    )
    
    ## Yemu distribution
    ### production
    r_dsig_dYemu_WW_ATLAS_13TeV_nodecay = EFTInternalParameter.from_json(
      json_path+'r_dsig_dYemu_WW_ATLAS_13TeV_LO.json'
    )
    ### production * leptonic branching ratio
    mu_dsig_dYemu_WW_ATLAS_13TeV = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_product( 
        r_dsig_dYemu_WW_ATLAS_13TeV_nodecay, 
        mu_BR_WW_emu
      ),
      'mu_dsig_dYemu_WW_ATLAS_13TeV'
    )
    
    ## WZ CERN-EP-2018-327
    
    ### WZ leptonic branching ratio
    BR_WZ_lep = mu_BR_W_emuv.product( mu_BR_Z_eemumu )
    
    ## Memu distribution
    ### production
    r_dsig_dMTWZ_WZ_ATLAS_13TeV_nodecay = EFTInternalParameter.from_json(
      json_path+'r_dsig_dMTWZ_WZ_ATLAS_13TeV_LO.json'
    )
    ### production * leptonic branching ratio
    mu_dsig_dMTWZ_WZ_ATLAS_13TeV = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_product( r_dsig_dMTWZ_WZ_ATLAS_13TeV_nodecay, BR_WZ_lep ),
      'mu_dsig_dMTWZ_WZ_ATLAS_13TeV'
    )
    
    ## pTZ distribution
    ### production
    r_dsig_dpTZ_WZ_ATLAS_13TeV_nodecay = EFTInternalParameter.from_json(
      json_path+'r_dsig_dpTZ_WZ_ATLAS_13TeV_LO.json'
    )
    ### production * leptonic branching ratio
    mu_dsig_dpTZ_WZ_ATLAS_13TeV = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_product( r_dsig_dpTZ_WZ_ATLAS_13TeV_nodecay, BR_WZ_lep ),
      'mu_dsig_dpTZ_WZ_ATLAS_13TeV'
    )
    
    ## pTW distribution
    ### production
    r_dsig_dpTW_WZ_ATLAS_13TeV_nodecay = EFTInternalParameter.from_json(
      json_path+'r_dsig_dpTW_WZ_ATLAS_13TeV_LO.json'
    )
    ### production * leptonic branching ratio
    mu_dsig_dpTW_WZ_ATLAS_13TeV = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_product( r_dsig_dpTW_WZ_ATLAS_13TeV_nodecay, BR_WZ_lep ),
      'mu_dsig_dpTW_WZ_ATLAS_13TeV'
    )
    
    ## Zjj CERN-EP-2020-045
    ### production
    r_dsig_dphijj_Zjj_ATLAS_13TeV_nodecay = EFTInternalParameter.from_json(
      json_path+'r_dsig_dphijj_Zjj_ATLAS_13TeV_LO.json'
    )
    ### production * leptonic branching ratio
    mu_dsig_dphijj_Zjj_ATLAS_13TeV = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_product( r_dsig_dphijj_Zjj_ATLAS_13TeV_nodecay, mu_BR_Z_eemumu ),
      'mu_dsig_dphijj_Zjj_ATLAS_13TeV'
    )
    # Some SMEFiT predictions
    dsig_dMTWZ_WZ_ATLAS_LO_13TeV = EFTPrediction.from_json(
      json_path_SMEFiT+'dsig_dMTWZ_WZ_ATLAS_LO_13TeV.json'
    )
    dsig_dpTZ_WZ_CMS_LO_13TeV = EFTPrediction.from_json(
      json_path_SMEFiT+'dsig_dpTZ_WZ_CMS_LO_13TeV.json'
    )
    dsig_dmemu_WW_ATLAS_LO_13TeV = EFTPrediction.from_json(
      json_path_SMEFiT+'dsig_dmemu_WW_ATLAS_LO_13TeV.json'
    )
     
# LHC Diboson in terms of Warsaw basis
class LHC_LO_lin(V_Decays_lin):
    '''
    SMEFT Warsaw basis for LHC Diboson, computed with SMEFT_MW_MZ_GF at LO.
    '''

    # leptonic BRs
    mu_BR_W_lv = V_Decays_lin.mu_BR_W_lv
    mu_BR_W_emuv = V_Decays_lin.mu_BR_W_emuv
    mu_BR_W_ev = V_Decays_lin.mu_BR_W_ev
    mu_BR_W_mv = V_Decays_lin.mu_BR_W_mv
    mu_BR_Z_ll = V_Decays_lin.mu_BR_Z_ll
    mu_BR_Z_eemumu = V_Decays_lin.mu_BR_Z_eemumu
        
    # LHC 13 TeV VV differential signal strengths
    
    ## WW CERN-EP-2019-055
    
    ### WW leptonic branching ratio
    # BR_WW_lep = mu_BR_W_lv.power(2., linear_only=True)
    mu_BR_WW_emu = EFTInternalParameter.as_product(
     mu_BR_W_ev, mu_BR_W_mv, linear_only=True
    )
        
    ## Memu distribution
    ### production
    r_dsig_dMemu_WW_ATLAS_13TeV_nodecay = EFTInternalParameter.from_json(
      json_path+'r_dsig_dMemu_WW_ATLAS_13TeV_LO.json',
      linear_only=True
    )
    ### production * leptonic branching ratio
    mu_dsig_dMemu_WW_ATLAS_13TeV = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_product( 
        r_dsig_dMemu_WW_ATLAS_13TeV_nodecay, 
        mu_BR_WW_emu,
        linear_only=True
      ),
      'mu_dsig_dMemu_WW_ATLAS_13TeV'
    )
    
    ## PTemu distribution
    ### production
    r_dsig_dPTemu_WW_ATLAS_13TeV_nodecay = EFTInternalParameter.from_json(
      json_path+'r_dsig_dPTemu_WW_ATLAS_13TeV_LO.json',
      linear_only=True
    )
    ### production * leptonic branching ratio
    mu_dsig_dPTemu_WW_ATLAS_13TeV = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_product( 
        r_dsig_dPTemu_WW_ATLAS_13TeV_nodecay, 
        mu_BR_WW_emu,
        linear_only=True
      ),
      'mu_dsig_dPTemu_WW_ATLAS_13TeV'
    )
    
    ## PTl1 distribution
    ### production
    r_dsig_dPTl1_WW_ATLAS_13TeV_nodecay = EFTInternalParameter.from_json(
      json_path+'r_dsig_dPTl1_WW_ATLAS_13TeV_LO.json',
      linear_only=True
    )
    ### production * leptonic branching ratio
    mu_dsig_dPTl1_WW_ATLAS_13TeV = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_product( 
        r_dsig_dPTl1_WW_ATLAS_13TeV_nodecay, 
        mu_BR_WW_emu,
        linear_only=True
      ),
      # 'mu_dsig_dPTl1_WW_ATLAS_13TeV'
      'fidmu_WW_enumunu_ptl_ATLAS13'
    )
    
    ## Yemu distribution
    ### production
    r_dsig_dYemu_WW_ATLAS_13TeV_nodecay = EFTInternalParameter.from_json(
      json_path+'r_dsig_dYemu_WW_ATLAS_13TeV_LO.json',
      linear_only=True
    )
    ### production * leptonic branching ratio
    mu_dsig_dYemu_WW_ATLAS_13TeV = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_product( 
        r_dsig_dYemu_WW_ATLAS_13TeV_nodecay, 
        mu_BR_WW_emu,
        linear_only=True
      ),
      'mu_dsig_dYemu_WW_ATLAS_13TeV'
    )
    
    ## WZ CERN-EP-2018-327
    
    ### WZ leptonic branching ratio
    mu_BR_WZ_lep = mu_BR_W_emuv.product( mu_BR_Z_eemumu, linear_only=True)
    
    ## Memu distribution
    ### production
    r_dsig_dMTWZ_WZ_ATLAS_13TeV_nodecay = EFTInternalParameter.from_json(
      json_path+'r_dsig_dMTWZ_WZ_ATLAS_13TeV_LO.json',
      linear_only=True
    )
    ### production * leptonic branching ratio
    mu_dsig_dMTWZ_WZ_ATLAS_13TeV = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_product(
        r_dsig_dMTWZ_WZ_ATLAS_13TeV_nodecay, 
        mu_BR_WZ_lep,
        linear_only=True
      ),
      'mu_dsig_dMTWZ_WZ_ATLAS_13TeV'
    )
    
    ## pTZ distribution
    ### production
    r_dsig_dpTZ_WZ_ATLAS_13TeV_nodecay = EFTInternalParameter.from_json(
      json_path+'r_dsig_dpTZ_WZ_ATLAS_13TeV_LO.json',
      linear_only=True
    )
    ### production * leptonic branching ratio
    mu_dsig_dpTZ_WZ_ATLAS_13TeV = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_product( 
        r_dsig_dpTZ_WZ_ATLAS_13TeV_nodecay, 
        mu_BR_WZ_lep,
        linear_only=True
      ),
      'mu_dsig_dpTZ_WZ_ATLAS_13TeV'
    )
    
    ## pTW distribution
    ### production
    r_dsig_dpTW_WZ_ATLAS_13TeV_nodecay = EFTInternalParameter.from_json(
      json_path+'r_dsig_dpTW_WZ_ATLAS_13TeV_LO.json',
      linear_only=True
      )
    ### production * leptonic branching ratio
    mu_dsig_dpTW_WZ_ATLAS_13TeV = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_product(
        r_dsig_dpTW_WZ_ATLAS_13TeV_nodecay, 
        mu_BR_WZ_lep,
        linear_only=True
      ),
      'mu_dsig_dpTW_WZ_ATLAS_13TeV'
    )
    
    ## Zjj CERN-EP-2020-045
    ### production
    r_dsig_dphijj_Zjj_ATLAS_13TeV_nodecay = EFTInternalParameter.from_json(
      json_path+'r_dsig_dphijj_Zjj_ATLAS_13TeV_LO.json',
      linear_only=True
    )
    ### production * leptonic branching ratio
    mu_dsig_dphijj_Zjj_ATLAS_13TeV = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_product(
        r_dsig_dphijj_Zjj_ATLAS_13TeV_nodecay,
        mu_BR_Z_eemumu,
        linear_only=True
      ),
      'mu_dsig_dphijj_Zjj_ATLAS_13TeV'
    )
    # Some SMEFiT predictions
    dsig_dMTWZ_WZ_ATLAS_LO_13TeV = EFTPrediction.from_json(
      json_path_SMEFiT+'dsig_dMTWZ_WZ_ATLAS_LO_13TeV.json',
      linear_only=True
    )
    dsig_dpTZ_WZ_CMS_LO_13TeV = EFTPrediction.from_json(
      json_path_SMEFiT+'dsig_dpTZ_WZ_CMS_LO_13TeV.json',
      linear_only=True
    )
    dsig_dmemu_WW_ATLAS_LO_13TeV = EFTPrediction.from_json(
      json_path_SMEFiT+'dsig_dmemu_WW_ATLAS_LO_13TeV.json',
      linear_only=True
    )

# LHC Diboson in terms of Warsaw basis
class LHC_NLO(V_Decays):
    '''
    SMEFT Warsaw basis for LHC Diboson, computed with SMEFT_MW_MZ_GF at NLO. 
    Branching fractions computed at LO only.
    '''

    # leptonic BRs
    mu_BR_W_lv = V_Decays.mu_BR_W_lv
    mu_BR_W_emuv = V_Decays.mu_BR_W_emuv
    mu_BR_W_ev = V_Decays.mu_BR_W_ev
    mu_BR_W_mv = V_Decays.mu_BR_W_mv
    mu_BR_Z_ll = V_Decays.mu_BR_Z_ll
    mu_BR_Z_eemumu = V_Decays.mu_BR_Z_eemumu
    
    # LHC 13 TeV VV differential signal strengths
    
    ## WW CERN-EP-2019-055
    
    ### WW leptonic branching ratio
    # BR_WW_lep = mu_BR_W_lv.power(2.)
    mu_BR_WW_emu = EFTInternalParameter.as_product(
     mu_BR_W_ev, mu_BR_W_mv
    )
    
    ## Memu distribution
    ### production
    r_dsig_dMemu_WW_ATLAS_13TeV_nodecay = EFTInternalParameter.from_json(
      json_path+'r_dsig_dMemu_WW_ATLAS_13TeV_NLO.json'
    )
    ### production * leptonic branching ratio
    mu_dsig_dMemu_WW_ATLAS_13TeV = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_product( 
        r_dsig_dMemu_WW_ATLAS_13TeV_nodecay, 
        mu_BR_WW_emu
      ),
      'mu_dsig_dMemu_WW_ATLAS_13TeV'
    )
    
    ## PTemu distribution
    ### production
    r_dsig_dPTemu_WW_ATLAS_13TeV_nodecay = EFTInternalParameter.from_json(
      json_path+'r_dsig_dPTemu_WW_ATLAS_13TeV_NLO.json'
    )
    ### production * leptonic branching ratio
    mu_dsig_dPTemu_WW_ATLAS_13TeV = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_product( 
        r_dsig_dPTemu_WW_ATLAS_13TeV_nodecay, 
        mu_BR_WW_emu
      ),
      'mu_dsig_dPTemu_WW_ATLAS_13TeV'
    )
    
    ## PTl1 distribution
    ### production
    r_dsig_dPTl1_WW_ATLAS_13TeV_nodecay = EFTInternalParameter.from_json(
      json_path+'r_dsig_dPTl1_WW_ATLAS_13TeV_NLO.json'
    )
    ### production * leptonic branching ratio
    mu_dsig_dPTl1_WW_ATLAS_13TeV = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_product( 
        r_dsig_dPTl1_WW_ATLAS_13TeV_nodecay, 
        mu_BR_WW_emu
      ),
      # 'mu_dsig_dPTl1_WW_ATLAS_13TeV'
      'fidmu_WW_enumunu_ptl_ATLAS13'
    )
    
    ## Yemu distribution
    ### production
    r_dsig_dYemu_WW_ATLAS_13TeV_nodecay = EFTInternalParameter.from_json(
      json_path+'r_dsig_dYemu_WW_ATLAS_13TeV_NLO.json'
    )
    ### production * leptonic branching ratio
    mu_dsig_dYemu_WW_ATLAS_13TeV = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_product( 
        r_dsig_dYemu_WW_ATLAS_13TeV_nodecay, 
        mu_BR_WW_emu
      ),
      'mu_dsig_dYemu_WW_ATLAS_13TeV'
    )
    
    ## WZ CERN-EP-2018-327
    
    ### WZ leptonic branching ratio
    mu_BR_WZ_lep = mu_BR_W_emuv.product( mu_BR_Z_eemumu )
    
    ## Memu distribution
    ### production
    r_dsig_dMTWZ_WZ_ATLAS_13TeV_nodecay = EFTInternalParameter.from_json(
      json_path+'r_dsig_dMTWZ_WZ_ATLAS_13TeV_NLO.json'
    )
    ### production * leptonic branching ratio
    mu_dsig_dMTWZ_WZ_ATLAS_13TeV = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_product( r_dsig_dMTWZ_WZ_ATLAS_13TeV_nodecay, mu_BR_WZ_lep ),
      'mu_dsig_dMTWZ_WZ_ATLAS_13TeV'
    )
    
    ## pTZ distribution
    ### production
    r_dsig_dpTZ_WZ_ATLAS_13TeV_nodecay = EFTInternalParameter.from_json(
      json_path+'r_dsig_dpTZ_WZ_ATLAS_13TeV_NLO.json'
    )
    ### production * leptonic branching ratio
    mu_dsig_dpTZ_WZ_ATLAS_13TeV = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_product( r_dsig_dpTZ_WZ_ATLAS_13TeV_nodecay, mu_BR_WZ_lep ),
      'mu_dsig_dpTZ_WZ_ATLAS_13TeV'
    )
    
    ## pTW distribution
    ### production
    r_dsig_dpTW_WZ_ATLAS_13TeV_nodecay = EFTInternalParameter.from_json(
      json_path+'r_dsig_dpTW_WZ_ATLAS_13TeV_NLO.json'
    )
    ### production * leptonic branching ratio
    mu_dsig_dpTW_WZ_ATLAS_13TeV = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_product( r_dsig_dpTW_WZ_ATLAS_13TeV_nodecay, mu_BR_WZ_lep ),
      'mu_dsig_dpTW_WZ_ATLAS_13TeV'
    )
    
    ## Zjj CERN-EP-2020-045
    ### production
    r_dsig_dphijj_Zjj_ATLAS_13TeV_nodecay = EFTInternalParameter.from_json(
      json_path+'r_dsig_dphijj_Zjj_ATLAS_13TeV_LO.json'
    )
    ### production * leptonic branching ratio
    mu_dsig_dphijj_Zjj_ATLAS_13TeV = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_product( r_dsig_dphijj_Zjj_ATLAS_13TeV_nodecay, mu_BR_Z_eemumu ),
      'mu_dsig_dphijj_Zjj_ATLAS_13TeV'
    )
    
    # Some SMEFiT predictions
    dsig_dMTWZ_WZ_ATLAS_LO_13TeV = EFTPrediction.from_json(
      json_path_SMEFiT+'dsig_dMTWZ_WZ_ATLAS_NLO_13TeV.json'
    )
    dsig_dpTZ_WZ_CMS_LO_13TeV = EFTPrediction.from_json(
      json_path_SMEFiT+'dsig_dpTZ_WZ_CMS_NLO_13TeV.json'
    )
    dsig_dmemu_WW_ATLAS_LO_13TeV = EFTPrediction.from_json(
      json_path_SMEFiT+'dsig_dmemu_WW_ATLAS_NLO_13TeV.json'
    )
    
# LHC Diboson in terms of Warsaw basis
class LHC_NLO_lin(V_Decays_lin):
    '''
    SMEFT Warsaw basis for LHC Diboson, computed with SMEFT_MW_MZ_GF at LO.
    '''

    # leptonic BRs
    mu_BR_W_lv = V_Decays_lin.mu_BR_W_lv
    mu_BR_W_emuv = V_Decays_lin.mu_BR_W_emuv
    mu_BR_W_ev = V_Decays_lin.mu_BR_W_ev
    mu_BR_W_mv = V_Decays_lin.mu_BR_W_mv
    mu_BR_Z_ll = V_Decays_lin.mu_BR_Z_ll
    mu_BR_Z_eemumu = V_Decays_lin.mu_BR_Z_eemumu
    
    # LHC 13 TeV VV differential signal strengths
    
    ## WW CERN-EP-2019-055
    
    ### WW leptonic branching ratio
    # BR_WW_lep = mu_BR_W_lv.power(2., linear_only=True)
    mu_BR_WW_emu = EFTInternalParameter.as_product(
     mu_BR_W_ev, mu_BR_W_mv, linear_only=True
    )
    
    ## Memu distribution
    ### production
    r_dsig_dMemu_WW_ATLAS_13TeV_nodecay = EFTInternalParameter.from_json(
      json_path+'r_dsig_dMemu_WW_ATLAS_13TeV_NLO.json',
      linear_only=True
    )
    ### production * leptonic branching ratio
    mu_dsig_dMemu_WW_ATLAS_13TeV = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_product( 
        r_dsig_dMemu_WW_ATLAS_13TeV_nodecay, 
        mu_BR_WW_emu,
        linear_only=True
      ),
      'mu_dsig_dMemu_WW_ATLAS_13TeV'
    )
    
    ## PTemu distribution
    ### production
    r_dsig_dPTemu_WW_ATLAS_13TeV_nodecay = EFTInternalParameter.from_json(
      json_path+'r_dsig_dPTemu_WW_ATLAS_13TeV_NLO.json',
      linear_only=True
    )
    ### production * leptonic branching ratio
    mu_dsig_dPTemu_WW_ATLAS_13TeV = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_product( 
        r_dsig_dPTemu_WW_ATLAS_13TeV_nodecay, 
        mu_BR_WW_emu,
        linear_only=True
      ),
      'mu_dsig_dPTemu_WW_ATLAS_13TeV'
    )
    
    ## PTl1 distribution
    ### production
    r_dsig_dPTl1_WW_ATLAS_13TeV_nodecay = EFTInternalParameter.from_json(
      json_path+'r_dsig_dPTl1_WW_ATLAS_13TeV_NLO.json',
      linear_only=True
    )
    ### production * leptonic branching ratio
    mu_dsig_dPTl1_WW_ATLAS_13TeV = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_product( 
        r_dsig_dPTl1_WW_ATLAS_13TeV_nodecay, 
        mu_BR_WW_emu,
        linear_only=True
      ),
      # 'mu_dsig_dPTl1_WW_ATLAS_13TeV'
      'fidmu_WW_enumunu_ptl_ATLAS13'
    )
    
    ## Yemu distribution
    ### production
    r_dsig_dYemu_WW_ATLAS_13TeV_nodecay = EFTInternalParameter.from_json(
      json_path+'r_dsig_dYemu_WW_ATLAS_13TeV_NLO.json',
      linear_only=True
    )
    ### production * leptonic branching ratio
    mu_dsig_dYemu_WW_ATLAS_13TeV = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_product( 
        r_dsig_dYemu_WW_ATLAS_13TeV_nodecay, 
        mu_BR_WW_emu,
        linear_only=True
      ),
      'mu_dsig_dYemu_WW_ATLAS_13TeV'
    )
    
    ## WZ CERN-EP-2018-327
    
    ### WZ leptonic branching ratio
    mu_BR_WZ_lep = mu_BR_W_emuv.product( mu_BR_Z_eemumu,linear_only=True)
    
    ## Memu distribution
    ### production
    r_dsig_dMTWZ_WZ_ATLAS_13TeV_nodecay = EFTInternalParameter.from_json(
      json_path+'r_dsig_dMTWZ_WZ_ATLAS_13TeV_NLO.json',
      linear_only=True
    )
    ### production * leptonic branching ratio
    mu_dsig_dMTWZ_WZ_ATLAS_13TeV = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_product(
        r_dsig_dMTWZ_WZ_ATLAS_13TeV_nodecay, 
        mu_BR_WZ_lep,
        linear_only=True
      ),
      'mu_dsig_dMTWZ_WZ_ATLAS_13TeV'
    )
    
    ## pTZ distribution
    ### production
    r_dsig_dpTZ_WZ_ATLAS_13TeV_nodecay = EFTInternalParameter.from_json(
      json_path+'r_dsig_dpTZ_WZ_ATLAS_13TeV_NLO.json',
      linear_only=True
    )
    ### production * leptonic branching ratio
    mu_dsig_dpTZ_WZ_ATLAS_13TeV = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_product(
        r_dsig_dpTZ_WZ_ATLAS_13TeV_nodecay,
        mu_BR_WZ_lep,
        linear_only=True
      ),
      'mu_dsig_dpTZ_WZ_ATLAS_13TeV'
    )
    
    ## pTW distribution
    ### production
    r_dsig_dpTW_WZ_ATLAS_13TeV_nodecay = EFTInternalParameter.from_json(
      json_path+'r_dsig_dpTW_WZ_ATLAS_13TeV_NLO.json',
      linear_only=True
      )
    ### production * leptonic branching ratio
    mu_dsig_dpTW_WZ_ATLAS_13TeV = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_product(
        r_dsig_dpTW_WZ_ATLAS_13TeV_nodecay,
        mu_BR_WZ_lep,
        linear_only=True
        ),
      'mu_dsig_dpTW_WZ_ATLAS_13TeV'
    )
    
    ## Zjj CERN-EP-2020-045
    ### production
    r_dsig_dphijj_Zjj_ATLAS_13TeV_nodecay = EFTInternalParameter.from_json(
      json_path+'r_dsig_dphijj_Zjj_ATLAS_13TeV_LO.json',
      linear_only=True
    )
    ### production * leptonic branching ratio
    mu_dsig_dphijj_Zjj_ATLAS_13TeV = EFTPrediction.from_EFTMethod(
      EFTInternalParameter.as_product(
        r_dsig_dphijj_Zjj_ATLAS_13TeV_nodecay, 
        mu_BR_Z_eemumu,
        linear_only=True
      ),
      'mu_dsig_dphijj_Zjj_ATLAS_13TeV'
    )
    
    # Some SMEFiT predictions
    dsig_dMTWZ_WZ_ATLAS_LO_13TeV = EFTPrediction.from_json(
      json_path_SMEFiT+'dsig_dMTWZ_WZ_ATLAS_NLO_13TeV.json',
      linear_only=True
    )
    dsig_dpTZ_WZ_CMS_LO_13TeV = EFTPrediction.from_json(
      json_path_SMEFiT+'dsig_dpTZ_WZ_CMS_NLO_13TeV.json',
      linear_only=True
    )
    dsig_dmemu_WW_ATLAS_LO_13TeV = EFTPrediction.from_json(
      json_path_SMEFiT+'dsig_dmemu_WW_ATLAS_NLO_13TeV.json',
      linear_only=True
    )
