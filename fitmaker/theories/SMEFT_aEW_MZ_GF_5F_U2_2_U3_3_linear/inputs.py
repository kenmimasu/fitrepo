from ..SM.aEW_MZ_GF_5F.inputs import aEW_MZ_GF_5F_Fixed

class aEW_MZ_GF_5F_U3_5(aEW_MZ_GF_5F_Fixed):
    external_parameters = [
      'CHWB', 'CHD', 'Cll', 'CHl3', 'CHl1', 'CHe','CHq3', 'CHq1', 'CHu', 'CHd',
      'CW','CG', 'CHbox', 'CHG', 'CHW', 'CHB', 'CeH', 'CuH', 'CdH', 'CuG', 
      'Cqq1', 'Cqq3', 'Cuu', 'Cud1', 'Cud8', 'Cqu1', 'Cqu8', 'Cqd1', 'Cqd8', 
      'Cquqd1', 'Cquqd8'    
    ]


class aEW_MZ_GF_5F_U2_2_U3_3(aEW_MZ_GF_5F_Fixed):
    external_parameters = [
      # bosonic
      'CHWB', 'CHD', 'Cll', 'CHbox', 'CHG', 'CHW', 'CHB', 'CW', 'CG',
      # flavor universal lepton & down quark currents
      'CHl3', 'CHl1', 'CHe', 'CHd',   
      # currents for 1st 2 gens.
      'CHq3', 'CHq1', 'CHu', 
      # currents for 3rd gen.
      'CHQ3', 'CHQ1', 'CHt', 
      # 3rd gen. Yukawas
      'CtaH', 'CtH', 'CbH',
      # 2nd gen. Yukawas
      'CmuH', 'CcH', 'CsH',
      # top-specific dipoles
      'CtG', 'CtW', 'CtB', 
       # interfering two-heavy-two-light 4-quark operators
      'CQq38', 'CQq18', 'CQu8', 'CQd8', 'Ctq8', 'Ctu8', 'Ctd8',
      'CQq31',
       # interfering four-heavy 4-quark operators
      'CQQ8', 'CQt8', 'CQb8', 'CQQ1', 'CQt1', 'CQb1', 'Ctb8', 'Ctt'
    ] 