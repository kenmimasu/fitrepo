from ....fitlib.theory import internal_parameter, prediction

from ...SM.aEW_MZ_GF_5F.higgs import (
  Production as SM_HiggsProduction, 
  Decay_old as SM_HiggsDecay # old ones
)

from ..higgs.decay import HiggsDecay as SMEFT_HiggsDecay_new # new ones
from ..higgs.decay import SMEFT_HiggsDecay_old as SMEFT_HiggsDecay # old ones
from ..higgs.production import HiggsProduction as SMEFT_HiggsProduction
from ..higgs.STXS import STXS_1p0 as SMEFT_HiggsProduction_STXS_1p0

class SMEFT_HiggsSignalStrengths_Run2_ATLAScombination_new(SM_HiggsProduction, SMEFT_HiggsProduction_STXS_1p0, SMEFT_HiggsProduction, SMEFT_HiggsDecay_new):
    '''
    Signal strengths for the ATLAS 80fb^-1 STXS stage 1.0 combination paper 
    using new decay info.
    '''
    @prediction('mu_WH0j_ptV_0T150_H_Zll_13')
    def mu_WH0j_ptV_0T150_H_Zll_13(self):
        rprod = self['ratio_WH0j_ptV_0T150']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth


    @prediction('mu_WH0j_ptV_150T250_H_Zll_13')
    def mu_WH0j_ptV_150T250_H_Zll_13(self):
        rprod = self['ratio_WH0j_ptV_150T250']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_WH0j_ptV_250Tinf_H_Zll_13')
    def mu_WH0j_ptV_250Tinf_H_Zll_13(self):
        rprod = self['ratio_WH0j_ptV_250Tinf']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_WH0j_ptV_0T250_H_Zll_13')
    def mu_WH0j_ptV_0T250_H_Zll_13(self):
        rprod = self['ratio_WH0j_ptV_0T250']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ZH0j_ptV_0T150_H_Zll_13')
    def mu_ZH0j_ptV_0T150_H_Zll_13(self):
        rprod = self['ratio_ZH0j_ptV_0T150']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ZH0j_ptV_150T250_H_Zll_13')
    def mu_ZH0j_ptV_150T250_H_Zll_13(self):
        rprod = self['ratio_ZH0j_ptV_150T250']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ZH0j_ptV_250Tinf_H_Zll_13')
    def mu_ZH0j_ptV_250Tinf_H_Zll_13(self):
        rprod = self['ratio_ZH0j_ptV_250Tinf']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_WH1j_ptV_150T250_H_Zll_13')
    def mu_WH1j_ptV_150T250_H_Zll_13(self):
        rprod = self['ratio_WH1j_ptV_150T250']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth


    @prediction('mu_ZH1j_ptV_150T250_H_Zll_13')
    def mu_ZH1j_ptV_150T250_H_Zll_13(self):
        rprod = self['ratio_ZH1j_ptV_150T250']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth


    @prediction('mu_qqH_rest_H_Zll_13')
    def mu_qqH_rest_H_Zll_13(self):
        rprod = self['ratio_qqH_rest']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth


    @prediction('mu_qqH_VBFtopo_H_Zll_13')
    def mu_qqH_VBFtopo_H_Zll_13(self):
        rprod = self['ratio_qqH_VBFtopo']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_qqH_VBFtopo_rest_H_Zll_13')
    def mu_qqH_VBFtopo_rest_H_Zll_13(self):
        rprod = self['ratio_qqH_VBFtopo_rest']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_qqH_ptj1_200Tinf_H_Zll_13')
    def mu_qqH_ptj1_200Tinf_H_Zll_13(self):
        rprod = self['ratio_qqH_ptj1_200Tinf']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_qqH_mjj_60T120_H_Zll_13')
    def mu_qqH_mjj_60T120_H_Zll_13(self):
        rprod = self['ratio_qqH_mjj_60T120']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ttH_H_Zll_13')
    def mu_ttH_H_Zll_13(self):
        rprod = self['dsigma_ttH_13']/self['sigma_ttH_13_SM']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF0j_H_Zll_13')
    def mu_ggF0j_H_Zll_13(self):
        rprod = self['ratio_ggF0j']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF1j_ptH_0T60_H_Zll_13')
    def mu_ggF1j_ptH_0T60_H_Zll_13(self):
        rprod = self['ratio_ggF1j_ptH_0T60']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF1j_ptH_60T120_H_Zll_13')
    def mu_ggF1j_ptH_60T120_H_Zll_13(self):
        rprod = self['ratio_ggF1j_ptH_60T120']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF1j_ptH_120T200_H_Zll_13')
    def mu_ggF1j_ptH_120T200_H_Zll_13(self):
        rprod = self['ratio_ggF1j_ptH_120T200']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF1j_ptH_200Tinf_H_Zll_13')
    def mu_ggF1j_ptH_200Tinf_H_Zll_13(self):
        rprod = self['ratio_ggF1j_ptH_200Tinf']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF2j_ptH_0T200_H_Zll_13')
    def mu_ggF2j_ptH_0T200_H_Zll_13(self):
        rprod = self['ratio_ggF2j_ptH_0T200']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF2j_ptH_0T60_H_Zll_13')
    def mu_ggF2j_ptH_0T60_H_Zll_13(self):
        rprod = self['ratio_ggF2j_ptH_0T60']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF2j_ptH_60T120_H_Zll_13')
    def mu_ggF2j_ptH_60T120_H_Zll_13(self):
        rprod = self['ratio_ggF2j_ptH_60T120']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF2j_ptH_120T200_H_Zll_13')
    def mu_ggF2j_ptH_120T200_H_Zll_13(self):
        rprod = self['ratio_ggF2j_ptH_120T200']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF2j_ptH_200Tinf_H_Zll_13')
    def mu_ggF2j_ptH_200Tinf_H_Zll_13(self):
        rprod = self['ratio_ggF2j_ptH_200Tinf']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF2j_mjj_400Tinf_ptH_0T200_H_Zll_13')
    def mu_ggF2j_mjj_400Tinf_ptH_0T200_H_Zll_13(self):
        rprod = self['ratio_ggF2j_mjj_400Tinf_ptH_0T200']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    # branching ratios to other final states divided by H->4l
    @prediction('mu_H_aaOverZZ')
    def mu_H_aaOverZZ_ATLAS_Run2(self):
        r_aa = self['rGam_H_aa']
        r_ZZ = self['rGam_H_4l']
        return 1. + r_aa - r_ZZ

    @prediction('mu_H_bbOverZZ')
    def mu_H_bbOverZZ_ATLAS_Run2(self):
        r_bb = self['rGam_H_bb']
        r_ZZ = self['rGam_H_4l']
        return 1. + r_bb - r_ZZ

    @prediction('mu_H_tataOverZZ')
    def mu_H_tataOverZZ_ATLAS_Run2(self):
        r_tata = self['rGam_H_tata']
        r_ZZ = self['rGam_H_4l']
        return 1. + r_tata - r_ZZ

    @prediction('mu_H_WWOverZZ')
    def mu_H_WWOverZZ_ATLAS_Run2(self):
        r_WW = self['rGam_H_WW_2l2v']
        r_ZZ = self['rGam_H_4l']
        return 1. + r_WW - r_ZZ

class SMEFT_HiggsSignalStrengths_Run2_ATLAScombination(SM_HiggsProduction, SMEFT_HiggsProduction_STXS_1p0, SMEFT_HiggsProduction, SMEFT_HiggsDecay, SM_HiggsDecay):
    '''
    Signal strengths for the ATLAS 80fb^-1 STXS stage 1.0 combination paper
    '''

    @prediction('mu_WH0j_ptV_0T150_H_Zll_13')
    def mu_WH0j_ptV_0T150_H_Zll_13(self):
        rprod = self['ratio_WH0j_ptV_0T150']
        rdecay = self['dGam_H_Zll']/self['Gam_H_Zll_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth


    @prediction('mu_WH0j_ptV_150T250_H_Zll_13')
    def mu_WH0j_ptV_150T250_H_Zll_13(self):
        rprod = self['ratio_WH0j_ptV_150T250']
        rdecay = self['dGam_H_Zll']/self['Gam_H_Zll_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_WH0j_ptV_250Tinf_H_Zll_13')
    def mu_WH0j_ptV_250Tinf_H_Zll_13(self):
        rprod = self['ratio_WH0j_ptV_250Tinf']
        rdecay = self['dGam_H_Zll']/self['Gam_H_Zll_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_WH0j_ptV_0T250_H_Zll_13')
    def mu_WH0j_ptV_0T250_H_Zll_13(self):
        rprod = self['ratio_WH0j_ptV_0T250']
        rdecay = self['dGam_H_Zll']/self['Gam_H_Zll_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ZH0j_ptV_0T150_H_Zll_13')
    def mu_ZH0j_ptV_0T150_H_Zll_13(self):
        rprod = self['ratio_ZH0j_ptV_0T150']
        rdecay = self['dGam_H_Zll']/self['Gam_H_Zll_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ZH0j_ptV_150T250_H_Zll_13')
    def mu_ZH0j_ptV_150T250_H_Zll_13(self):
        rprod = self['ratio_ZH0j_ptV_150T250']
        rdecay = self['dGam_H_Zll']/self['Gam_H_Zll_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth


    @prediction('mu_ZH0j_ptV_250Tinf_H_Zll_13')
    def mu_ZH0j_ptV_250Tinf_H_Zll_13(self):
        rprod = self['ratio_ZH0j_ptV_250Tinf']
        rdecay = self['dGam_H_Zll']/self['Gam_H_Zll_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_WH1j_ptV_150T250_H_Zll_13')
    def mu_WH1j_ptV_150T250_H_Zll_13(self):
        rprod = self['ratio_WH1j_ptV_150T250']
        rdecay = self['dGam_H_Zll']/self['Gam_H_Zll_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth


    @prediction('mu_ZH1j_ptV_150T250_H_Zll_13')
    def mu_ZH1j_ptV_150T250_H_Zll_13(self):
        rprod = self['ratio_ZH1j_ptV_150T250']
        rdecay = self['dGam_H_Zll']/self['Gam_H_Zll_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth


    @prediction('mu_qqH_rest_H_Zll_13')
    def mu_qqH_rest_H_Zll_13(self):
        rprod = self['ratio_qqH_rest']
        rdecay = self['dGam_H_Zll']/self['Gam_H_Zll_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth


    @prediction('mu_qqH_VBFtopo_H_Zll_13')
    def mu_qqH_VBFtopo_H_Zll_13(self):
        rprod = self['ratio_qqH_VBFtopo']
        rdecay = self['dGam_H_Zll']/self['Gam_H_Zll_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_qqH_VBFtopo_rest_H_Zll_13')
    def mu_qqH_VBFtopo_rest_H_Zll_13(self):
        rprod = self['ratio_qqH_VBFtopo_rest']
        rdecay = self['dGam_H_Zll']/self['Gam_H_Zll_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_qqH_ptj1_200Tinf_H_Zll_13')
    def mu_qqH_ptj1_200Tinf_H_Zll_13(self):
        rprod = self['ratio_qqH_ptj1_200Tinf']
        rdecay = self['dGam_H_Zll']/self['Gam_H_Zll_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth


    @prediction('mu_qqH_mjj_60T120_H_Zll_13')
    def mu_qqH_mjj_60T120_H_Zll_13(self):
        rprod = self['ratio_qqH_mjj_60T120']
        rdecay = self['dGam_H_Zll']/self['Gam_H_Zll_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth


    @prediction('mu_ttH_H_Zll_13')
    def mu_ttH_H_Zll_13(self):
        rprod = self['dsigma_ttH_13']/self['sigma_ttH_13_SM']
        rdecay = self['dGam_H_Zll']/self['Gam_H_Zll_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF0j_H_Zll_13')
    def mu_ggF0j_H_Zll_13(self):
        rprod = self['ratio_ggF0j']
        rdecay = self['dGam_H_Zll']/self['Gam_H_Zll_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF1j_ptH_0T60_H_Zll_13')
    def mu_ggF1j_ptH_0T60_H_Zll_13(self):
        rprod = self['ratio_ggF1j_ptH_0T60']
        rdecay = self['dGam_H_Zll']/self['Gam_H_Zll_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF1j_ptH_60T120_H_Zll_13')
    def mu_ggF1j_ptH_60T120_H_Zll_13(self):
        rprod = self['ratio_ggF1j_ptH_60T120']
        rdecay = self['dGam_H_Zll']/self['Gam_H_Zll_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF1j_ptH_120T200_H_Zll_13')
    def mu_ggF1j_ptH_120T200_H_Zll_13(self):
        rprod = self['ratio_ggF1j_ptH_120T200']
        rdecay = self['dGam_H_Zll']/self['Gam_H_Zll_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF1j_ptH_200Tinf_H_Zll_13')
    def mu_ggF1j_ptH_200Tinf_H_Zll_13(self):
        rprod = self['ratio_ggF1j_ptH_200Tinf']
        rdecay = self['dGam_H_Zll']/self['Gam_H_Zll_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF2j_ptH_0T200_H_Zll_13')
    def mu_ggF2j_ptH_0T200_H_Zll_13(self):
        rprod = self['ratio_ggF2j_ptH_0T200']
        rdecay = self['dGam_H_Zll']/self['Gam_H_Zll_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF2j_ptH_0T60_H_Zll_13')
    def mu_ggF2j_ptH_0T60_H_Zll_13(self):
        rprod = self['ratio_ggF2j_ptH_0T60']
        rdecay = self['dGam_H_Zll']/self['Gam_H_Zll_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF2j_ptH_60T120_H_Zll_13')
    def mu_ggF2j_ptH_60T120_H_Zll_13(self):
        rprod = self['ratio_ggF2j_ptH_60T120']
        rdecay = self['dGam_H_Zll']/self['Gam_H_Zll_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF2j_ptH_120T200_H_Zll_13')
    def mu_ggF2j_ptH_120T200_H_Zll_13(self):
        rprod = self['ratio_ggF2j_ptH_120T200']
        rdecay = self['dGam_H_Zll']/self['Gam_H_Zll_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF2j_ptH_200Tinf_H_Zll_13')
    def mu_ggF2j_ptH_200Tinf_H_Zll_13(self):
        rprod = self['ratio_ggF2j_ptH_200Tinf']
        rdecay = self['dGam_H_Zll']/self['Gam_H_Zll_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF2j_mjj_400Tinf_ptH_0T200_H_Zll_13')
    def mu_ggF2j_mjj_400Tinf_ptH_0T200_H_Zll_13(self):
        rprod = self['ratio_ggF2j_mjj_400Tinf_ptH_0T200']
        rdecay = self['dGam_H_Zll']/self['Gam_H_Zll_SM']
        rwidth = self['dGam_H']/self['Gam_H_SM']
        return 1. + rprod + rdecay - rwidth


    @prediction('mu_H_aaOverZZ')
    def mu_H_aaOverZZ_ATLAS_Run2(self):
        r_aa = self['dGam_H_aa']/self['Gam_H_aa_SM']
        r_ZZ = self['dGam_H_Zll']/self['Gam_H_Zll_SM']
        return 1. + r_aa - r_ZZ

    @prediction('mu_H_bbOverZZ')
    def mu_H_bbOverZZ_ATLAS_Run2(self):
        r_bb = self['dGam_H_bb']/self['Gam_H_bb_SM']
        r_ZZ = self['dGam_H_Zll']/self['Gam_H_Zll_SM']
        return 1. + r_bb - r_ZZ

    @prediction('mu_H_tataOverZZ')
    def mu_H_tataOverZZ_ATLAS_Run2(self):
        r_tata = self['dGam_H_tata']/self['Gam_H_tata_SM']
        r_ZZ = self['dGam_H_Zll']/self['Gam_H_Zll_SM']
        return 1. + r_tata - r_ZZ

    @prediction('mu_H_WWOverZZ')
    def mu_H_WWOverZZ_ATLAS_Run2(self):
        r_WW = self['dGam_H_Wlvl']/self['Gam_H_Wlvl_SM']
        r_ZZ = self['dGam_H_Zll']/self['Gam_H_Zll_SM']
        return 1. + r_WW - r_ZZ

