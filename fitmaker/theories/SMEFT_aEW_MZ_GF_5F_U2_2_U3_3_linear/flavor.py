from ...fitlib.constants import pi, GeV2pb 
from ...fitlib.theory import internal_parameter, prediction
from ..EFT import EFTBase, EFTPrediction, EFTInternalParameter
from .inputs import aEW_MZ_GF_5F_U2_2_U3_3
from . import data_path

json_path = data_path+'Flavor/'

class CKM(EFTBase, aEW_MZ_GF_5F_U2_2_U3_3):
    '''
    SMEFT Warsaw basis for CKM unitarity violation production, based on 2204.08440 & refs therein.
    '''
    deltaCKM = EFTPrediction.from_json(
      json_path+'deltaCKM.json', linear_only=True
    )