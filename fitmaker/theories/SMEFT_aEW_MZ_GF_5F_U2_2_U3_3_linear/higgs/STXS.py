from ....fitlib.constants import pi, GeV2pb
from ....fitlib.theory import internal_parameter, prediction

from ...EFT import EFTBase, EFTPrediction, EFTInternalParameter
from ...SM.aEW_MZ_GF_5F.higgs import Decay_old as SM_HiggsDecay_old, Decay as SM_HiggsDecay
from ...SM.aEW_MZ_GF_5F.higgs import Production as SM_HiggsProduction

from ..LEP import SMEFT_Deltas
from ..inputs import aEW_MZ_GF_5F_U2_2_U3_3
from .. import data_path

from .production import HiggsProduction as SMEFT_HiggsProduction
from .decay import HiggsDecay as SMEFT_HiggsDecay, SMEFT_HiggsDecay_old

path = data_path+'STXS/'
rename = {
  "CuH":"CtH",
  "CuG":"CtG",
  "CeH":"CtaH",
  "CdH":"CbH"
}
class STXS_1p1(EFTBase, aEW_MZ_GF_5F_U2_2_U3_3):
    '''
    SMEFT Warsaw basis for Higgs production, STXS, stage 1.1
    '''

    #VH production, stage 1.1 main splitting
    #change in the LO production cross section as a ratio to the SM
    ratio_WH0j_ptV_0T75 = EFTInternalParameter.from_json(path+"WH0j_ptV_0T75.json",linear_only=True,rename=rename)
    ratio_WH0j_ptV_75T150 = EFTInternalParameter.from_json(path+"WH0j_ptV_75T150.json",linear_only=True,rename=rename)
    ratio_WH0j_ptV_150T250 = EFTInternalParameter.from_json(path+"WH0j_ptV_150T250.json",linear_only=True,rename=rename)
    ratio_WH1j_ptV_150T250 = EFTInternalParameter.from_json(path+"WH1j_ptV_150T250.json",linear_only=True,rename=rename)
    ratio_WH0j_ptV_250Tinf = EFTInternalParameter.from_json(path+"WH0j_ptV_250Tinf.json",linear_only=True,rename=rename)
    ratio_ZH0j_ptV_0T75 = EFTInternalParameter.from_json(path+"ZH0j_ptV_0T75.json",linear_only=True,rename=rename)
    ratio_ZH0j_ptV_75T150 = EFTInternalParameter.from_json(path+"ZH0j_ptV_75T150.json",linear_only=True,rename=rename)
    ratio_ZH0j_ptV_150T250 = EFTInternalParameter.from_json(path+"ZH0j_ptV_150T250.json",linear_only=True,rename=rename)
    ratio_ZH0j_ptV_250Tinf = EFTInternalParameter.from_json(path+"ZH0j_ptV_250Tinf.json",linear_only=True,rename=rename)
    ratio_ZH1j_ptV_150T250 = EFTInternalParameter.from_json(path+"ZH1j_ptV_150T250.json",linear_only=True,rename=rename)

    #Merged ZH+WH bins for CMS H -> ZZ* -> 4l paper
    ratio_VH_ptV_0T150 = EFTInternalParameter.from_json(path+"VH_ptV_0T150.json",linear_only=True,rename=rename)
    ratio_VH_ptV_150Tinf = EFTInternalParameter.from_json(path+"VH_ptV_150Tinf.json",linear_only=True,rename=rename)


    #EW qqH - merged bins for CMS H -> ZZ* -> 4l paper
    ratio_qqH_rest = EFTInternalParameter.from_json(path+"qqH_rest.json",linear_only=True,rename=rename)
    ratio_qqH_mjj_60T120 = EFTInternalParameter.from_json(path+"qqH_mjj_60T120.json",linear_only=True,rename=rename)
    ratio_qqH_mjj_350T700_ptH_0T200_ptHjj_0T25 = EFTInternalParameter.from_json(path+"qqH_mjj_350T700_ptH_0T200_ptHjj_0T25.json",linear_only=True,rename=rename)
    ratio_qqH_mjj_700Tinf_ptH_0T200_ptHjj_0T25 = EFTInternalParameter.from_json(path+"qqH_mjj_700Tinf_ptH_0T200_ptHjj_0T25.json",linear_only=True,rename=rename)
    ratio_qqH_mjj_350Tinf_ptH_0T200_ptHjj_25Tinf = EFTInternalParameter.from_json(path+"qqH_mjj_350Tinf_ptH_0T200_ptHjj_25Tinf.json",linear_only=True,rename=rename)
    ratio_qqH_mjj_350Tinf_ptH_200Tinf = EFTInternalParameter.from_json(path+"qqH_mjj_350Tinf_ptH_200Tinf.json",linear_only=True,rename=rename)

    #ggF
    ratio_ggF0j_ptH_0T10 = EFTInternalParameter.from_json(path+"r_ggF0j_ptH_0T10.json",linear_only=True,rename=rename)
    ratio_ggF0j_ptH_10T200 = EFTInternalParameter.from_json(path+"r_ggF0j_ptH_10T200.json",linear_only=True,rename=rename)
    ratio_ggF1j_ptH_0T60 = EFTInternalParameter.from_json(path+"r_ggF1j_ptH_0T60.json",linear_only=True,rename=rename)
    ratio_ggF1j_ptH_60T120 = EFTInternalParameter.from_json(path+"r_ggF1j_ptH_60T120.json",linear_only=True,rename=rename)
    ratio_ggF1j_ptH_120T200 = EFTInternalParameter.from_json(path+"r_ggF1j_ptH_120T200.json",linear_only=True,rename=rename)
    ratio_ggF_ptH_200Tinf = EFTInternalParameter.from_json(path+"r_ggF_ptH_200Tinf.json",linear_only=True,rename=rename)
    ratio_ggF2j_mjj_0T350_ptH_0T60 = EFTInternalParameter.from_json(path+"r_ggF2j_mjj_0T350_ptH_0T60_ptHjj_0T25.json",linear_only=True,rename=rename)
    ratio_ggF2j_mjj_0T350_ptH_60T120 = EFTInternalParameter.from_json(path+"r_ggF2j_mjj_0T350_ptH_60T120_ptHjj_0T25.json",linear_only=True,rename=rename)
    ratio_ggF2j_mjj_0T350_ptH_120T200 = EFTInternalParameter.from_json(path+"r_ggF2j_mjj_0T350_ptH_120T200_ptHjj_0T25.json",linear_only=True,rename=rename)
    ratio_ggF2j_mjj_350Tinf = EFTInternalParameter.from_json(path+"r_ggF2j_mjj_350Tinf.json",linear_only=True,rename=rename)

class STXS_1p0(EFTBase, aEW_MZ_GF_5F_U2_2_U3_3):
    '''
    SMEFT Warsaw basis for Higgs production, STXS, stage 1.0
    '''
    #VH production, stage 1.0
    #change in the LO production cross section as a ratio to the SM (linear in WC only)
    # path =  os.getcwd()+"/fitmaker/theories/STXS/"
    ratio_WH0j_ptV_0T150 = EFTInternalParameter.from_json(path+"WH0j_ptV_0T150.json",linear_only=True,rename=rename)
    ratio_WH0j_ptV_150T250 = EFTInternalParameter.from_json(path+"WH0j_ptV_150T250.json",linear_only=True,rename=rename)
    ratio_WH1j_ptV_150T250 = EFTInternalParameter.from_json(path+"WH1j_ptV_150T250.json",linear_only=True,rename=rename)
    ratio_WH0j_ptV_250Tinf = EFTInternalParameter.from_json(path+"WH0j_ptV_250Tinf.json",linear_only=True,rename=rename)
    ratio_ZH0j_ptV_0T150 = EFTInternalParameter.from_json(path+"ZH0j_ptV_0T150.json",linear_only=True,rename=rename)
    ratio_ZH0j_ptV_150T250 = EFTInternalParameter.from_json(path+"ZH0j_ptV_150T250.json",linear_only=True,rename=rename)
    ratio_ZH0j_ptV_250Tinf = EFTInternalParameter.from_json(path+"ZH0j_ptV_250Tinf.json",linear_only=True,rename=rename)
    ratio_ZH1j_ptV_150T250 = EFTInternalParameter.from_json(path+"ZH1j_ptV_150T250.json",linear_only=True,rename=rename)

    #WH production, merged stage 1.0 for ATLAS combination
    ratio_WH0j_ptV_0T250 = EFTInternalParameter.from_json(path+"WH0j_ptV_0T250.json",linear_only=True,rename=rename)

    #EW qqH production, stage 1.0
    #change in the LO production cross section as a ratio to the SM (linear in WC only)
    ratio_qqH_rest = EFTInternalParameter.from_json(path+"qqH_rest.json",linear_only=True,rename=rename)
    ratio_qqH_ptj1_200Tinf = EFTInternalParameter.from_json(path+"qqH_ptj1_200Tinf.json",linear_only=True,rename=rename)
    ratio_qqH_mjj_60T120 = EFTInternalParameter.from_json(path+"qqH_mjj_60T120.json",linear_only=True,rename=rename)
    ratio_qqH_VBFtopo = EFTInternalParameter.from_json(path+"qqH_VBFtopo.json",linear_only=True,rename=rename)

    #EW qqH production, merged stage 1.0 for ATLAS combination
    ratio_qqH_VBFtopo_rest = EFTInternalParameter.from_json(path+"qqH_VBFtopo_rest.json",linear_only=True,rename=rename)

    #EW qqH production, merged VH+rest+BSM bin for CMS H-aa
    ratio_qqH_other = EFTInternalParameter.from_json(path+"qqH_other.json",linear_only=True,rename=rename)

    #ggF production, coarse stage 1.0 binning for ATLAS combination
    ratio_ggF0j = EFTInternalParameter.from_json(path+"r_ggF0j.json",linear_only=True,rename=rename)
    ratio_ggF1j_ptH_0T60 = EFTInternalParameter.from_json(path+"r_ggF1j_ptH_0T60.json",linear_only=True,rename=rename)
    ratio_ggF1j_ptH_60T120 = EFTInternalParameter.from_json(path+"r_ggF1j_ptH_60T120.json",linear_only=True,rename=rename)
    ratio_ggF1j_ptH_120T200 = EFTInternalParameter.from_json(path+"r_ggF1j_ptH_120T200.json",linear_only=True,rename=rename)
    ratio_ggF1j_ptH_200Tinf = EFTInternalParameter.from_json(path+"r_ggF1j_ptH_200Tinf.json",linear_only=True,rename=rename)
    ratio_ggF2j_ptH_0T200 = EFTInternalParameter.from_json(path+"r_ggF2j_ptH_0T200.json",linear_only=True,rename=rename)
    ratio_ggF2j_ptH_0T60 = EFTInternalParameter.from_json(path+"r_ggF2j_ptH_0T60.json",linear_only=True,rename=rename)
    ratio_ggF2j_ptH_60T120 = EFTInternalParameter.from_json(path+"r_ggF2j_ptH_60T120_1p0.json",linear_only=True,rename=rename)
    ratio_ggF2j_ptH_120T200 = EFTInternalParameter.from_json(path+"r_ggF2j_ptH_120T200_1p0.json",linear_only=True,rename=rename)
    ratio_ggF2j_ptH_200Tinf = EFTInternalParameter.from_json(path+"r_ggF2j_ptH_200Tinf_1p0.json",linear_only=True,rename=rename)
    ratio_ggF2j_mjj_400Tinf_ptH_0T200 = EFTInternalParameter.from_json(path+"r_ggF2j_mjj_400Tinf_ptH_0T200_1p0.json",linear_only=True,rename=rename)

    #ggF production, merged stage 1.0 binning for CMS H->tata paper
    ratio_ggF1j_ptH_120Tinf = EFTInternalParameter.from_json(path+"r_ggF1j_ptH_120Tinf.json",linear_only=True,rename=rename)
    ratio_ggF2j = EFTInternalParameter.from_json(path+"r_ggF2j.json",linear_only=True,rename=rename)

    #merged ggF + EW qqH VBF topology STXS stage 1.0 for CMS H->tata paper
    ratio_ggF_plus_qqH_VBF = EFTInternalParameter.from_json(path+"r_ggF_plus_qqH_VBF.json",linear_only=True,rename=rename)
