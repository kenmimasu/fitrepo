from ....fitlib.constants import pi, GeV2pb 
from ....fitlib.theory import internal_parameter, prediction

from ...SM.aEW_MZ_GF_5F.higgs import Decay as SM_aEW_MZ_GF_5F_HiggsDecay
from ...EFT import EFTBase, EFTPrediction, EFTInternalParameter

from .. import data_path
from ..inputs import aEW_MZ_GF_5F_U2_2_U3_3

import numpy as np

json_path = data_path + 'HiggsProduction/'

class HiggsProduction(EFTBase, aEW_MZ_GF_5F_U2_2_U3_3):
    '''
    SMEFT Warsaw basis for Higgs production, based on "SMEFT_global_fit_warsaw.nb". 
    '''
    
    # LO LHC Higgs production cross sections in pb
    @internal_parameter(depends_on=('CHG','CtH','CtG','CHbox','CHD','CHl3','Cll'))
    def dsigma_ggF_13(self):
        '''SMEFT gluon gluon fusion Higgs production cross section @ 13 TeV in pb'''
        # return 630.9*self['CHG']*1e6/self.Lambda**2
        # recomputed with SMEFTatNLO 
        coeffs = np.array([
          self['CHG'], self['CtH'], self['CtG'], 
          self['CHbox'], self['CHD'], self['CHl3'], self['Cll']
        ])
        lin = np.array([631., -2.14, 16.9, 2.13, -2.13/4., -2.13, 2.13/2.])
        return np.dot(coeffs, lin)*1e6/self.Lambda**2

        
    @internal_parameter(depends_on=('CHG','CtH','CtG','CHbox','CHD','CHl3','Cll'))
    def dsigma_ggF_8(self):
        '''SMEFT Gluon gluon fusion Higgs production cross section @ 8 TeV in pb'''
        # return 263.8*self['CHG']*1e6/self.Lambda**2
        # recomputed with SMEFTatNLO
        coeffs = np.array([
          self['CHG'], self['CtH'], self['CtG'], 
          self['CHbox'], self['CHD'], self['CHl3'], self['Cll']
        ])
        lin = np.array([264., -0.897, 7.06, 0.892, -0.892/4., -0.892, 0.892/2.])
        return np.dot(coeffs, lin)*1e6/self.Lambda**2

    @internal_parameter(depends_on=('CG','CHG','CtH','CtG','CHbox','CHD','CHl3','Cll'))
    def dsigma_ggjH_13(self):
        '''SMEFT gluon gluon fusion Higgs production + 1 jet cross section @ 13 TeV in pb'''
        # return (0.5277*self['CG'] + 455.0*self['CHG'])*1e6/self.Lambda**2
        # recomputed with SMEFTatNLO (eta_H < 2.5, OK for ratio to SM)
        coeffs = np.array([
          self['CHG'], self['CtH'], self['CtG'], self['CG'], 
          self['CHbox'], self['CHD'], self['CHl3'], self['Cll']
        ])
        lin = np.array([339., -1.14, 9.11, -0.31, 1.13, -1.13/4., -1.13, 1.13/2.])
        return np.dot(coeffs, lin)*1e6/self.Lambda**2

    @internal_parameter(depends_on=('CG','CHG','CtH','CtG','CHbox','CHD','CHl3','Cll'))
    def dsigma_ggjH_13_ptjG450(self):
        '''SMEFT Gluon gluon fusion Higgs production + 1 jet cross section @ 13 TeV in pb.
        Includes a pT(jet) > 450 GeV cut.
        '''
        #return (0.0436*self['CG'] + 0.7826*self['CHG'])*1e6/self.Lambda**2
        # recomputed with SMEFTatNLO (eta_H < 2.5, OK for ratio to SM)
        coeffs = np.array([
          self['CHG'], self['CtH'], self['CtG'], self['CG'], 
          self['CHbox'], self['CHD'], self['CHl3'], self['Cll']
        ])
        lin = np.array([1.29, -0.00245, 0.0482, -0.00825, 0.00244, -0.00244/4., -0.00244, 0.00244/2.,])
        return np.dot(coeffs, lin)*1e6/self.Lambda**2
        
    # dsigma_ttH_13 = EFTInternalParameter( # UPDATE
    #   params = [
    #     'CG', 'CHbox', 'CHD', 'CHG', 'CuH', 'CuG', 'CHu', 'Cqq1', 'Cqq3',
    #     'Cuu', 'Cud1', 'Cud8', 'Cqu1', 'Cqu8', 'CHq1', 'CHq3', 'Cqd1',
    #     'Cqd8', 'Cquqd1', 'Cquqd8', 'Cll'
    #   ],
    #   linear = [
    #     0.06063, 0.0512, -0.01289, 0.2397, -0.05116, -0.363, 0.0001831,
    #     0.004251, 0.01421, 0.005303, -0.000838, 0.01133, 0.003284,  0.04702,
    #     -0.0003053, 0.001343, -0.0005221, 0.01131, -7.523e-10, -2.209e-13,
    #     2.*0.0129
    #   ],
    #   doc='''SMEFT top-pair associated Higgs production cross section @ 13 TeV in pb''',
    #   lambda_gen=1000.
    # )
    #
    dsigma_ttH_13= EFTInternalParameter.from_json(json_path+"dsigma_ttH_13.json")
    
    # dsigma_ttH_8 = EFTInternalParameter( # UPDATE
    #   params = [
    #     'CG', 'CHbox', 'CHD', 'CHG', 'CuH', 'CuG', 'Cqq1', 'Cqq3', 'Cuu',
    #     'Cud1', 'Cud8', 'Cqu8', 'Cqd8', 'CHq1', 'CHq3', 'CHu', 'Cll'
    #   ],
    #   linear = [
    #     0.01528, 0.01385, -0.003487, 0.06331, -0.01388, -0.09463, 0.001445,
    #     0.004481, 0.001682, -0.0002557, 0.003582, 0.01523, 0.003564,
    #     -0.0001034, 0.0003671, 3.851e-5, 0.007012
    #   ],
    #   doc='''SMEFT top-pair associated Higgs production cross section @ 8 TeV in pb''',
    #   lambda_gen=1000.
    # )
    dsigma_ttH_8= EFTInternalParameter.from_json(json_path+"dsigma_ttH_8.json")
    
    
    dsigma_WH_13 = EFTInternalParameter(
      params = [
        'CHbox', 'CHD', 'CHW', 'CHB', 'CHWB', 'CHl1', 'CHl3', 'CHe', 'CHq1', 
        'CHq3', 'CHu', 'CHd', 'Cll'
      ],
      linear=[
        0.1249, -0.1739, 0.9143, 0., -0.3296, 0., -0.4379, 0., 0., 1.993, 0.,
        0., 2.*0.1021
      ],
      doc='''SMEFT W associated Higgs production cross section @ 13 TeV in pb''',
      lambda_gen=1000.
    )
    
    dsigma_WH_8 = EFTInternalParameter(
      params = [
        'CHbox', 'CHD', 'CHW', 'CHB', 'CHWB', 'CHl1', 'CHl3', 'CHe', 'CHq1', 
        'CHq3', 'CHu', 'CHd', 'Cll'
      ],
      linear=[
        0.06383, -0.0891, 0.4639, 0., -0.1688, 0., -0.2242, 0., 0., 0.9521, 
        0., 0., 2*0.05226
      ],
      doc='''SMEFT W associated Higgs production cross section @ 8 TeV in pb''',
      lambda_gen=1000.
    )
    
    dsigma_ZH_13 = EFTInternalParameter(
      params = [
        'CHbox', 'CHD', 'CHW', 'CHB', 'CHWB', 'CHl1', 'CHl3', 'CHe', 'CHq1', 
        'CHq3', 'CHu', 'CHd', 'Cll'
      ],
      linear=[
        0.06655, -0.008074, 0.3934, 0.05354, 0.1185, 0., -0.1658, 0., -0.07362, 
        1.014, 0.2636, -0.09145, 2*0.04145
      ],
      doc='''SMEFT Z associated Higgs production cross section @ 13 TeV in pb''',
      lambda_gen=1000.
    )

    dsigma_ZH_8 = EFTInternalParameter(
      params = [
        'CHbox', 'CHD', 'CHW', 'CHB', 'CHWB', 'CHl1', 'CHl3', 'CHe', 'CHq1', 
        'CHq3', 'CHu', 'CHd', 'Cll'
      ],
      linear=[
        0.03356, -0.004105, 0.1968, 0.02662, 0.05899, 0., -0.08375, 0., 
        -0.04474, 0.4793, 0.1263, -0.04212, 2.*0.02093
      ],
      doc='''SMEFT Z associated Higgs production cross section @ 8 TeV in pb''',
      lambda_gen=1000.
    )
    
    dsigma_VBF_8 = EFTInternalParameter.from_json(json_path+"dsigma_VBF_8.json")
    dsigma_VBF_13 = EFTInternalParameter.from_json(json_path+"dsigma_VBF_13.json")
    dsigma_VBF_13_ptjL200 = EFTInternalParameter.from_json(json_path+"dsigma_VBF_ptjL200_13.json")
    dsigma_VBF_13_ptjG200 = EFTInternalParameter.from_json(json_path+"dsigma_VBF_ptjG200_13.json")

    @internal_parameter(depends_on=(
      'dsigma_ggF_13','dsigma_VBF_13','dsigma_WH_13','dsigma_ZH_13','dsigma_ttH_13'
    ))
    def dsigma_13(self):
        '''SMEFT total Higgs production  @ 13 TeV in pb'''
        return (
          self['dsigma_ggF_13'] + self['dsigma_VBF_13'] + self['dsigma_WH_13'] 
        + self['dsigma_ZH_13'] + self['dsigma_ttH_13'] 
        )
    
    @internal_parameter(depends_on=(
      'dsigma_ggF_8','dsigma_VBF_8','dsigma_WH_8','dsigma_ZH_8','dsigma_ttH_8'
    ))
    def dsigma_8(self):
        '''SMEFT total Higgs production  @ 8 TeV in pb'''
        return (
          self['dsigma_ggF_8'] + self['dsigma_VBF_8'] + self['dsigma_WH_8'] 
        + self['dsigma_ZH_8'] + self['dsigma_ttH_8'] 
        )

    @internal_parameter(depends_on=('CG','CHG','CtH','CtG','CHbox','CHD','CHl3','Cll'))
    def dsigma_ggjjH_13(self): # WHy is this larger than ggF?
        '''SMEFT Gluon gluon fusion Higgs production + 2 jet cross section @ 13 TeV in pb'''
        # return (0.4543*self['CG'] + 256.4*self['CHG'])*1e6/self.Lambda**2
        # recomputed with SMEFTatNLO (|eta_H| < 2.5, OK for ratio)
        coeffs = np.array([
          self['CHG'], self['CtH'], self['CtG'], self['CG'], 
          self['CHbox'], self['CHD'], self['CHl3'], self['Cll']
        ])
        lin = np.array([174.2, -0.579, 6.5419, -0.3657, 0.576, -0.576/4., -0.576, 0.576/2.])
        return np.dot(coeffs, lin)*1e6/self.Lambda**2
        
    @internal_parameter(depends_on=('CG','CHG','CtH','CtG','CHbox','CHD','CHl3','Cll'))
    def dsigma_ggjH_13_pt20T60(self):
        '''SMEFT Gluon gluon fusion Higgs production + 1 jet cross section @ 13 TeV in pb.
        Includes cuts: 20 < pT(H) < 60 GeV.
        '''
        # return (0.09005*self['CG'] + 317.8*self['CHG'])*1e6/self.Lambda**2
        # recomputed with SMEFTatNLO (|eta_H| < 2.5, OK for ratio)
        coeffs = np.array([
          self['CHG'], self['CtH'], self['CtG'], self['CG'], 
          self['CHbox'], self['CHD'], self['CHl3'], self['Cll']
        ])
        lin = np.array([180.9, -0.613, 4.84, -0.0572, 0.61, -0.61/4., -0.61, 0.61/2.])
        return np.dot(coeffs, lin)*1e6/self.Lambda**2

    @internal_parameter(depends_on=('CG','CHG','CtH','CtG','CHbox','CHD','CHl3','Cll'))
    def dsigma_ggjH_13_pt60T120(self):
        '''SMEFT Gluon gluon fusion Higgs production + 1 jet cross section @ 13 TeV in pb.
        Includes cuts: 60 < pT(H) < 120 GeV.
        '''
        # return (0.1331*self['CG'] + 95.38*self['CHG'])*1e6/self.Lambda**2
        # recomputed with SMEFTatNLO (|eta_H| < 2.5, OK for ratio)
        coeffs = np.array([
          self['CHG'], self['CtH'], self['CtG'], self['CG'], 
          self['CHbox'], self['CHD'], self['CHl3'], self['Cll']
        ])
        lin = np.array([104.3, -0.352, 2.8, -0.11, 0.35, -0.35/4., -0.35, 0.35/2.])
        return np.dot(coeffs, lin)*1e6/self.Lambda**2
        
        
    @internal_parameter(depends_on=('CG','CHG','CtH','CtG','CHbox','CHD','CHl3','Cll'))
    def dsigma_ggjH_13_pt120T200(self):
        '''SMEFT Gluon gluon fusion Higgs production + 1 jet cross section @ 13 TeV in pb.
        Includes cuts: 120 < pT(H) < 200 GeV.
        '''
        # return (0.1223*self['CG'] + 28.32*self['CHG'])*1e6/self.Lambda**2
        # recomputed with SMEFTatNLO (|eta_H| < 2.5, OK for ratio)
        coeffs = np.array([
          self['CHG'], self['CtH'], self['CtG'], self['CG'], 
          self['CHbox'], self['CHD'], self['CHl3'], self['Cll']
        ])
        lin = np.array([36.0, -0.1206, 0.975, -0.0933, 0.1199, -0.1199/4., -0.1199, 0.1199/2.])
        return np.dot(coeffs, lin)*1e6/self.Lambda**2
        
    @internal_parameter(depends_on=('CG','CHG','CtH','CtG','CHbox','CHD','CHl3','Cll'))
    def dsigma_ggjH_13_ptG200(self):
        '''SMEFT Gluon gluon fusion Higgs production + 1 jet cross section @ 13 TeV in pb.
        Includes cuts: pT(H) > 200 GeV.
        '''
        # return (0.185*self['CG'] + 11.36*self['CHG'])*1e6/self.Lambda**2
        # recomputed with SMEFTatNLO (|eta_H| < 2.5, OK for ratio)
        coeffs = np.array([
          self['CHG'], self['CtH'], self['CtG'], self['CG'], 
          self['CHbox'], self['CHD'], self['CHl3'], self['Cll']
        ])
        lin = np.array([17.27, -0.0511, 0.493, -0.0528, 0.0508, -0.0508/4., -0.0508, 0.0508/2.])
        return np.dot(coeffs, lin)*1e6/self.Lambda**2
        
        