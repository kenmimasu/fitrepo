from ....fitlib.constants import pi, GeV2pb
from ....fitlib.theory import internal_parameter, prediction

from ...EFT import EFTPrediction, EFTInternalParameter
from ...SM.aEW_MZ_GF_5F.higgs import Decay_old as SM_HiggsDecay_old, Decay as SM_HiggsDecay
from ...SM.aEW_MZ_GF_5F.higgs import Production as SM_HiggsProduction

from ..LEP import SMEFT_Deltas
from .. import data_path

from .production import HiggsProduction as SMEFT_HiggsProduction
from .decay import HiggsDecay as SMEFT_HiggsDecay, SMEFT_HiggsDecay_old

class Run1(SM_HiggsProduction, SMEFT_HiggsProduction, SMEFT_HiggsDecay, SMEFT_Deltas):
    '''
    SMEFT Warsaw basis LHC Run 1 Higgs SignalStrengths, based on "SMEFT_global_fit_warsaw.nb".
    Takes Higgs production and decay fromm SMEFT_HiggsProduction & SMEFT_HiggsDecay.
    All linear coefficients validated to one part in 1e-12 or better.
    '''
    # LHC Run 1 observable predictions
    @prediction('mu_ggF_H_aa_8')
    def mu_ggF_H_aa_8(self):
        rprod = self['dsigma_ggF_8']/self['sigma_ggF_8_SM']
        rdecay = self['rGam_H_aa']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF_H_ZZ_8')
    def mu_ggF_H_ZZ_8(self):
        rprod = self['dsigma_ggF_8']/self['sigma_ggF_8_SM']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF_H_WW_8')
    def mu_ggF_H_WW_8(self):
        rprod = self['dsigma_ggF_8']/self['sigma_ggF_8_SM']
        rdecay = self['rGam_H_WW_2l2v']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF_H_tata_8')
    def mu_ggF_H_tata_8(self):
        rprod = self['dsigma_ggF_8']/self['sigma_ggF_8_SM']
        rdecay = self['rGam_H_tata']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_VBF_H_aa_8')
    def mu_VBF_H_aa_8(self):
        rprod = self['dsigma_VBF_8']/self['sigma_VBF_8_SM']
        rdecay = self['rGam_H_aa']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_VBF_H_ZZ_8')
    def mu_VBF_H_ZZ_8(self):
        rprod = self['dsigma_VBF_8']/self['sigma_VBF_8_SM']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_VBF_H_WW_8')
    def mu_VBF_H_WW_8(self):
        rprod = self['dsigma_VBF_8']/self['sigma_VBF_8_SM']
        rdecay = self['rGam_H_WW_2l2v']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_VBF_H_tata_8')
    def mu_VBF_H_tata_8(self):
        rprod = self['dsigma_VBF_8']/self['sigma_VBF_8_SM']
        rdecay = self['rGam_H_tata']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_WH_H_aa_8')
    def mu_WH_H_aa_8(self):
        rprod = self['dsigma_WH_8']/self['sigma_WH_8_SM']
        rdecay = self['rGam_H_aa']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_WH_H_WW_8')
    def mu_WH_H_WW_8(self):
        rprod = self['dsigma_WH_8']/self['sigma_WH_8_SM']
        rdecay = self['rGam_H_WW_2l2v']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_WH_H_tata_8')
    def mu_WH_H_tata_8(self):
        rprod = self['dsigma_WH_8']/self['sigma_WH_8_SM']
        rdecay = self['rGam_H_tata']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_WH_H_bb_8')
    def mu_WH_H_ZZ_8(self):
        rprod = self['dsigma_WH_8']/self['sigma_WH_8_SM']
        rdecay = self['rGam_H_bb']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ZH_H_aa_8')
    def mu_ZH_H_aa_8(self):
        rprod = self['dsigma_ZH_8']/self['sigma_ZH_8_SM']
        rdecay = self['rGam_H_aa']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ZH_H_WW_8')
    def mu_ZH_H_WW_8(self):
        rprod = self['dsigma_ZH_8']/self['sigma_ZH_8_SM']
        rdecay = self['rGam_H_WW_2l2v']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ZH_H_tata_8')
    def mu_ZH_H_tata_8(self):
        rprod = self['dsigma_ZH_8']/self['sigma_ZH_8_SM']
        rdecay = self['rGam_H_tata']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ZH_H_bb_8')
    def mu_ZH_H_ZZ_8(self):
        rprod = self['dsigma_ZH_8']/self['sigma_ZH_8_SM']
        rdecay = self['rGam_H_bb']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ttH_H_aa_8')
    def mu_ttH_H_aa_8(self):
        rprod = self['dsigma_ttH_8']/self['sigma_ttH_8_SM']
        rdecay = self['rGam_H_aa']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ttH_H_WW_8')
    def mu_ttH_H_WW_8(self):
        rprod = self['dsigma_ttH_8']/self['sigma_ttH_8_SM']
        rdecay = self['rGam_H_WW_2l2v']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ttH_H_tata_8')
    def mu_ttH_H_tata_8(self):
        rprod = self['dsigma_ttH_8']/self['sigma_ttH_8_SM']
        rdecay = self['rGam_H_tata']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ttH_H_bb_8')
    def mu_ttH_H_ZZ_8(self):
        rprod = self['dsigma_ttH_8']/self['sigma_ttH_8_SM']
        rdecay = self['rGam_H_bb']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_H_mumu_8')
    def mu_H_mumu_8(self):
        rprod = self['dsigma_8']/self['sigma_8_SM']
        rdecay = self['rGam_H_mumu']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_H_Za_8')
    def mu_H_Za_8(self):
        rprod = self['dsigma_8']/self['sigma_8_SM']
        rZdecay = self['dGam_Z_ll']/self['Gam_Z_ll_SM']
        rZwidth = self['dGam_Z']/self['Gam_Z_SM']
        rHdecay = self['rGam_H_Za']
        rHwidth = self['rGam_H']
        return 1. + rprod + rHdecay - rHwidth + rZdecay - rZwidth

class Run2(SM_HiggsProduction, SMEFT_HiggsProduction, SMEFT_HiggsDecay, SMEFT_Deltas):
    '''
    SMEFT Warsaw basis LHC Run 2 Higgs SignalStrengths, based on "SMEFT_global_fit_warsaw.nb".
    Takes Higgs production and decay fromm SMEFT_HiggsProduction & SMEFT_HiggsDecay.
    All linear coefficients validated to per-mille or better.
    '''
    @prediction('mu_ttH_H_bb_13')
    def mu_ttH_H_bb_13(self):
        rprod = self['dsigma_ttH_13']/self['sigma_ttH_13_SM']
        rdecay = self['rGam_H_bb']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF_H_bb_13')
    def mu_ggF_H_bb_13(self):
        rprod = self['dsigma_ggF_13']/self['sigma_ggF_13_SM']
        rdecay = self['rGam_H_bb']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ZH_H_bb_13')
    def mu_ZH_H_bb_13(self):
        rprod = self['dsigma_ZH_13']/self['sigma_ZH_13_SM']
        rdecay = self['rGam_H_bb']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_WH_H_tata_13')
    def mu_WH_H_tata_13(self):
        rprod = self['dsigma_WH_13']/self['sigma_WH_13_SM']
        rdecay = self['rGam_H_tata']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ZH_H_tata_13')
    def mu_ZH_H_tata_13(self):
        rprod = self['dsigma_ZH_13']/self['sigma_ZH_13_SM']
        rdecay = self['rGam_H_tata']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ttH_H_tata_13')
    def mu_ttH_H_tata_13(self):
        rprod = self['dsigma_ttH_13']/self['sigma_ttH_13_SM']
        rdecay = self['rGam_H_tata']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_WH_H_bb_13')
    def mu_WH_H_bb_13(self):
        rprod = self['dsigma_WH_13']/self['sigma_WH_13_SM']
        rdecay = self['rGam_H_bb']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ttH_H_1l+2ta-h_CMS_Run2')
    def mu_ttH_H_1l2tah_CMS_Run2(self):
        rprod = self['dsigma_ttH_13']/self['sigma_ttH_13_SM']
        r_tata = self['rGam_H_tata']
        r_Wlvl = self['rGam_H_WW_2l2v']
        r_Zll = self['rGam_H_4l']
        rdecay = 0.969*r_tata + 0.024*r_Wlvl+ 0.007*r_Zll
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ttH_H_2lss+1ta-h_CMS_Run2')
    def mu_ttH_H_2lss1tah_CMS_Run2(self):
        rprod = self['dsigma_ttH_13']/self['sigma_ttH_13_SM']
        r_tata = self['rGam_H_tata']
        r_Wlvl = self['rGam_H_WW_2l2v']
        r_Zll = self['rGam_H_4l']
        rdecay = 0.567*r_tata + 0.417*r_Wlvl+ 0.016*r_Zll
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ttH_H_3l+1ta-h_CMS_Run2')
    def mu_ttH_H_3l1tah_CMS_Run2(self):
        rprod = self['dsigma_ttH_13']/self['sigma_ttH_13_SM']
        r_tata = self['rGam_H_tata']
        r_Wlvl = self['rGam_H_WW_2l2v']
        r_Zll = self['rGam_H_4l']
        rdecay = 0.584*r_tata + 0.364*r_Wlvl + 0.052*r_Zll
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF_H_Wlvl_13')
    def mu_ggF_H_Wlvl_13(self):
        rprod = self['dsigma_ggF_13']/self['sigma_ggF_13_SM']
        rdecay = self['rGam_H_WW_2l2v']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_1j-DF_H_Wlvl_CMS_Run2')
    def mu_1jDF_H_Wlvl_CMS_Run2(self):
        rprod = self['dsigma_ggjH_13']/self['sigma_ggjH_13_SM']
        rdecay = self['rGam_H_WW_2l2v']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_2j-DF_H_Wlvl_CMS_Run2')
    def mu_2jDF_H_Wlvl_CMS_Run2(self):
        rprod = self['dsigma_ggjjH_13']/self['sigma_ggjjH_13_SM']
        rdecay = self['rGam_H_WW_2l2v']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_VBF_H_Wlvl_13')
    def mu_VBF_H_Wlvl_13(self):
        rprod = self['dsigma_VBF_13']/self['sigma_VBF_13_SM']
        rdecay = self['rGam_H_WW_2l2v']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_VH_H_Wlvl_13')
    def mu_VH_H_Wlvl_13(self):
        rprod = (
        (self['dsigma_WH_13']+self['dsigma_ZH_13'])/
        (self['sigma_WH_13_SM']+self['sigma_ZH_13_SM'])
        )
        rdecay = self['rGam_H_WW_2l2v']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_WH_H_WW_3-lep_13')
    def mu_WH_H_WW_3lep_13(self):
        rprod = self['dsigma_WH_13']/self['sigma_WH_13_SM']
        rdecay = self['rGam_H_WW_2l2v']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ZH_H_WW_4-lep_13')
    def mu_ZH_H_WW_4lep_13(self):
        rprod = self['dsigma_ZH_13']/self['sigma_ZH_13_SM']
        rdecay = self['rGam_H_WW_2l2v']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ttH_H_2lss_CMS_Run2')
    def mu_ttH_H_2lss_CMS_Run2(self):
        rprod = self['dsigma_ttH_13']/self['sigma_ttH_13_SM']
        r_tata = self['rGam_H_tata']
        r_Wlvl = self['rGam_H_WW_2l2v']
        r_Zll = self['rGam_H_4l']
        rdecay = 0.175*r_tata + 0.794*r_Wlvl+ 0.031*r_Zll
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ttH_H_3l_CMS_Run2')
    def mu_ttH_H_3l_CMS_Run2(self):
        rprod = self['dsigma_ttH_13']/self['sigma_ttH_13_SM']
        r_tata = self['rGam_H_tata']
        r_Wlvl = self['rGam_H_WW_2l2v']
        r_Zll = self['rGam_H_4l']
        rdecay = 0.204*r_tata + 0.755*r_Wlvl+ 0.041*r_Zll
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ttH_H_4l_CMS_Run2')
    def mu_ttH_H_4l_CMS_Run2(self):
        rprod = self['dsigma_ttH_13']/self['sigma_ttH_13_SM']
        r_tata = self['rGam_H_tata']
        r_Wlvl = self['rGam_H_WW_2l2v']
        r_Zll = self['rGam_H_4l']
        rdecay = 0.182*r_tata + 0.727*r_Wlvl+ 0.091*r_Zll
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF_H_tata_13')
    def mu_ggF_H_tata_13(self):
        rprod = self['dsigma_ggF_13']/self['sigma_ggF_13_SM']
        rdecay = self['rGam_H_tata']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_boosted_H_tata_CMS_Run2')
    def mu_boosted_H_tata_CMS_Run2(self):
        r_VBF = self['dsigma_VBF_13']/self['sigma_VBF_13_SM']
        r_VH = ( (self['dsigma_WH_13']+self['dsigma_ZH_13'])/
                 (self['sigma_WH_13_SM']+self['sigma_ZH_13_SM']) )
        r_ggjH = self['dsigma_ggjH_13']/self['sigma_ggjH_13_SM']
        rdecay = self['rGam_H_tata']
        rprod = 0.814*r_ggjH + 0.124*r_VBF + 0.062*r_VH
        rwidth = self['rGam_H']

        return 1. + rprod + rdecay - rwidth

    @prediction('mu_VBF_H_tata_13')
    def mu_VBF_H_tata_13(self):
        rprod = self['dsigma_VBF_13']/self['sigma_VBF_13_SM']
        rdecay = self['rGam_H_tata']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF_H_aa_13')
    def mu_ggF_H_aa_13(self):
        rprod = self['dsigma_ggF_13']/self['sigma_ggF_13_SM']
        rdecay = self['rGam_H_aa']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_VBF_H_aa_13')
    def mu_VBF_H_aa_13(self):
        rprod = self['dsigma_VBF_13']/self['sigma_VBF_13_SM']
        rdecay = self['rGam_H_aa']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ttH_H_aa_13')
    def mu_ttH_H_aa_13(self):
        rprod = self['dsigma_ttH_13']/self['sigma_ttH_13_SM']
        rdecay = self['rGam_H_aa']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_VH_H_aa_13')
    def mu_VH_H_aa_13(self):
        rprod = ( (self['dsigma_WH_13']+self['dsigma_ZH_13'])/
                  (self['sigma_WH_13_SM']+self['sigma_ZH_13_SM']) )
        rdecay = self['rGam_H_aa']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF_H_Zll_13')
    def mu_ggF_H_Zll_13(self):
        rprod = self['dsigma_ggF_13']/self['sigma_ggF_13_SM']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggjH_H_bb_CMS_Run2')
    def mu_ggjH_H_bb_CMS_Run2(self):
        rprod = self['dsigma_ggjH_13_ptjG450']/self['sigma_ggjH_13_ptjG450_SM']
        rdecay = self['rGam_H_bb']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_VBF_H_mumu_13')
    def mu_VBF_H_mumu_13(self):
        rprod = self['dsigma_VBF_13']/self['sigma_VBF_13_SM']
        rdecay = self['rGam_H_mumu']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF_H_mumu_13')
    def mu_ggF_H_mumu_13(self):
        rprod = self['dsigma_ggF_13']/self['sigma_ggF_13_SM']
        rdecay = self['rGam_H_mumu']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_pp_H_mumu_13')
    def mu_pp_H_mumu_13(self):
        rprod = self['dsigma_13']/self['sigma_13_SM']
        rdecay = self['rGam_H_mumu']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ttH_H_2lOS1ta-h_ATLAS_Run2')
    def mu_ttH_H_2lOS1tah_ATLAS_Run2(self):
        rprod = self['dsigma_ttH_13']/self['sigma_ttH_13_SM']
        r_tata = self['rGam_H_tata']
        r_Wlvl = self['rGam_H_WW_2l2v']
        r_Zll = self['rGam_H_4l']
        rdecay = 0.589187*r_tata + 0.394529*r_Wlvl + 0.0162837*r_Zll
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ttH_H_1l2ta-h_ATLAS_Run2')
    def mu_ttH_H_1l2tah_ATLAS_Run2(self):
        rprod = self['dsigma_ttH_13']/self['sigma_ttH_13_SM']
        r_tata = self['rGam_H_tata']
        r_Wlvl = self['rGam_H_WW_2l2v']
        r_Zll = self['rGam_H_4l']
        rdecay = 0.963931*r_tata + 0.0308955*r_Wlvl + 0.00517302*r_Zll
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ttH_H_3l1ta-h_ATLAS_Run2')
    def mu_ttH_H_3l1tah_ATLAS_Run2(self):
        rprod = self['dsigma_ttH_13']/self['sigma_ttH_13_SM']
        r_tata = self['rGam_H_tata']
        r_Wlvl = self['rGam_H_WW_2l2v']
        r_Zll = self['rGam_H_4l']
        rdecay = 0.604407*r_tata + 0.358641*r_Wlvl + 0.0369522*r_Zll
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ttH_H_2lSS1ta-h_ATLAS_Run2')
    def mu_ttH_H_2lSS1tah_ATLAS_Run2(self):
        rprod = self['dsigma_ttH_13']/self['sigma_ttH_13_SM']
        r_tata = self['rGam_H_tata']
        r_Wlvl = self['rGam_H_WW_2l2v']
        r_Zll = self['rGam_H_4l']
        rdecay = 0.586502*r_tata + 0.403935*r_Wlvl + 0.00956296*r_Zll
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ttH_H_3l_ATLAS_Run2')
    def mu_ttH_H_3l_ATLAS_Run2(self):
        rprod = self['dsigma_ttH_13']/self['sigma_ttH_13_SM']
        r_tata = self['rGam_H_tata']
        r_Wlvl = self['rGam_H_WW_2l2v']
        r_Zll = self['rGam_H_4l']
        rdecay = 0.135984*r_tata + 0.800033*r_Wlvl + 0.063983*r_Zll
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ttH_H_2lSS_ATLAS_Run2')
    def mu_ttH_H_2lSS_ATLAS_Run2(self):
        rprod = self['dsigma_ttH_13']/self['sigma_ttH_13_SM']
        r_tata = self['rGam_H_tata']
        r_Wlvl = self['rGam_H_WW_2l2v']
        r_Zll = self['rGam_H_4l']
        rdecay = 0.175621*r_tata + 0.804091*r_Wlvl + 0.0202879*r_Zll
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_H_aaOverZZ_ATLAS_Run2')
    def mu_H_aaOverZZ_ATLAS_Run2(self):
        r_aa = self['rGam_H_aa']
        r_ZZ = self['rGam_H_4l']
        return 1. + r_aa - r_ZZ

    @prediction('mu_ggjH_pt20T60_H_Zll_13')
    def mu_ggjH_13_pt20T60_H_Zll_13(self):
        rprod = self['dsigma_ggjH_13_pt20T60']/self['sigma_ggjH_13_pt20T60_SM']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggjH_pt60T120_H_Zll_13')
    def mu_ggjH_13_pt60T120_H_Zll_13(self):
        rprod = self['dsigma_ggjH_13_pt60T120']/self['sigma_ggjH_13_pt60T120_SM']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggjH_pt120T200_H_Zll_13')
    def mu_ggjH_pt120T200_H_Zll_13(self):
        rprod = self['dsigma_ggjH_13_pt120T200']/self['sigma_ggjH_13_pt120T200_SM']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggjjH_H_Zll_13')
    def mu_ggjjH_H_Zll_13(self):
        rprod = self['dsigma_ggjjH_13']/self['sigma_ggjjH_13_SM']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggjjH_ptG200_H_Zll_13')
    def mu_ggjjH_ptG200_H_Zll_13(self):
        rprod = (
          (self['dsigma_ggjH_13_ptG200'] + self['dsigma_VBF_13_ptjG200'])/
          (self['sigma_ggjH_13_ptG200_SM'] + self['sigma_VBF_13_ptjG200_SM'])
        )
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth


    @prediction('mu_VBF_ptjL200_H_Zll_13')
    def mu_VBF_ptjL200_H_Zll_13(self):
        rprod = self['dsigma_VBF_13_ptjL200']/self['sigma_VBF_13_ptjL200_SM']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_VH_H_Zll_13')
    def mu_VH_H_ZZ_ATLAS_Run2(self):
        rprod = ( (self['dsigma_WH_13']+self['dsigma_ZH_13'])/
                  (self['sigma_WH_13_SM']+self['sigma_ZH_13_SM']) )
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ttH_13TeV')
    def mu_ttH_13TeV(self):
        rprod = self['dsigma_ttH_13']/self['sigma_ttH_13_SM']
        return 1. + rprod

    @prediction('mu_ttH_H_Zll_13')
    def mu_ttH_H_ZZ_ATLAS_Run2(self):
        rprod = self['dsigma_ttH_13']/self['sigma_ttH_13_SM']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_WH_H_WW_13')
    def mu_WH_H_WW_13(self):
        rprod = self['dsigma_WH_13']/self['sigma_WH_13_SM']
        rdecay = self['rGam_H_WW_2l2v']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ZH_H_WW_13')
    def mu_ZH_H_WW_13(self):
        rprod = self['dsigma_ZH_13']/self['sigma_ZH_13_SM']
        rdecay = self['rGam_H_WW_2l2v']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_VBF_H_WW_13')
    def mu_VBF_H_WW_13(self):
        rprod = self['dsigma_VBF_13']/self['sigma_VBF_13_SM']
        rdecay = self['rGam_H_WW_2l2v']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_VBF_H_ZZ_13')
    def mu_VBF_H_ZZ_13(self):
        rprod = self['dsigma_VBF_13']/self['sigma_VBF_13_SM']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF_H_WW_13')
    def mu_ggF_H_WW_13(self):
        rprod = self['dsigma_ggF_13']/self['sigma_ggF_13_SM']
        rdecay = self['rGam_H_WW_2l2v']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF_H_ZZ_13')
    def mu_ggF_H_ZZ_13(self):
        rprod = self['dsigma_ggF_13']/self['sigma_ggF_13_SM']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ttH_H_WW_13')
    def mu_ttH_H_WW_13(self):
        rprod = self['dsigma_ttH_13']/self['sigma_ttH_13_SM']
        rdecay = self['rGam_H_WW_2l2v']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ttH_H_ZZ_13')
    def mu_ttH_H_ZZ_13(self):
        rprod = self['dsigma_ttH_13']/self['sigma_ttH_13_SM']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_VH_H_ZZ_13')
    def mu_VH_H_ZZ_13(self):
        rprod = ( (self['dsigma_WH_13']+self['dsigma_ZH_13'])/
                  (self['sigma_WH_13_SM']+self['sigma_ZH_13_SM']) )
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_H_Za_13')
    def mu_H_Za_13(self):
        '''
        Signal strength for Higgs -> Z a, taking into account Z branching
        fraction into leptons.
        '''
        rprod = self['dsigma_13']/self['sigma_13_SM']
        rZdecay = self['dGam_Z_ll']/self['Gam_Z_ll_SM']
        rZwidth = self['dGam_Z']/self['Gam_Z_SM']
        rHdecay = self['rGam_H_Za']
        rHwidth = self['rGam_H']
        return 1. + rprod + rHdecay - rHwidth + rZdecay - rZwidth

    @prediction('mu_VBF_H_bb_13')
    def mu_VBF_H_bb_13(self):
        rprod = self['dsigma_VBF_13']/self['sigma_VBF_13_SM']
        rdecay = self['rGam_H_bb']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_VH_H_bb_13')
    def mu_VH_H_bb_13(self):
        rprod = ( (self['dsigma_WH_13']+self['dsigma_ZH_13'])/
                  (self['sigma_WH_13_SM']+self['sigma_ZH_13_SM']) )
        rdecay = self['rGam_H_bb']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ttH_H_bb_13')
    def mu_ttH_H_bb_13(self):
        rprod = self['dsigma_ttH_13']/self['sigma_ttH_13_SM']
        rdecay = self['rGam_H_bb']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ttH_H_VV_13')
    def mu_ttH_H_VV_13(self):
        rprod = self['dsigma_ttH_13']/self['sigma_ttH_13_SM']
        r_Wlvl = self['rGam_H_WW_2l2v']
        r_Zll = self['rGam_H_4l']
        rdecay = (r_Wlvl*self['Gam_H_WW_2l2v_SM']  + r_Zll*self['Gam_H_4l_SM'])/(self['Gam_H_WW_2l2v_SM'] + self['Gam_H_4l_SM'])
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

class Run1_old(SM_HiggsProduction, SMEFT_HiggsProduction, SMEFT_HiggsDecay_old,  SM_HiggsDecay_old):
    '''
    SMEFT Warsaw basis LHC Run 1 Higgs SignalStrengths, based on "SMEFT_global_fit_warsaw.nb".
    Takes Higgs production and decay fromm SMEFT_HiggsProduction & SMEFT_HiggsDecay.
    All linear coefficients validated to one part in 1e-12 or better.
    '''
    # LHC Run 1 observable predictions
    @prediction('mu_ggF_H_aa_8')
    def mu_ggF_H_aa_8(self):
        rprod = self['dsigma_ggF_8']/self['sigma_ggF_8_SM']
        rdecay = self['dGam_H_aa']/self['Gam_H_aa_SM']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF_H_ZZ_8')
    def mu_ggF_H_ZZ_8(self):
        rprod = self['dsigma_ggF_8']/self['sigma_ggF_8_SM']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF_H_WW_8')
    def mu_ggF_H_WW_8(self):
        rprod = self['dsigma_ggF_8']/self['sigma_ggF_8_SM']
        rdecay = self['rGam_H_WW_2l2v']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF_H_tata_8')
    def mu_ggF_H_tata_8(self):
        rprod = self['dsigma_ggF_8']/self['sigma_ggF_8_SM']
        rdecay = self['dGam_H_tata']/self['Gam_H_tata_SM']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_VBF_H_aa_8')
    def mu_VBF_H_aa_8(self):
        rprod = self['dsigma_VBF_8']/self['sigma_VBF_8_SM']
        rdecay = self['dGam_H_aa']/self['Gam_H_aa_SM']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_VBF_H_ZZ_8')
    def mu_VBF_H_ZZ_8(self):
        rprod = self['dsigma_VBF_8']/self['sigma_VBF_8_SM']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_VBF_H_WW_8')
    def mu_VBF_H_WW_8(self):
        rprod = self['dsigma_VBF_8']/self['sigma_VBF_8_SM']
        rdecay = self['rGam_H_WW_2l2v']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_VBF_H_tata_8')
    def mu_VBF_H_tata_8(self):
        rprod = self['dsigma_VBF_8']/self['sigma_VBF_8_SM']
        rdecay = self['dGam_H_tata']/self['Gam_H_tata_SM']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_WH_H_aa_8')
    def mu_WH_H_aa_8(self):
        rprod = self['dsigma_WH_8']/self['sigma_WH_8_SM']
        rdecay = self['dGam_H_aa']/self['Gam_H_aa_SM']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_WH_H_WW_8')
    def mu_WH_H_WW_8(self):
        rprod = self['dsigma_WH_8']/self['sigma_WH_8_SM']
        rdecay = self['rGam_H_WW_2l2v']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_WH_H_tata_8')
    def mu_WH_H_tata_8(self):
        rprod = self['dsigma_WH_8']/self['sigma_WH_8_SM']
        rdecay = self['dGam_H_tata']/self['Gam_H_tata_SM']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_WH_H_bb_8')
    def mu_WH_H_ZZ_8(self):
        rprod = self['dsigma_WH_8']/self['sigma_WH_8_SM']
        rdecay = self['dGam_H_bb']/self['Gam_H_bb_SM']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ZH_H_aa_8')
    def mu_ZH_H_aa_8(self):
        rprod = self['dsigma_ZH_8']/self['sigma_ZH_8_SM']
        rdecay = self['dGam_H_aa']/self['Gam_H_aa_SM']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ZH_H_WW_8')
    def mu_ZH_H_WW_8(self):
        rprod = self['dsigma_ZH_8']/self['sigma_ZH_8_SM']
        rdecay = self['rGam_H_WW_2l2v']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ZH_H_tata_8')
    def mu_ZH_H_tata_8(self):
        rprod = self['dsigma_ZH_8']/self['sigma_ZH_8_SM']
        rdecay = self['dGam_H_tata']/self['Gam_H_tata_SM']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ZH_H_bb_8')
    def mu_ZH_H_ZZ_8(self):
        rprod = self['dsigma_ZH_8']/self['sigma_ZH_8_SM']
        rdecay = self['dGam_H_bb']/self['Gam_H_bb_SM']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ttH_H_aa_8')
    def mu_ttH_H_aa_8(self):
        rprod = self['dsigma_ttH_8']/self['sigma_ttH_8_SM']
        rdecay = self['dGam_H_aa']/self['Gam_H_aa_SM']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ttH_H_WW_8')
    def mu_ttH_H_WW_8(self):
        rprod = self['dsigma_ttH_8']/self['sigma_ttH_8_SM']
        rdecay = self['rGam_H_WW_2l2v']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ttH_H_tata_8')
    def mu_ttH_H_tata_8(self):
        rprod = self['dsigma_ttH_8']/self['sigma_ttH_8_SM']
        rdecay = self['dGam_H_tata']/self['Gam_H_tata_SM']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ttH_H_bb_8')
    def mu_ttH_H_ZZ_8(self):
        rprod = self['dsigma_ttH_8']/self['sigma_ttH_8_SM']
        rdecay = self['dGam_H_bb']/self['Gam_H_bb_SM']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_H_mumu_8')
    def mu_H_mumu_8(self):
        rprod = self['dsigma_8']/self['sigma_8_SM']
        rdecay = self['dGam_H_mumu']/self['Gam_H_mumu_SM']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_H_Za_8')
    def mu_H_Za_8(self):
        rprod = self['dsigma_8']/self['sigma_8_SM']
        rZdecay = self['dGam_Z_ll']/self['Gam_Z_ll_SM']
        rZwidth = self['dGam_Z']/self['Gam_Z_SM']
        rHdecay = self['dGam_H_Za']/self['Gam_H_Za_SM']
        rHwidth = self['rGam_H']
        return 1. + rprod + rHdecay - rHwidth + rZdecay - rZwidth

class Run2_old(SM_HiggsProduction, SMEFT_HiggsProduction,  SMEFT_HiggsDecay_old,  SM_HiggsDecay_old):
    '''
    SMEFT Warsaw basis LHC Run 2 Higgs SignalStrengths, based on "SMEFT_global_fit_warsaw.nb".
    Takes Higgs production and decay fromm SMEFT_HiggsProduction & SMEFT_HiggsDecay.
    All linear coefficients validated to per-mille or better.
    '''
    @prediction('mu_ttH_H_bb_13')
    def mu_ttH_H_bb_13(self):
        rprod = self['dsigma_ttH_13']/self['sigma_ttH_13_SM']
        rdecay = self['dGam_H_bb']/self['Gam_H_bb_SM']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF_H_bb_13')
    def mu_ggF_H_bb_13(self):
        rprod = self['dsigma_ggF_13']/self['sigma_ggF_13_SM']
        rdecay = self['dGam_H_bb']/self['Gam_H_bb_SM']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ZH_H_bb_13')
    def mu_ZH_H_bb_13(self):
        rprod = self['dsigma_ZH_13']/self['sigma_ZH_13_SM']
        rdecay = self['dGam_H_bb']/self['Gam_H_bb_SM']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_WH_H_tata_13')
    def mu_WH_H_tata_13(self):
        rprod = self['dsigma_WH_13']/self['sigma_WH_13_SM']
        rdecay = self['dGam_H_tata']/self['Gam_H_tata_SM']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ZH_H_tata_13')
    def mu_ZH_H_tata_13(self):
        rprod = self['dsigma_ZH_13']/self['sigma_ZH_13_SM']
        rdecay = self['dGam_H_tata']/self['Gam_H_tata_SM']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ttH_H_tata_13')
    def mu_ttH_H_tata_13(self):
        rprod = self['dsigma_ttH_13']/self['sigma_ttH_13_SM']
        rdecay = self['dGam_H_tata']/self['Gam_H_tata_SM']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_WH_H_bb_13')
    def mu_WH_H_bb_13(self):
        rprod = self['dsigma_WH_13']/self['sigma_WH_13_SM']
        rdecay = self['dGam_H_bb']/self['Gam_H_bb_SM']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ttH_H_1l+2ta-h_CMS_Run2')
    def mu_ttH_H_1l2tah_CMS_Run2(self):
        rprod = self['dsigma_ttH_13']/self['sigma_ttH_13_SM']
        r_tata = self['dGam_H_tata']/self['Gam_H_tata_SM']
        r_Wlvl = self['rGam_H_WW_2l2v']
        r_Zll = self['rGam_H_4l']
        rdecay = 0.969*r_tata + 0.024*r_Wlvl+ 0.007*r_Zll
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ttH_H_2lss+1ta-h_CMS_Run2')
    def mu_ttH_H_2lss1tah_CMS_Run2(self):
        rprod = self['dsigma_ttH_13']/self['sigma_ttH_13_SM']
        r_tata = self['dGam_H_tata']/self['Gam_H_tata_SM']
        r_Wlvl = self['rGam_H_WW_2l2v']
        r_Zll = self['rGam_H_4l']
        rdecay = 0.567*r_tata + 0.417*r_Wlvl+ 0.016*r_Zll
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ttH_H_3l+1ta-h_CMS_Run2')
    def mu_ttH_H_3l1tah_CMS_Run2(self):
        rprod = self['dsigma_ttH_13']/self['sigma_ttH_13_SM']
        r_tata = self['dGam_H_tata']/self['Gam_H_tata_SM']
        r_Wlvl = self['rGam_H_WW_2l2v']
        r_Zll = self['rGam_H_4l']
        rdecay = 0.584*r_tata + 0.364*r_Wlvl + 0.052*r_Zll
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF_H_Wlvl_13')
    def mu_ggF_H_Wlvl_13(self):
        rprod = self['dsigma_ggF_13']/self['sigma_ggF_13_SM']
        rdecay = self['rGam_H_WW_2l2v']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_1j-DF_H_Wlvl_CMS_Run2')
    def mu_1jDF_H_Wlvl_CMS_Run2(self):
        rprod = self['dsigma_ggjH_13']/self['sigma_ggjH_13_SM']
        rdecay = self['rGam_H_WW_2l2v']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_2j-DF_H_Wlvl_CMS_Run2')
    def mu_2jDF_H_Wlvl_CMS_Run2(self):
        rprod = self['dsigma_ggjjH_13']/self['sigma_ggjjH_13_SM']
        rdecay = self['rGam_H_WW_2l2v']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_VBF_H_Wlvl_13')
    def mu_VBF_H_Wlvl_13(self):
        rprod = self['dsigma_VBF_13']/self['sigma_VBF_13_SM']
        rdecay = self['rGam_H_WW_2l2v']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_VH_H_Wlvl_13')
    def mu_VH_H_Wlvl_13(self):
        rprod = (
        (self['dsigma_WH_13']+self['dsigma_ZH_13'])/
        (self['sigma_WH_13_SM']+self['sigma_ZH_13_SM'])
        )
        rdecay = self['rGam_H_WW_2l2v']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_WH_H_WW_3-lep_13')
    def mu_WH_H_WW_3lep_13(self):
        rprod = self['dsigma_WH_13']/self['sigma_WH_13_SM']
        rdecay = self['rGam_H_WW_2l2v']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ZH_H_WW_4-lep_13')
    def mu_ZH_H_WW_4lep_13(self):
        rprod = self['dsigma_ZH_13']/self['sigma_ZH_13_SM']
        rdecay = self['rGam_H_WW_2l2v']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ttH_H_2lss_CMS_Run2')
    def mu_ttH_H_2lss_CMS_Run2(self):
        rprod = self['dsigma_ttH_13']/self['sigma_ttH_13_SM']
        r_tata = self['dGam_H_tata']/self['Gam_H_tata_SM']
        r_Wlvl = self['rGam_H_WW_2l2v']
        r_Zll = self['rGam_H_4l']
        rdecay = 0.175*r_tata + 0.794*r_Wlvl+ 0.031*r_Zll
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ttH_H_3l_CMS_Run2')
    def mu_ttH_H_3l_CMS_Run2(self):
        rprod = self['dsigma_ttH_13']/self['sigma_ttH_13_SM']
        r_tata = self['dGam_H_tata']/self['Gam_H_tata_SM']
        r_Wlvl = self['rGam_H_WW_2l2v']
        r_Zll = self['rGam_H_4l']
        rdecay = 0.204*r_tata + 0.755*r_Wlvl+ 0.041*r_Zll
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ttH_H_4l_CMS_Run2')
    def mu_ttH_H_4l_CMS_Run2(self):
        rprod = self['dsigma_ttH_13']/self['sigma_ttH_13_SM']
        r_tata = self['dGam_H_tata']/self['Gam_H_tata_SM']
        r_Wlvl = self['rGam_H_WW_2l2v']
        r_Zll = self['rGam_H_4l']
        rdecay = 0.182*r_tata + 0.727*r_Wlvl+ 0.091*r_Zll
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF_H_tata_13')
    def mu_ggF_H_tata_13(self):
        rprod = self['dsigma_ggF_13']/self['sigma_ggF_13_SM']
        rdecay = self['dGam_H_tata']/self['Gam_H_tata_SM']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_boosted_H_tata_CMS_Run2')
    def mu_boosted_H_tata_CMS_Run2(self):
        r_VBF = self['dsigma_VBF_13']/self['sigma_VBF_13_SM']
        r_VH = ( (self['dsigma_WH_13']+self['dsigma_ZH_13'])/
                 (self['sigma_WH_13_SM']+self['sigma_ZH_13_SM']) )
        r_ggjH = self['dsigma_ggjH_13']/self['sigma_ggjH_13_SM']
        rdecay = self['dGam_H_tata']/self['Gam_H_tata_SM']
        rprod = 0.814*r_ggjH + 0.124*r_VBF + 0.062*r_VH
        rwidth = self['rGam_H']

        return 1. + rprod + rdecay - rwidth

    @prediction('mu_VBF_H_tata_13')
    def mu_VBF_H_tata_13(self):
        rprod = self['dsigma_VBF_13']/self['sigma_VBF_13_SM']
        rdecay = self['dGam_H_tata']/self['Gam_H_tata_SM']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF_H_aa_13')
    def mu_ggF_H_aa_13(self):
        rprod = self['dsigma_ggF_13']/self['sigma_ggF_13_SM']
        rdecay = self['dGam_H_aa']/self['Gam_H_aa_SM']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_VBF_H_aa_13')
    def mu_VBF_H_aa_13(self):
        rprod = self['dsigma_VBF_13']/self['sigma_VBF_13_SM']
        rdecay = self['dGam_H_aa']/self['Gam_H_aa_SM']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ttH_H_aa_13')
    def mu_ttH_H_aa_13(self):
        rprod = self['dsigma_ttH_13']/self['sigma_ttH_13_SM']
        rdecay = self['dGam_H_aa']/self['Gam_H_aa_SM']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_VH_H_aa_13')
    def mu_VH_H_aa_13(self):
        rprod = ( (self['dsigma_WH_13']+self['dsigma_ZH_13'])/
                  (self['sigma_WH_13_SM']+self['sigma_ZH_13_SM']) )
        rdecay = self['dGam_H_aa']/self['Gam_H_aa_SM']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF_H_Zll_13')
    def mu_ggF_H_Zll_13(self):
        rprod = self['dsigma_ggF_13']/self['sigma_ggF_13_SM']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggjH_H_bb_CMS_Run2')
    def mu_ggjH_H_bb_CMS_Run2(self):
        rprod = self['dsigma_ggjH_13_ptjG450']/self['sigma_ggjH_13_ptjG450_SM']
        rdecay = self['dGam_H_bb']/self['Gam_H_bb_SM']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_VBF_H_mumu_13')
    def mu_VBF_H_mumu_13(self):
        rprod = self['dsigma_VBF_13']/self['sigma_VBF_13_SM']
        rdecay = self['dGam_H_mumu']/self['Gam_H_mumu_SM']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF_H_mumu_13')
    def mu_ggF_H_mumu_13(self):
        rprod = self['dsigma_ggF_13']/self['sigma_ggF_13_SM']
        rdecay = self['dGam_H_mumu']/self['Gam_H_mumu_SM']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_pp_H_mumu_13')
    def mu_pp_H_mumu_13(self):
        rprod = self['dsigma_13']/self['sigma_13_SM']
        rdecay = self['dGam_H_mumu']/self['Gam_H_mumu_SM']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ttH_H_2lOS1ta-h_ATLAS_Run2')
    def mu_ttH_H_2lOS1tah_ATLAS_Run2(self):
        rprod = self['dsigma_ttH_13']/self['sigma_ttH_13_SM']
        r_tata = self['dGam_H_tata']/self['Gam_H_tata_SM']
        r_Wlvl = self['rGam_H_WW_2l2v']
        r_Zll = self['rGam_H_4l']
        rdecay = 0.589187*r_tata + 0.394529*r_Wlvl + 0.0162837*r_Zll
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ttH_H_1l2ta-h_ATLAS_Run2')
    def mu_ttH_H_1l2tah_ATLAS_Run2(self):
        rprod = self['dsigma_ttH_13']/self['sigma_ttH_13_SM']
        r_tata = self['dGam_H_tata']/self['Gam_H_tata_SM']
        r_Wlvl = self['rGam_H_WW_2l2v']
        r_Zll = self['rGam_H_4l']
        rdecay = 0.963931*r_tata + 0.0308955*r_Wlvl + 0.00517302*r_Zll
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ttH_H_3l1ta-h_ATLAS_Run2')
    def mu_ttH_H_3l1tah_ATLAS_Run2(self):
        rprod = self['dsigma_ttH_13']/self['sigma_ttH_13_SM']
        r_tata = self['dGam_H_tata']/self['Gam_H_tata_SM']
        r_Wlvl = self['rGam_H_WW_2l2v']
        r_Zll = self['rGam_H_4l']
        rdecay = 0.604407*r_tata + 0.358641*r_Wlvl + 0.0369522*r_Zll
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ttH_H_2lSS1ta-h_ATLAS_Run2')
    def mu_ttH_H_2lSS1tah_ATLAS_Run2(self):
        rprod = self['dsigma_ttH_13']/self['sigma_ttH_13_SM']
        r_tata = self['dGam_H_tata']/self['Gam_H_tata_SM']
        r_Wlvl = self['rGam_H_WW_2l2v']
        r_Zll = self['rGam_H_4l']
        rdecay = 0.586502*r_tata + 0.403935*r_Wlvl + 0.00956296*r_Zll
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ttH_H_3l_ATLAS_Run2')
    def mu_ttH_H_3l_ATLAS_Run2(self):
        rprod = self['dsigma_ttH_13']/self['sigma_ttH_13_SM']
        r_tata = self['dGam_H_tata']/self['Gam_H_tata_SM']
        r_Wlvl = self['rGam_H_WW_2l2v']
        r_Zll = self['rGam_H_4l']
        rdecay = 0.135984*r_tata + 0.800033*r_Wlvl + 0.063983*r_Zll
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ttH_H_2lSS_ATLAS_Run2')
    def mu_ttH_H_2lSS_ATLAS_Run2(self):
        rprod = self['dsigma_ttH_13']/self['sigma_ttH_13_SM']
        r_tata = self['dGam_H_tata']/self['Gam_H_tata_SM']
        r_Wlvl = self['rGam_H_WW_2l2v']
        r_Zll = self['rGam_H_4l']
        rdecay = 0.175621*r_tata + 0.804091*r_Wlvl + 0.0202879*r_Zll
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_H_aaOverZZ_ATLAS_Run2')
    def mu_H_aaOverZZ_ATLAS_Run2(self):
        r_aa = self['dGam_H_aa']/self['Gam_H_aa_SM']
        r_ZZ = self['rGam_H_4l']
        return 1. + r_aa - r_ZZ

    @prediction('mu_ggjH_pt20T60_H_Zll_13')
    def mu_ggjH_13_pt20T60_H_Zll_13(self):
        rprod = self['dsigma_ggjH_13_pt20T60']/self['sigma_ggjH_13_pt20T60_SM']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggjH_pt60T120_H_Zll_13')
    def mu_ggjH_13_pt60T120_H_Zll_13(self):
        rprod = self['dsigma_ggjH_13_pt60T120']/self['sigma_ggjH_13_pt60T120_SM']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggjH_pt120T200_H_Zll_13')
    def mu_ggjH_pt120T200_H_Zll_13(self):
        rprod = self['dsigma_ggjH_13_pt120T200']/self['sigma_ggjH_13_pt120T200_SM']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggjjH_H_Zll_13')
    def mu_ggjjH_H_Zll_13(self):
        rprod = self['dsigma_ggjjH_13']/self['sigma_ggjjH_13_SM']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggjjH_ptG200_H_Zll_13')
    def mu_ggjjH_ptG200_H_Zll_13(self):
        rprod = (
          (self['dsigma_ggjH_13_ptG200'] + self['dsigma_VBF_13_ptjG200'])/
          (self['sigma_ggjH_13_ptG200_SM'] + self['sigma_VBF_13_ptjG200_SM'])
        )
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth


    @prediction('mu_VBF_ptjL200_H_Zll_13')
    def mu_VBF_ptjL200_H_Zll_13(self):
        rprod = self['dsigma_VBF_13_ptjL200']/self['sigma_VBF_13_ptjL200_SM']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_VH_H_Zll_13')
    def mu_VH_H_ZZ_ATLAS_Run2(self):
        rprod = ( (self['dsigma_WH_13']+self['dsigma_ZH_13'])/
                  (self['sigma_WH_13_SM']+self['sigma_ZH_13_SM']) )
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ttH_H_Zll_13')
    def mu_ttH_H_ZZ_ATLAS_Run2(self):
        rprod = self['dsigma_ttH_13']/self['sigma_ttH_13_SM']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_WH_H_WW_13')
    def mu_WH_H_WW_13(self):
        rprod = self['dsigma_WH_13']/self['sigma_WH_13_SM']
        rdecay = self['rGam_H_WW_2l2v']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ZH_H_WW_13')
    def mu_ZH_H_WW_13(self):
        rprod = self['dsigma_ZH_13']/self['sigma_ZH_13_SM']
        rdecay = self['rGam_H_WW_2l2v']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_VBF_H_WW_13')
    def mu_VBF_H_WW_13(self):
        rprod = self['dsigma_VBF_13']/self['sigma_VBF_13_SM']
        rdecay = self['rGam_H_WW_2l2v']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_VBF_H_ZZ_13')
    def mu_VBF_H_ZZ_13(self):
        rprod = self['dsigma_VBF_13']/self['sigma_VBF_13_SM']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF_H_WW_13')
    def mu_ggF_H_WW_13(self):
        rprod = self['dsigma_ggF_13']/self['sigma_ggF_13_SM']
        rdecay = self['rGam_H_WW_2l2v']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ggF_H_ZZ_13')
    def mu_ggF_H_ZZ_13(self):
        rprod = self['dsigma_ggF_13']/self['sigma_ggF_13_SM']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ttH_H_WW_13')
    def mu_ttH_H_WW_13(self):
        rprod = self['dsigma_ttH_13']/self['sigma_ttH_13_SM']
        rdecay = self['rGam_H_WW_2l2v']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_ttH_H_ZZ_13')
    def mu_ttH_H_ZZ_13(self):
        rprod = self['dsigma_ttH_13']/self['sigma_ttH_13_SM']
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_VH_H_ZZ_13')
    def mu_VH_H_ZZ_13(self):
        rprod = ( (self['dsigma_WH_13']+self['dsigma_ZH_13'])/
                  (self['sigma_WH_13_SM']+self['sigma_ZH_13_SM']) )
        rdecay = self['rGam_H_4l']
        rwidth = self['rGam_H']
        return 1. + rprod + rdecay - rwidth

    @prediction('mu_H_Za_13')
    def mu_H_Za_13(self):
        rprod = self['dsigma_13']/self['sigma_13_SM']
        rZdecay = self['dGam_Z_ll']/self['Gam_Z_ll_SM']
        rZwidth = self['dGam_Z']/self['Gam_Z_SM']
        rHdecay = self['dGam_H_Za']/self['Gam_H_Za_SM']
        rHwidth = self['rGam_H']
        return 1. + rprod + rHdecay - rHwidth + rZdecay - rZwidth
