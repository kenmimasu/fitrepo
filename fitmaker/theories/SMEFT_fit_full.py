from ..fitlib.theory import internal_parameter
from ..fitlib.constants import pi, GeV2pb

from .SMEFT_aEW_MZ_GF_5F_U2_2_U3_3_linear.LEP import(
  SMEFT_EWPO as EWPO,
  SMEFT_Diboson_LEP as Diboson_LEP
)

from .SMEFT_aEW_MZ_GF_5F_U2_2_U3_3_linear.diboson import LHC as Diboson_LHC

from .SMEFT_aEW_MZ_GF_5F_U2_2_U3_3_linear.higgs.signal_strengths import(
  # Using decays from 1906.06949
  Run1 as Higgs_Run1_new,
  Run2 as Higgs_Run2_new,
  # Using on-shell approximated decays from SMEFTsim
  Run1_old as Higgs_Run1,
  Run2_old as Higgs_Run2
)

# Using on-shell approximated decays from SMEFTsim
from .SMEFT_aEW_MZ_GF_5F_U2_2_U3_3_linear.papers import SMEFT_HiggsSignalStrengths_Run2_CMS_H_aa as CMSSTXS_aa
from .SMEFT_aEW_MZ_GF_5F_U2_2_U3_3_linear.papers import SMEFT_HiggsSignalStrengths_Run2_CMS_H_tata as CMSSTXS_tata
from .SMEFT_aEW_MZ_GF_5F_U2_2_U3_3_linear.papers import SMEFT_HiggsSignalStrengths_Run2_CMS_H_Zll as CMSSTXS_Zll
from .SMEFT_aEW_MZ_GF_5F_U2_2_U3_3_linear.papers import SMEFT_HiggsSignalStrengths_Run2_ATLAScombination as ATLASSTXS

# Using decays from 1906.06949
from .SMEFT_aEW_MZ_GF_5F_U2_2_U3_3_linear.papers import SMEFT_HiggsSignalStrengths_Run2_CMS_H_aa_new as CMSSTXS_aa_new
from .SMEFT_aEW_MZ_GF_5F_U2_2_U3_3_linear.papers import SMEFT_HiggsSignalStrengths_Run2_CMS_H_tata_new as CMSSTXS_tata_new
from .SMEFT_aEW_MZ_GF_5F_U2_2_U3_3_linear.papers import SMEFT_HiggsSignalStrengths_Run2_CMS_H_Zll_new as CMSSTXS_Zll_new
from .SMEFT_aEW_MZ_GF_5F_U2_2_U3_3_linear.papers import SMEFT_HiggsSignalStrengths_Run2_ATLAScombination_new as ATLASSTXS_new

# top
from .SMEFT_aEW_MZ_GF_5F_U2_2_U3_3_linear.top import top_LHC_Tevatron

#
class SMEFT_old(EWPO, Diboson_LEP, Diboson_LHC, Higgs_Run1, Higgs_Run2, top_LHC_Tevatron, ATLASSTXS, CMSSTXS_tata, CMSSTXS_aa, CMSSTXS_Zll):
    ##### 4-heavy 4F operators
    @internal_parameter()
    def Ctb8(self):
        return 0.

    @internal_parameter()
    def CQQ8(self):
        return 0.

    @internal_parameter()
    def CQt8(self):
        return 0.

    @internal_parameter()
    def CQb8(self):
        return 0.

    @internal_parameter()
    def CQQ1(self):
        return 0.

    @internal_parameter()
    def CQt1(self):
        return 0.

    @internal_parameter()
    def CQb1(self):
        return 0.

    @internal_parameter()
    def CQb8(self):
        return 0.

    @internal_parameter()
    def Ctt(self):
        return 0.

    @internal_parameter()
    def Ctb8(self):
        return 0.

class SMEFT(EWPO, Diboson_LEP, Diboson_LHC, Higgs_Run1_new, Higgs_Run2_new, top_LHC_Tevatron, ATLASSTXS_new, CMSSTXS_tata_new, CMSSTXS_aa_new, CMSSTXS_Zll_new):
    ##### 4-heavy 4F operators
    @internal_parameter()
    def Ctb8(self):
        return 0.

    @internal_parameter()
    def CQQ8(self):
        return 0.

    @internal_parameter()
    def CQt8(self):
        return 0.

    @internal_parameter()
    def CQb8(self):
        return 0.

    @internal_parameter()
    def CQQ1(self):
        return 0.

    @internal_parameter()
    def CQt1(self):
        return 0.

    @internal_parameter()
    def CQb1(self):
        return 0.

    @internal_parameter()
    def CQb8(self):
        return 0.

    @internal_parameter()
    def Ctt(self):
        return 0.

    @internal_parameter()
    def Ctb8(self):
        return 0.
