from .SMEFT_aEW_MZ_GF_5F_U2_2_U3_3_linear.LEP import SMEFT_EWPO, SMEFT_Diboson_LEP

class LEP(SMEFT_EWPO, SMEFT_Diboson_LEP):
    '''
    Subset of SMEFT_EWPO with only the S & T parameters mapped to the CHWB & CHD 
    operators.
    '''
    external_parameters = ['S', 'T']
    
    @internal_parameter('S')
    def CHWB(self):
        return self['gp']*self['gw']/(16.*pi)*self['S']

    @internal_parameter('T')
    def CHD(self):
        # return -self['gp']*self['gw']/(self['gp']+self['gw'])/(2.*pi)*self['T']
        return - (self['gp']*self['gw'])**2/(self['gp']**2+self['gw']**2)/(2.*pi)*self['T']
    
    @internal_parameter()
    def Cll(self):
        return 0. 

    @internal_parameter()
    def CHl3(self):
        return 0.
    
    @internal_parameter()
    def CHl1(self):
        return 0.
    
    @internal_parameter()
    def CHe(self):
        return 0.
    
    @internal_parameter()
    def CHq3(self):
        return 0.
    
    @internal_parameter()
    def CHq1(self):
        return 0.
    
    @internal_parameter()
    def CHu(self):
        return 0.
    
    @internal_parameter()
    def CHd(self):
        return 0.
    
    @internal_parameter()
    def CW(self):
        return 0.
