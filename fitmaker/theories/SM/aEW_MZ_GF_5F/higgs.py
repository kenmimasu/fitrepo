from ....fitlib.theory import internal_parameter, prediction
from .inputs import aEW_MZ_GF_5F_Fixed
from .EWPO import Tree_Fixed as EWPO_Tree
# TODO: recompute ggF stuff in SM to get correct mu's for old measurements

class Production(aEW_MZ_GF_5F_Fixed):
    '''
    LO predictions for Higgs production cross sections at the LHC.
    Computed with SMEFTsim_A_U35_alphaScheme_UFO_v2_1 with input parameters
    fixed to match aEW_MZ_GF_5F_Fixed.
    '''
    # LO LHC Higgs production cross sections in pb
    @internal_parameter()
    def sigma_ggF_13_SM(self):
        '''SM Gluon gluon fusion Higgs production cross section @ 13 TeV in pb'''
        return 17.59

    @internal_parameter()
    def sigma_ggF_8_SM(self):
        '''SM Gluon gluon fusion Higgs production cross section @ 8 TeV in pb'''
        return 7.357

    @internal_parameter()
    def sigma_ggjH_13_SM(self):
        '''SM Gluon gluon fusion Higgs production + 1 jet cross section @ 13 TeV in pb'''
        # return 14.92
        # recomputed with SMEFTatNLO (|eta_H| < 2.5, OK for ratio)
        return 9.328

    @internal_parameter()
    def sigma_ggjH_13_ptjG450_SM(self):
        '''
        SM Gluon gluon fusion Higgs production + 1 jet cross section @ 13 TeV in pb.
        Includes a pT(jet) > 450 GeV cut.
        '''
        # return 0.02681
        # recomputed with SMEFTatNLO (|eta_H| < 2.5, OK for ratio)
        return 0.0201

    @internal_parameter()
    def sigma_ttH_13_SM(self):
        '''
        SM top-pair associated Higgs production cross section @ 13 TeV in pb
        (fixed scale=236 GeV)
        '''
        # return 0.4004 # default MG5 dynamical scale
        return 0.4401

    @internal_parameter()
    def sigma_ttH_8_SM(self):
        '''
        SM top-pair associated Higgs production cross section @ 8 TeV in pb
        (fixed scale=236 GeV)
        '''
        # return 0.1093 # default MG5 dynamical scale
        return 0.1203

    @internal_parameter()
    def sigma_WH_13_SM(self):
        '''SM W associated Higgs production cross section @ 13 TeV in pb'''
        return 1.03

    @internal_parameter()
    def sigma_WH_8_SM(self):
        '''SM W associated Higgs production cross section @ 8 TeV in pb'''
        return 0.5273

    @internal_parameter()
    def sigma_ZH_13_SM(self):
        '''SM Z associated Higgs production cross section @ 13 TeV in pb'''
        return 0.5447

    @internal_parameter()
    def sigma_ZH_8_SM(self):
        '''SM Z associated Higgs production cross section @ 8 TeV in pb'''
        return 0.2748
    
    @internal_parameter()
    def sigma_VH_13_SM(self):
        '''SM Z+W associated Higgs production cross section @ 13 TeV in pb'''
        return 0.5447 + 1.03

    @internal_parameter()
    def sigma_VH_8_SM(self):
        '''SM Z+W associated Higgs production cross section @ 8 TeV in pb'''
        return 0.2748 + 0.5273

    @internal_parameter()
    def sigma_VBF_13_SM(self):
        '''
        SM Vector boson fusion Higgs production cross section @ 13 TeV in pb.
        Includes cuts on mmjj > 130 GeV & deltaeta > 3.
        '''
        return 1.959

    @internal_parameter()
    def sigma_VBF_8_SM(self):
        '''
        SM Vector boson fusion Higgs production cross section @ 8 TeV in pb.
        Includes cuts on mmjj > 130 GeV & deltaeta > 3.
        '''
        return 0.8197

    @internal_parameter(depends_on=(
      'sigma_ggF_13_SM','sigma_VBF_13_SM'
    ))
    def sigma_ggF_VBF_13_SM(self):
        '''SM ggF+VBF Higgs production  @ 13 TeV in pb'''
        return (
          self['sigma_ggF_13_SM'] + self['sigma_VBF_13_SM']
        )

    @internal_parameter(depends_on=(
      'sigma_ggF_8_SM','sigma_VBF_8_SM'
    ))
    def sigma_ggF_VBF_8_SM(self):
        '''SM ggF+VBF Higgs production  @ 8 TeV in pb'''
        return (
          self['sigma_ggF_8_SM'] + self['sigma_VBF_8_SM']
        )

    @internal_parameter(depends_on=(
      'sigma_ggF_13_SM','sigma_ttH_13_SM'
    ))
    def sigma_ggF_ttH_13_SM(self):
        '''SM total Higgs production  @ 13 TeV in pb'''
        return (
          self['sigma_ggF_13_SM'] + self['sigma_ttH_13_SM']
        )
    
    @internal_parameter(depends_on=(
      'sigma_ggF_8_SM','sigma_ttH_8_SM'
    ))
    def sigma_ggF_ttH_8_SM(self):
        '''SM total Higgs production  @ 8 TeV in pb'''
        return (
          self['sigma_ggF_8_SM'] + self['sigma_ttH_8_SM']
        )

    @internal_parameter(depends_on=(
      'sigma_VBF_13_SM','sigma_VH_13_SM'
    ))
    def sigma_VBF_VH_13_SM(self):
        '''SM total Higgs production  @ 13 TeV in pb'''
        return (
         self['sigma_VBF_13_SM'] + self['sigma_VH_13_SM']
        )
    
    @internal_parameter(depends_on=(
      'sigma_VBF_8_SM','sigma_VH_8_SM'
    ))
    def sigma_VBF_VH_8_SM(self):
        '''SM total Higgs production  @ 8 TeV in pb'''
        return (
         self['sigma_VBF_8_SM'] + self['sigma_VH_8_SM']
        )

    @internal_parameter(depends_on=(
      'sigma_ggF_13_SM','sigma_VBF_13_SM','sigma_WH_13_SM','sigma_ZH_13_SM','sigma_ttH_13_SM'
    ))
    def sigma_13_SM(self):
        '''SM total Higgs production  @ 13 TeV in pb'''
        return (
          self['sigma_ggF_13_SM'] + self['sigma_VBF_13_SM'] + self['sigma_WH_13_SM']
        + self['sigma_ZH_13_SM'] + self['sigma_ttH_13_SM']
        )

    @internal_parameter(depends_on=(
      'sigma_ggF_8_SM','sigma_VBF_8_SM','sigma_WH_8_SM','sigma_ZH_8_SM','sigma_ttH_8_SM'
    ))
    def sigma_8_SM(self):
        '''SM total Higgs production  @ 8 TeV in pb'''
        return (
          self['sigma_ggF_8_SM'] + self['sigma_VBF_8_SM'] + self['sigma_WH_8_SM']
        + self['sigma_ZH_8_SM'] + self['sigma_ttH_8_SM']
        )

    @internal_parameter()
    def sigma_VBF_13_ptjL200_SM(self):
        '''
        SM Vector boson fusion Higgs production cross section @ 8 TeV in pb.
        Includes cuts on mmjj > 130 GeV, deltaeta > 3 & pT(jet) < 200 GeV.
        '''
        return 1.857

    @internal_parameter()
    def sigma_VBF_13_ptjG200_SM(self):
        '''
        SM Vector boson fusion Higgs production cross section @ 8 TeV in pb.
        Includes cuts on mmjj > 130 GeV, deltaeta > 3 & pT(jet) > 200 GeV.
        '''
        return 0.1178

    @internal_parameter()
    def sigma_ggjjH_13_SM(self):
        '''SM Gluon gluon fusion Higgs production + 2 jet cross section @ 13 TeV in pb'''
        # return 18.2 # why is this larger than ggF?
        # recomputed with SMEFTatNLO (|eta_H| < 2.5, OK for ratio)
        return 4.75

    @internal_parameter()
    def sigma_ggjH_13_pt20T60_SM(self):
        '''
        SM Gluon gluon fusion Higgs production + 1 jet cross section @ 13 TeV in pb.
        Includes cuts: 20 < pT(H) < 60 GeV.
        '''
        # return 10.18
        # recomputed with SMEFTatNLO (|eta_H| < 2.5, OK for ratio)
        return 5.03

    @internal_parameter()
    def sigma_ggjH_13_pt60T120_SM(self):
        '''
        SM Gluon gluon fusion Higgs production + 1 jet cross section @ 13 TeV in pb.
        Includes cuts: 60 < pT(H) < 120 GeV.
        '''
        # return 3.36
        # recomputed with SMEFTatNLO (|eta_H| < 2.5, OK for ratio)
        return 2.89

    @internal_parameter()
    def sigma_ggjH_13_pt120T200_SM(self):
        '''
        SM Gluon gluon fusion Higgs production + 1 jet cross section @ 13 TeV in pb.
        Includes cuts: 120 < pT(H) < 200 GeV.
        '''
        # return 1.028
        # recomputed with SMEFTatNLO (|eta_H| < 2.5, OK for ratio)
        return 0.989


    @internal_parameter()
    def sigma_ggjH_13_ptG200_SM(self):
        '''
        SM Gluon gluon fusion Higgs production + 1 jet cross section @ 13 TeV in pb.
        Includes cuts: pT(H) > 200 GeV.
        '''
        # return 0.4065
        # recomputed with SMEFTatNLO (|eta_H| < 2.5, OK for ratio)
        return 0.419


class Decay(aEW_MZ_GF_5F_Fixed):
    '''
    LO predictions for Higgs partial widths. 2-body results Computed with
    SMEFTsim_A_U35_alphaScheme_UFO_v2_1 with input parameters fixed to match
    aEW_MZ_GF_5F_Fixed. 4-body decays taken from 1906.06949.
    '''
    @internal_parameter()
    def Gam_H_bb_SM(self):
        '''
        SM Partial Higgs width to b-bbar [GeV]. mb=3.237 GeV used to
        approximate QCD running to mh.
        '''
        return 2.563e-3

    @internal_parameter()
    def Gam_H_tata_SM(self):
        '''SM Partial Higgs width to tau+ tau- [GeV].'''
        return 2.589e-4

    @internal_parameter()
    def Gam_H_cc_SM(self):
        '''
        SM Partial Higgs width to c-cbar [GeV]. mc=0.907 GeV used to
        approximate QCD running to mh.
        '''
        return 2.025e-4

    @internal_parameter()
    def Gam_H_ss_SM(self):
        '''SM Partial Higgs width to s-sbar [GeV].'''
        return 2.270e-6

    @internal_parameter()
    def Gam_H_mumu_SM(self):
        '''SM Partial Higgs width to mu+ mu- [GeV].'''
        return 9.166e-7

    @internal_parameter()
    def Gam_H_gg_SM(self):
        '''SM Partial Higgs width to gluon-gluon[GeV].'''
        return 1.958e-4

    @internal_parameter()
    def Gam_H_aa_SM(self):
        '''SM Partial Higgs width to di-photon [GeV].'''
        return 1.031e-5 # our value

    @internal_parameter()
    def Gam_H_Za_SM(self):
        '''SM Partial Higgs width to Z-photon [GeV].'''
        return 6.276e-6

    # 4-body
    @internal_parameter()
    def Gam_H_lplplrlr_SM(self):
        '''SM Partial Higgs width to two different flavor pairs of OSSF leptons. [GeV] (multiplicity 3).'''
        # return 2.3333e-07 # 1906.06949
        return 2.36e-07 # SMEFTsim

    @internal_parameter()
    def Gam_H_vpvpvrvr_SM(self):
        '''SM Partial Higgs width to two different flavor pairs of SF neutrinos. [GeV] (multiplicity 3).'''
        return 1.0000e-06

    @internal_parameter()
    def Gam_H_lplpvrvr_SM(self):
        '''SM Partial Higgs width to different flavor pairs of OSSF lepton and SF neutrino. [GeV] (multiplicity 6).'''
        return 5.0000e-07

    @internal_parameter()
    def Gam_H_upupurur_SM(self):
        '''SM Partial Higgs width to two different flavor pairs of SF up-type quarks. [GeV] (multiplicity 1).'''
        return 3.0000e-06

    @internal_parameter()
    def Gam_H_dpdpdrdr_SM(self):
        '''SM Partial Higgs width to two different flavor pairs of SF down-type quarks. [GeV] (multiplicity 3).'''
        return 5.0000e-06

    @internal_parameter()
    def Gam_H_upupdrdr_SM(self):
        '''SM Partial Higgs width to different flavor pairs of SF up-type and SF down-type quarks. [GeV] (multiplicity 4).'''
        return 4.0000e-06

    @internal_parameter()
    def Gam_H_lluu_SM(self):
        '''SM Partial Higgs width to a lepton and an up-type quark pair. [GeV] (multiplicity 6).'''
        return 8.3333e-07

    @internal_parameter()
    def Gam_H_lldd_SM(self):
        '''SM Partial Higgs width to a lepton and an down-type quark pair. [GeV] (multiplicity 9).'''
        return 1.1111e-06

    @internal_parameter()
    def Gam_H_vvuu_SM(self):
        '''SM Partial Higgs width to a neutrino and an up-type quark pair. [GeV] (multiplicity 6).'''
        return 1.6667e-06

    @internal_parameter()
    def Gam_H_vvdd_SM(self):
        '''SM Partial Higgs width to a lepton and an down-type quark pair. [GeV] (multiplicity 9).'''
        return 2.2222e-06

    @internal_parameter()
    def Gam_H_lplplplp_SM(self):
        '''SM Partial Higgs width to two same flavor pairs of charged leptons. [GeV] (multiplicity 3).'''
        # return 1.3333e-07 # 1906.06949
        return 1.30e-07 # SMEFTsim

    @internal_parameter()
    def Gam_H_vpvpvpvp_SM(self):
        '''SM Partial Higgs width to two same flavor pairs of neutrinos. [GeV] (multiplicity 3).'''
        return 6.6667e-07

    @internal_parameter()
    def Gam_H_upupupup_SM(self):
        '''SM Partial Higgs width to two same flavor pairs of up-type quarks. [GeV] (multiplicity 2).'''
        return 1.5000e-06

    @internal_parameter()
    def Gam_H_dpdpdpdp_SM(self):
        '''SM Partial Higgs width to two same flavor pairs of down-type quarks. [GeV] (multiplicity 3).'''
        return 2.6667e-06

    @internal_parameter()
    def Gam_H_lpvplrvr_SM(self):
        '''SM Partial Higgs width to two different flavor pairs of SF lepton and neutrino. [GeV] (multiplicity 3).'''
        # return 2.0000e-05
        return 2.108e-05

    @internal_parameter()
    def Gam_H_updpurdr_SM(self):
        '''SM Partial Higgs width to two different flavor pairs of SF up- and down-type quark. [GeV] (multiplicity 1).'''
        return 2.0000e-04

    @internal_parameter()
    def Gam_H_lvud_SM(self):
        '''SM Partial Higgs width to a lepton-neutrino and an up-down quark pair. [GeV] (multiplicity 6).'''
        return 6.5000e-05

    @internal_parameter()
    def Gam_H_upupdpdp_SM(self):
        '''SM Partial Higgs width to same flavor pairs of SF up-type and SF down-type quarks. [GeV] (multiplicity 2).'''
        return 1.0000e-04

    @internal_parameter()
    def Gam_H_lplpvpvp_SM(self):
        '''SM Partial Higgs width to same flavor pairs of OSSF lepton and SF neutrino. [GeV] (multiplicity 3).'''
        # return 1.0000e-05
        return 1.049e-05

    @internal_parameter(depends_on=('Gam_H_vpvpvpvp_SM', 'Gam_H_vpvpvrvr_SM'))
    def Gam_H_4v_SM(self):
        '''SM Partial Higgs width to four neutrinos [GeV].'''
        return 3.*self['Gam_H_vpvpvpvp_SM'] + 3.*self['Gam_H_vpvpvrvr_SM']

    @internal_parameter(depends_on=('Gam_H_lplplplp_SM','Gam_H_lplplrlr_SM'))
    def Gam_H_4l_SM(self):
        '''SM Partial Higgs width to four light leptons (not taus)[GeV].'''
        return 2.*self['Gam_H_lplplplp_SM'] + self['Gam_H_lplplrlr_SM']

    @internal_parameter(depends_on=('Gam_H_lplpvrvr_SM', 'Gam_H_lpvplrvr_SM', 'Gam_H_lplpvpvp_SM'))
    def Gam_H_2l2v_SM(self):
        '''SM Partial Higgs width to 2 light leptons (not taus) & 2 neutrinos [GeV].'''
        return ( 4.*self['Gam_H_lplpvrvr_SM'] + self['Gam_H_lpvplrvr_SM'] +
                 2.*self['Gam_H_lplpvpvp_SM'] )

    @internal_parameter(depends_on=('Gam_H_lpvplrvr_SM', 'Gam_H_lplpvpvp_SM'))
    def Gam_H_WW_2l2v_SM(self):
        '''
        SM Partial Higgs width via charged or charged/neutral currents to
        2 light leptons (not taus) & 2 neutrinos [GeV].
        '''
        return ( self['Gam_H_lpvplrvr_SM'] +  2.*self['Gam_H_lplpvpvp_SM'] )

    @internal_parameter()
    def Gam_H_4f_SM(self):
        '''SM Partial Higgs width to four fermions [GeV] (multiplicity 1).'''
        return 9.8500e-04

    @internal_parameter(depends_on=(
      'Gam_H_bb_SM','Gam_H_tata_SM','Gam_H_cc_SM','Gam_H_gg_SM','Gam_H_aa_SM',
      'Gam_H_Za_SM','Gam_H_ss_SM','Gam_H_mumu_SM','Gam_H_4f_SM'
    ))
    def Gam_H_SM(self):
        '''SM total Higgs width [GeV].'''
        return (
          self['Gam_H_bb_SM'] + self['Gam_H_tata_SM'] + self['Gam_H_cc_SM']
        + self['Gam_H_gg_SM'] + self['Gam_H_aa_SM'] + self['Gam_H_Za_SM']
        + self['Gam_H_ss_SM'] + self['Gam_H_mumu_SM'] + self['Gam_H_4f_SM']
        )

    # LO Higgs decay partial widths
    # # 2 body from trott
    # @internal_parameter()
    # def Gam_H_bb_SM(self):
    #     '''SM Partial Higgs width to b-bbar [GeV].'''
    #     return 2.38e-3
    #
    # @internal_parameter()
    # def Gam_H_tata_SM(self):
    #     '''SM Partial Higgs width to tau+ tau- [GeV].'''
    #     return 2.6e-4
    #
    # @internal_parameter()
    # def Gam_H_cc_SM(self):
    #     '''SM Partial Higgs width to c-cbar [GeV].'''
    #     return 1.2e-4
    #
    # @internal_parameter()
    # def Gam_H_ss_SM(self):
    #     '''SM Partial Higgs width to s-sbar [GeV].'''
    #     return 2.270e-6
    #
    # @internal_parameter()
    # def Gam_H_mumu_SM(self):
    #     '''SM Partial Higgs width to mu+ mu- [GeV].'''
    #     return 9.166e-7
    #
    # @internal_parameter()
    # def Gam_H_gg_SM(self):
    #     '''SM Partial Higgs width to gluon-gluon[GeV].'''
    #     return 3.3e-4
    #
    # @internal_parameter()
    # def Gam_H_aa_SM(self):
    #     '''SM Partial Higgs width to di-photon [GeV].'''
    #     return 9.31e-6
    #
    # @internal_parameter()
    # def Gam_H_Za_SM(self):
    #     '''SM Partial Higgs width to Z-photon [GeV].'''
    #     return 6.32e-6

class Decay_old(EWPO_Tree, aEW_MZ_GF_5F_Fixed):
    # LO Higgs decay partial widths
    @internal_parameter()
    def Gam_H_bb_SM(self):
        '''SM Partial Higgs width to b-bbar [GeV].'''
        return 2.563e-3

    @internal_parameter()
    def Gam_H_tata_SM(self):
        '''SM Partial Higgs width to tau+ tau- [GeV].'''
        return 2.589e-4

    @internal_parameter()
    def Gam_H_cc_SM(self):
        '''SM Partial Higgs width to c-cbar [GeV].'''
        return 2.025e-4

    @internal_parameter()
    def Gam_H_ss_SM(self):
        '''SM Partial Higgs width to s-sbar [GeV].'''
        return 2.270e-6

    @internal_parameter()
    def Gam_H_mumu_SM(self):
        '''SM Partial Higgs width to mu+ mu- [GeV].'''
        return 9.166e-7

    @internal_parameter()
    def Gam_H_Wud_SM(self):
        '''SM Partial Higgs width to W+-(W*->ud)[GeV].'''
        return 2.*1.327e-4

    @internal_parameter()
    def Gam_H_Wcs_SM(self):
        '''SM Partial Higgs width to W+ - (W-* -> c s)[GeV].'''
        return 2.*1.318e-4

    @internal_parameter()
    def Gam_H_Wenu_SM(self):
        '''SM Partial Higgs width to W+ - (W-* -> e- nu)[GeV].'''
        return 2.*4.641e-5

    @internal_parameter()
    def Gam_H_Wmunu_SM(self):
        '''SM Partial Higgs width to W-(W*->mu nu)[GeV].'''
        return 2.*4.624e-5

    @internal_parameter()
    def Gam_H_Wtanu_SM(self):
        '''SM Partial Higgs width to W-(W*->tau nu)[GeV].'''
        return 2.*4.617e-5

    @internal_parameter()
    def Gam_H_Wus_SM(self):
        '''SM Partial Higgs width to W-(W*->us)[GeV].'''
        return 2.*7.167e-6

    @internal_parameter()
    def Gam_H_Wcd_SM(self):
        '''SM Partial Higgs width to W-(W*->cd)[GeV].'''
        return 2.*7.021e-6

    @internal_parameter()
    def Gam_H_Zss_SM(self):
        '''SM Partial Higgs width to Z-(Z*->s-sbar)[GeV].'''
        return 1.285e-5

    @internal_parameter()
    def Gam_H_Zdd_SM(self):
        '''SM Partial Higgs width to Z-(Z*->d-dbar)[GeV].'''
        return 1.278e-5

    @internal_parameter()
    def Gam_H_Zbb_SM(self):
        '''SM Partial Higgs width to Z-(Z*->b-bbar)[GeV].'''
        return 1.072e-5

    @internal_parameter()
    def Gam_H_Zuu_SM(self):
        '''SM Partial Higgs width to Z-(Z*->u-ubar)[GeV].'''
        return 9.960e-6

    @internal_parameter()
    def Gam_H_Zcc_SM(self):
        '''SM Partial Higgs width to Z-(Z*->c-cbar)[GeV].'''
        return 9.736e-6

    @internal_parameter()
    def Gam_H_Znunu_SM(self):
        '''SM Partial Higgs width to Z-(Z*->nu nu)[GeV].'''
        return 3.*5.813e-6
    
    @internal_parameter()
    def Gam_H_Zvv_SM(self):
        '''SM Partial Higgs width to Z-(Z*->nu nu)[GeV].'''
        return 3.*5.813e-6

    @internal_parameter()
    def Gam_H_Zll_SM(self):
        '''SM Partial Higgs width to Z-(Z*->l+ l-), l=e,mu [GeV].'''
        return 5.959e-6
    
    @internal_parameter()
    def Gam_H_Ztata_SM(self):
        '''SM Partial Higgs width to Z-(Z*->ta+ ta-) [GeV].'''
        return 5.959e-6/2.

    @internal_parameter()
    def Gam_H_gg_SM(self):
        '''SM Partial Higgs width to gluon-gluon[GeV].'''
        return 1.958e-4

    @internal_parameter()
    def Gam_H_aa_SM(self):
        '''SM Partial Higgs width to di-photon [GeV].'''
        return 1.031e-5

    @internal_parameter()
    def Gam_H_Za_SM(self):
        '''SM Partial Higgs width to Z-photon [GeV].'''
        return 6.276e-6

    @internal_parameter(depends_on=('Gam_H_Wud_SM', 'Gam_H_Wcs_SM', 'Gam_H_Wcd_SM', 'Gam_H_Wus_SM'))
    def Gam_H_Wqqp_SM(self):
        '''SM Partial Higgs width to W-(W*->q-qbar)[GeV].'''
        return (self['Gam_H_Wud_SM'] + self['Gam_H_Wcs_SM'] +
                self['Gam_H_Wcd_SM'] + self['Gam_H_Wus_SM'] )

    @internal_parameter(depends_on=('Gam_H_Wenu_SM', 'Gam_H_Wmunu_SM'))
    def Gam_H_Wlvl_SM(self):
        '''SM Partial Higgs width to W-(W*->l nu), l=e,mu [GeV].'''
        return self['Gam_H_Wenu_SM'] + self['Gam_H_Wmunu_SM'] 
    
    @internal_parameter(depends_on=('Gam_H_Wlvl_SM',))
    def Gam_H_Wlv_SM(self):
        '''SM Partial Higgs width to W-(W*->l nu), l=e,mu [GeV].'''
        return self['Gam_H_Wlvl_SM']

    @internal_parameter(depends_on=('Gam_H_Zuu_SM', 'Gam_H_Zcc_SM'))
    def Gam_H_Zupup_SM(self):
        '''SM Partial Higgs width to Z-(Z*->up type quarks)[GeV].'''
        return (self['Gam_H_Zuu_SM'] + self['Gam_H_Zcc_SM'])

    @internal_parameter(depends_on=('Gam_H_Zdd_SM', 'Gam_H_Zss_SM', 'Gam_H_Zbb_SM'))
    def Gam_H_Zdodo_SM(self):
        '''SM Partial Higgs width to Z-(Z*->down type quarks)[GeV].'''
        return (self['Gam_H_Zdd_SM'] + self['Gam_H_Zss_SM'] + self['Gam_H_Zbb_SM'])

    @internal_parameter(depends_on=('Gam_H_Wqqp_SM', 'Gam_H_Wlvl_SM','Gam_H_Wtanu_SM'))
    def Gam_H_WW_SM(self):
        '''SM Partial Higgs width to W-(W*->all)[GeV].'''
        return self['Gam_H_Wqqp_SM'] + self['Gam_H_Wlvl_SM'] + self['Gam_H_Wtanu_SM']

    @internal_parameter(depends_on=(
      'Gam_H_Zdodo_SM','Gam_H_Zupup_SM','Gam_H_Znunu_SM','Gam_H_Zll_SM','Gam_H_Ztata_SM'
    ))
    def Gam_H_ZZ_SM(self):
        '''SM Partial Higgs width to Z-(Z->all) [GeV].'''
        return (
          self['Gam_H_Zdodo_SM'] + self['Gam_H_Zupup_SM']
        + self['Gam_H_Znunu_SM'] + self['Gam_H_Zll_SM'] + self['Gam_H_Ztata_SM']
        )
        
    @internal_parameter(depends_on=(
      'Gam_H_Zll_SM','Gam_Z_ll_SM','Gam_Z_SM'
    ))
    def Gam_H_4l_SM(self):
        '''SM partial width to 4 light leptons (no taus) in NWA for Z boson [GeV].'''
        return self['Gam_H_Zll_SM']*(2.*self['Gam_Z_ll_SM']/self['Gam_Z_SM'])

    @internal_parameter(depends_on=('Gam_H_Wlvl_SM',))
    def Gam_H_WW_2l2v_SM(self):
        '''SM partial width to 2 light leptons & 2 neutrinos (no taus) in NWA for W-boson [GeV].'''
        return self['Gam_H_Wlvl_SM']*(2./9.)

    @internal_parameter(depends_on=(
      'Gam_H_bb_SM','Gam_H_tata_SM','Gam_H_cc_SM','Gam_H_gg_SM','Gam_H_WW_SM',
      'Gam_H_ZZ_SM','Gam_H_aa_SM','Gam_H_Za_SM','Gam_H_ss_SM','Gam_H_mumu_SM'
    ))
    def Gam_H_SM(self):
        '''SM total Higgs width [GeV].'''
        return (
          self['Gam_H_bb_SM'] + self['Gam_H_tata_SM'] + self['Gam_H_cc_SM']
        + self['Gam_H_gg_SM'] + self['Gam_H_WW_SM'] + self['Gam_H_ZZ_SM']
        + self['Gam_H_aa_SM'] + self['Gam_H_Za_SM'] + self['Gam_H_ss_SM']
        + self['Gam_H_mumu_SM']
        )
