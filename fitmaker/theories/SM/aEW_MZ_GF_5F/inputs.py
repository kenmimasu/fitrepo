import sys
import numpy as np
from numpy import sqrt
from ....fitlib.theory import TheoryBase, internal_parameter
from ....fitlib.constants import pi, GeV2pb 

class aEW_MZ_GF_5F(TheoryBase):
    '''SM inputs and internal parameters.'''
    external_parameters = ['aEWM1', 'GF', 'MZ', 'aS', 'MT', 'MH']
    
    # Internal parameters
    @internal_parameter(depends_on=('GF',))
    def vev(self):
        '''Higgs vev.'''
        return sqrt( 1./(sqrt(2.)*self['GF']))
    
    @internal_parameter(depends_on=('aEWM1',))
    def aEW(self):
        '''QED coupling constant.'''
        return 1./self['aEWM1']
        
    @internal_parameter(depends_on=('aEW',))
    def ee(self):
        '''QED coupling constant.'''
        return sqrt(4.*pi*self['aEW'])
    
    @internal_parameter(depends_on=('aS',))
    def gs(self):
        '''QCD coupling constant.'''
        return sqrt(4.*pi*self['aS'])
    
    @internal_parameter(depends_on=('ee', 'vev', 'MZ'))
    def s2w(self):
        '''Sine of twice the Weinberg angle.'''
        return (self['ee']*self['vev']/self['MZ'])
    
    @internal_parameter(depends_on=('s2w',))
    def c2w(self):
        '''Cosine of twice the Weinberg angle.'''
        return sqrt(1. - self['s2w']**2)
    
    @internal_parameter(depends_on=('s2w',))
    def cw(self):
        '''Cosine of the Weinberg angle.'''
        return sqrt(1. + sqrt(1. - self['s2w']**2))/sqrt(2.)
    
    @internal_parameter(depends_on=('cw',))
    def sw(self):
        '''Sine of the Weinberg angle.'''
        return sqrt(1.-self['cw']**2)
    
    @internal_parameter(depends_on=('ee', 'vev', 'sw'))
    def MW(self):
        '''W-boson mass'''
        return self['ee']*self['vev']/(2.*self['sw'])
    
    @internal_parameter(depends_on=('ee', 'sw'))
    def gw(self):
        '''SU(2) gauge coupling.'''
        return self['ee']/self['sw']
    
    @internal_parameter(depends_on=('ee', 'cw'))
    def gp(self):
        '''U(1) hypercharge gauge coupling.'''
        return self['ee']/self['cw']
    
    @internal_parameter(depends_on=('MH', 'vev'))
    def lam(self):
        '''Higgs quartic coupling.'''
        return (self['MH']/self['vev'])**2/2.
    
    @internal_parameter()
    def MB(self):
        '''b-quark mass'''
        return 0.

class aEW_MZ_GF_5F_Fixed(aEW_MZ_GF_5F):
    '''Version of aEW_MZ_GF_5F with fixed values'''
    @internal_parameter()
    def aEWM1(self):
        '''Inverse EW fine structure constant.'''
        return 127.95
    
    @internal_parameter()
    def GF(self):
        '''Fermi constant.'''
        return 1.1663787e-5
    
    @internal_parameter()
    def MZ(self):
        '''Z mass.'''
        return 91.1876
    
    @internal_parameter()
    def aS(self):
        '''QCD fine structure constant at the Z mass.'''
        return 0.118
    
    @internal_parameter()
    def MT(self):
        '''top mass.'''
        return 173.21

    @internal_parameter()
    def MH(self):
        '''Higgs mass.'''
        return 125.09
    