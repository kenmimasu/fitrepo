import numpy as np
from numpy import sqrt

from ....fitlib.constants import pi, GeV2pb
from ....fitlib.theory import internal_parameter, prediction

from .inputs import aEW_MZ_GF_5F_Fixed

class LEP(aEW_MZ_GF_5F_Fixed):
    '''
    SM in the (aEW, MZ, GF) scheme with fixed values and massless b with
    predictions for Diboson production at LEP & LHC
    '''
    @prediction('WW_costheta_LEPII')
    def WW_costheta_LEPII(self):
        '''
        W+ W- cos(theta) differential cross section [pb] at LEP II.
        bins 1,4,7 & 10 for E=182.66 & 205.92.
        '''
        return np.array([0.74, 1.20, 2.16, 5.47, 0.52, 0.98, 2.06, 7.80])

    @prediction('WW_xs_lvlv_L3')
    def WW_xs_lvlv_L3(self):
        '''
        W+ W- total cross section measurements at L3 in the lvlv final state for 8 energies. Theory error = 0.5%
        '''
        return np.array([1.72, 1.76, 1.79, 1.80, 1.81, 1.82, 1.82, 1.82])

    @prediction('WW_xs_lvqq_L3')
    def WW_xs_lvqq_L3(self):
        '''
        W+ W- total cross section measurements at L3 in the lvqq final state for 8 energies. Theory error = 0.5%
        '''
        return np.array([7.14, 7.26, 7.38, 7.44, 7.47, 7.50, 7.50, 7.50])

    @prediction('WW_xs_qqqq_L3')
    def WW_xs_qqqq_L3(self):
        '''
        W+ W- total cross section measurements at L3 in the qqqq final state for 8 energies. Theory error = 0.5%
        '''
        return np.array([7.42, 7.56, 7.68, 7.76, 7.79, 7.81, 7.82, 7.82])

    @prediction('WW_xs_lvlv_OPAL')
    def WW_xs_lvlv_OPAL(self):
        '''
        W+ W- total cross section measurements at OPAL in the lvlv final state for 7 energies. Theory error = 0.5%
        '''
        return np.array([1.72, 1.75, 1.78, 1.79, 1.80, 1.81, 1.81])

    @prediction('WW_xs_lvqq_OPAL')
    def WW_xs_lvqq_OPAL(self):
        '''
        W+ W- total cross section measurements at OPAL in the lvqq final state for 7 energies. Theory error = 0.5%
        '''
        return np.array([7.13, 7.26, 7.38, 7.46, 7.48, 7.50, 7.51])

    @prediction('WW_xs_qqqq_OPAL')
    def WW_xs_qqqq_OPAL(self):
        '''
        W+ W- total cross section measurements at OPAL in the qqqq final state for 7 energies. Theory error = 0.5%
        '''
        return np.array([7.41, 7.54, 7.67, 7.75, 7.77, 7.79, 7.80])

    @prediction('WW_xs_lvlv_ALEPH')
    def WW_xs_lvlv_ALEPH(self):
        '''
        W+ W- total cross section measurements at ALEPH in the lvlv final state for 7 energies. Theory error = 0.5%
        '''
        return np.array([1.75, 1.78, 1.79, 1.80, 1.81, 1.81, 1.81])

    @prediction('WW_xs_lvqq_ALEPH')
    def WW_xs_lvqq_ALEPH(self):
        '''
        W+ W- total cross section measurements at ALEPH in the lvqq final state for 7 energies. Theory error = 0.5%
        '''
        return np.array([7.26, 7.38, 7.46, 7.48, 7.50, 7.51, 7.51])

    @prediction('WW_xs_qqqq_ALEPH')
    def WW_xs_qqqq_ALEPH(self):
        '''
        W+ W- total cross section measurements at ALEPH in the qqqq final state for 7 energies. Theory error = 0.5%
        '''
        return np.array([7.54, 7.67, 7.75, 7.77, 7.79, 7.80, 7.80])

class LHC(aEW_MZ_GF_5F_Fixed):
    @internal_parameter()
    def sigma_pp_WW_enumunu_ptl1G120_13(self):
        '''
        W+ W- cross section measurements at at the LHC with pt
        '''
        return 0.02535

class Diboson(LEP, LHC):
    '''Group together LEP & LHC diboson'''
    pass
