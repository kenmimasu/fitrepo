import numpy as np
from numpy import sqrt

from ....fitlib.constants import pi, GeV2pb 
from ....fitlib.theory import internal_parameter, prediction

from .inputs import MW_MZ_GF_5F_Fixed

class LEP(MW_MZ_GF_5F_Fixed):
    '''
    Diboson LEP predictions, taken to be the same as aEW_MZ_GF_5F, 
    neglects EW input scheme difference.
    '''
    @prediction('WW_costheta_LEPII')
    def WW_costheta_LEPII(self):
        '''
        W+ W- cos(theta) differential cross section [pb] at LEP II.
        bins 1,4,7 & 10 for E=182.66 & 205.92.
        '''
        return np.array([0.74, 1.20, 2.16, 5.47, 0.52, 0.98, 2.06, 7.80])
    
    @prediction('WW_costheta_182GeV_LEPII')
    def WW_costheta_182GeV_LEPII(self):
        '''
        W+ W- cos(theta) differential cross section [pb] at LEP II for 
        E=182.66 GeV, obtained from hep-ex/0409016.
        '''
        # return np.array([ 0.74, 0.84, 1.02, 1.20, 1.44, 1.78, 2.16, 2.86, 3.84, 5.47 ])
        # SMEFiT 'best' numbers, unknown origin...
        return np.array([  0.71512, 0.83721, 1.00581, 1.18605, 1.4186, 1.73837, 2.1686, 2.80233, 3.77907, 5.39535  ])
    
    @prediction('WW_costheta_189GeV_LEPII')
    def WW_costheta_189GeV_LEPII(self):
        '''
        W+ W- cos(theta) differential cross section [pb] at LEP II for 
        E=189.0 GeV, obtained from hep-ex/0409016.
        '''
        # return np.array([ 0.64, 0.78, 0.94, 1.14, 1.38, 1.72, 2.22, 2.95, 4.15, 6.24 ])
        # SMEFiT 'best' numbers, unknown origin...
        return np.array([  0.69942, 0.82081, 0.98266, 1.16763, 1.39884, 1.71676, 2.14451, 2.76301, 3.74566, 5.34682   ])
    
    @prediction('WW_costheta_198GeV_LEPII')
    def WW_costheta_198GeV_LEPII(self):
        '''
        W+ W- cos(theta) differential cross section [pb] at LEP II for 
        E=198.3 GeV, obtained from hep-ex/0409016.
        '''
        # return np.array([ 0.57, 0.71, 0.85, 1.05, 1.29, 1.65, 2.16, 2.97, 4.38, 7.20 ])
        # SMEFiT 'best' numbers, unknown origin...
        return np.array([  0.57391, 0.70725, 0.85217, 1.04928, 1.29275, 1.65797, 2.14493, 2.96812, 4.36522, 7.15362   ])
    
    @prediction('WW_costheta_206GeV_LEPII')
    def WW_costheta_206GeV_LEPII(self):
        '''
        W+ W- cos(theta) differential cross section [pb] at LEP II for 
        E=205.9 GeV, obtained from hep-ex/0409016.
        '''
        # return np.array([ 0.52, 0.64, 0.78, 0.98, 1.21, 1.55, 2.06, 2.92, 4.45, 7.80 ])
        # SMEFiT 'best' numbers, unknown origin...
        return np.array([  0.53644, 0.62974, 0.79883, 0.97959, 1.22449, 1.58601, 2.09329, 2.92711, 4.46647, 7.77843   ])
        
    @prediction('WW_xs_lvlv_L3')
    def WW_xs_lvlv_L3(self):
        '''
        W+ W- total cross section measurements at L3 in the lvlv final state for 8 energies. Theory error = 0.5%
        '''
        return np.array([1.72, 1.76, 1.79, 1.80, 1.81, 1.82, 1.82, 1.82])

    @prediction('WW_xs_lvqq_L3')
    def WW_xs_lvqq_L3(self):
        '''
        W+ W- total cross section measurements at L3 in the lvqq final state for 8 energies. Theory error = 0.5%
        '''
        return np.array([7.14, 7.26, 7.38, 7.44, 7.47, 7.50, 7.50, 7.50])
    
    @prediction('WW_xs_qqqq_L3')
    def WW_xs_qqqq_L3(self):
        '''
        W+ W- total cross section measurements at L3 in the qqqq final state for 8 energies. Theory error = 0.5%
        '''
        return np.array([7.42, 7.56, 7.68, 7.76, 7.79, 7.81, 7.82, 7.82])
    
    @prediction('WW_xs_lvlv_OPAL')
    def WW_xs_lvlv_OPAL(self):
        '''
        W+ W- total cross section measurements at OPAL in the lvlv final state for 7 energies. Theory error = 0.5%
        '''
        return np.array([1.72, 1.75, 1.78, 1.79, 1.80, 1.81, 1.81])
    
    @prediction('WW_xs_lvqq_OPAL')
    def WW_xs_lvqq_OPAL(self):
        '''
        W+ W- total cross section measurements at OPAL in the lvqq final state for 7 energies. Theory error = 0.5%
        '''
        return np.array([7.13, 7.26, 7.38, 7.46, 7.48, 7.50, 7.51])
    
    @prediction('WW_xs_qqqq_OPAL')
    def WW_xs_qqqq_OPAL(self):
        '''
        W+ W- total cross section measurements at OPAL in the qqqq final state for 7 energies. Theory error = 0.5%
        '''
        return np.array([7.41, 7.54, 7.67, 7.75, 7.77, 7.79, 7.80])

    @prediction('WW_xs_lvlv_ALEPH')
    def WW_xs_lvlv_ALEPH(self):
        '''
        W+ W- total cross section measurements at ALEPH in the lvlv final state for 7 energies. Theory error = 0.5%
        '''
        return np.array([1.75, 1.78, 1.79, 1.80, 1.81, 1.81, 1.81])
    
    @prediction('WW_xs_lvqq_ALEPH')
    def WW_xs_lvqq_ALEPH(self):
        '''
        W+ W- total cross section measurements at ALEPH in the lvqq final state for 7 energies. Theory error = 0.5%
        '''
        return np.array([7.26, 7.38, 7.46, 7.48, 7.50, 7.51, 7.51])
    
    @prediction('WW_xs_qqqq_ALEPH')
    def WW_xs_qqqq_ALEPH(self):
        '''
        W+ W- total cross section measurements at ALEPH in the qqqq final state for 7 energies. Theory error = 0.5%
        '''
        return np.array([7.54, 7.67, 7.75, 7.77, 7.79, 7.80, 7.80])

class Diboson(LEP):
    '''Group together LEP & LHC diboson'''
    pass