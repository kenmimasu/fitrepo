import numpy as np
from numpy import sqrt
from ....fitlib.constants import pi, GeV2pb 
from ....fitlib.theory import internal_parameter, prediction
from .inputs import MW_MZ_GF_5F, MW_MZ_GF_5F_Fixed

class Tree(MW_MZ_GF_5F):
    '''SM in the (MW, MZ, GF) scheme and massless b with tree-level expressions for EWPO'''
    
    @internal_parameter(depends_on=('sw',))
    def gVlSM(self):
        '''Vector coupling of Z to leptons.'''
        return -1./4.+self['sw']**2
    
    @internal_parameter(depends_on=('sw',))
    def gAlSM(self):
        '''Axial coupling of Z to leptons.'''
        return -1./4.
    
    @internal_parameter()
    def gVvSM(self):
        '''Vector coupling of Z to neutrinos.'''
        return 1./4.
    
    @internal_parameter()
    def gAvSM(self):
        '''Axial coupling of Z to neutrinos.'''
        return 1./4.
    
    @internal_parameter(depends_on=('sw',))
    def gVdSM(self):
        '''Vector coupling of Z to down-type quarks.'''
        return -1./4.+(1./3.)*self['sw']**2
    
    @internal_parameter()
    def gAdSM(self):
        '''Axial coupling of Z to down-type quarks.'''
        return -1./4.
    
    @internal_parameter(depends_on=('sw',))
    def gVuSM(self):
        '''Vector coupling of Z to up-type quarks.'''
        return 1./4.-(2./3.)*self['sw']**2
    
    @internal_parameter()
    def gAuSM(self):
        '''Axial coupling of Z to up-type quarks.'''
        return 1./4.
    
    @internal_parameter(depends_on=('ee','cw','sw'))
    def gZWWSM(self):
        '''Z-W-W coupling.'''
        return self['ee']*self['cw']/self['sw']
    
    @internal_parameter(depends_on=('ee',))
    def gAWWSM(self):
        '''photon-W-W coupling.'''
        return self['ee']

    # Tree-level predictions & pseudo-observables
    @internal_parameter(depends_on=('GF','MZ'))
    def Gam_Z_fac(self):
        '''Prefactor for partial widths [GeV].'''
        return sqrt(2.)/pi*self['GF']*self['MZ']**3
   
    #NOTE: rename all these as e.g. Gam_Z_ll_SMtree instead of just _SM? 
    @internal_parameter(depends_on=('Gam_Z_fac','gVlSM','gAlSM'))
    def Gam_Z_ll_SM(self):
        '''Partial Z-width to leptons [GeV].'''
        return self['Gam_Z_fac']*(self['gVlSM']**2+self['gAlSM']**2)/3.
    
    @internal_parameter(depends_on=('Gam_Z_fac','gVvSM','gAvSM'))
    def Gam_Z_vv_SM(self):
        '''Partial Z-width to neutrinos [GeV].'''
        return self['Gam_Z_fac']*(self['gVvSM']**2+self['gAvSM']**2)/3.
    
    @internal_parameter(depends_on=('Gam_Z_fac','gVuSM','gAuSM'))
    def Gam_Z_uu_SM(self):
        '''Partial Z-width to massless up-type quark [GeV].'''
        return self['Gam_Z_fac']*(self['gVuSM']**2+self['gAuSM']**2)
    
    @internal_parameter(depends_on=('Gam_Z_fac','gVdSM','gAdSM'))
    def Gam_Z_dd_SM(self):
        '''Partial Z-width to massless down-type quark [GeV].'''
        return self['Gam_Z_fac']*(self['gVdSM']**2+self['gAdSM']**2)
    
    @internal_parameter(depends_on=('Gam_Z_uu_SM','Gam_Z_dd_SM'))
    def Gam_Z_had_SM(self):
        '''Partial Z-width to hadrons [GeV].'''
        return 2.*self['Gam_Z_uu_SM'] + 3.*self['Gam_Z_dd_SM']
    
    # Z/W widths
    @internal_parameter(depends_on=('Gam_Z_ll_SM','Gam_Z_vv_SM','Gam_Z_had_SM'))
    def Gam_Z_SM(self):
        '''Z-width [GeV].'''
        return 3.*self['Gam_Z_ll_SM'] + 3.*self['Gam_Z_vv_SM'] + self['Gam_Z_had_SM']
    
    @internal_parameter(depends_on=('GF','MW'))
    def Gam_W_fac(self):
        '''Prefactor for W partial widths [GeV].'''
        return sqrt(2.)/(12.*pi)*self['GF']*self['MW']**3
    
    @internal_parameter(depends_on=('Gam_W_fac',))
    def Gam_W_lv_SM(self):
        '''W partial width to l,v [GeV].'''
        return self['Gam_W_fac']
    
    @internal_parameter(depends_on=('Gam_W_fac',))
    def Gam_W_qq_SM(self):
        '''W partial width to q,q' [GeV].'''
        return 3.*self['Gam_W_fac']
        
    @internal_parameter(depends_on=('Gam_W_lv_SM','Gam_W_qq_SM'))
    def Gam_W_SM(self):
        '''W-width [GeV].'''
        return 3.*self['Gam_W_lv_SM'] + 2.*self['Gam_W_qq_SM']

    # LEP Z-pole observables    
    def sigma_had_0_Tree(self):
        '''Hadronic cross section [pb].'''
        return GeV2pb*12.*pi/self['MZ']**2*(
          self['Gam_Z_ll_SM']*self['Gam_Z_had_SM']/self['Gam_Z_SM']**2
        )
    
    def Rl_0_SM(self):
        '''Hadronic to leptonic cross section ratio.'''
        return self['Gam_Z_had_SM']/self['Gam_Z_ll_SM']

    def Rb_0_SM(self):
        '''b-quark to hadronic cross section ratio.'''
        return self['Gam_Z_dd_SM']/self['Gam_Z_had_SM']
    
    def Rc_0_SM(self):
        '''c-quark to hadronic cross section ratio.'''
        return self['Gam_Z_uu_SM']/self['Gam_Z_had_SM']
        
    def Al_SM(self):
        '''Lepton chiral coupling parameter.'''
        return 2.*self['gVlSM']*self['gAlSM']/(self['gVlSM']**2+self['gAlSM']**2)
    
    def Ab_SM(self):
        '''b-quark chiral coupling parameter.'''
        return 2.*self['gVdSM']*self['gAdSM']/(self['gVdSM']**2+self['gAdSM']**2)
    
    def Ac_SM(self):
        '''c-quark chiral coupling parameter.'''
        return 2.*self['gVuSM']*self['gAuSM']/(self['gVuSM']**2+self['gAuSM']**2)
            
    def AFB_l_SM(self):
        '''Leptonic forward-backward asymmetry.'''
        return (3./4.)*self.Al_SM()**2
    
    def AFB_b_SM(self):
        '''b-quark forward-backward asymmetry.'''
        return (3./4.)*self.Al_SM()*self.Ab_SM()
    
    def AFB_c_SM(self):
        '''c-quark forward-backward asymmetry.'''
        return (3./4.)*self.Al_SM()*self.Ac_SM()

class Tree_Fixed(MW_MZ_GF_5F_Fixed, Tree):
    '''
    SM in the (MW, MZ, GF) scheme and massless b with tree-level expressions 
    for EWPO & fixed input parameter values.'''
    pass
    
class LEP_EWWG(MW_MZ_GF_5F_Fixed):
    '''
    SM in the (MW, MZ, GF) scheme with fixed values and massless b with 
    numerical expressions for EWPO taken from 2102.02819
    '''
    # LEP Z-pole observables
    @prediction('Gam_Z')
    def Gam_Z(self):
        '''Z-width [GeV].'''
        return 2.4957
    
    @prediction('sigma_had_0')
    def sigma_had_0(self):
        '''Hadronic cross section [nb].'''
        return 41.489
    
    @prediction('Rl_0')
    def Rl_0(self):
        '''Hadronic to leptonic cross section ratio.'''
        return 20.758

    @prediction('Rb_0')
    def Rb_0(self):
        '''b-quark to hadronic cross section ratio.'''
        return 0.21586
    
    @prediction('Rc_0')
    def Rc_0(self):
        '''c-quark to hadronic cross section ratio.'''
        return 0.17223
    
    @prediction('Al')
    def Al_Ptau(self):
        '''Leptonic chiral coupling.'''
        return 0.151349
    
    @prediction('Ab')
    def Ab(self):
        '''b-quark chiral coupling parameter.'''
        return 0.935142
    
    @prediction('Ac')
    def Ac(self):
        '''c-quark chiral coupling parameter.'''
        return 0.668034
            
    @prediction('AFB_l')
    def AFB_l(self):
        '''Leptonic forward-backward asymmetry.'''
        return 0.01718
    
    @prediction('AFB_b')
    def AFB_b(self):
        '''b-quark forward-backward asymmetry.'''
        return 0.10615
    
    @prediction('AFB_c')
    def AFB_c(self):
        '''c-quark forward-backward asymmetry.'''
        return 0.07583
    
    @prediction('aEWM1_MZ')
    def aEWM1_MZ(self):
        '''1 over the MSbar EW fine structure constant evaluated at the Z-pole.'''
        return self['aEWM1_MZ_MSbar']
