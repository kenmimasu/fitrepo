import sys
import numpy as np
from numpy import sqrt
from ....fitlib.theory import TheoryBase, internal_parameter
from ....fitlib.constants import pi, GeV2pb 

class MW_MZ_GF_5F(TheoryBase):
    '''SM inputs and internal parameters.'''
    external_parameters = ['MW', 'GF', 'MZ', 'aS', 'MT', 'MH']
    
    # Internal parameters
    @internal_parameter(depends_on=('GF',))
    def vev(self):
        '''Higgs vev.'''
        return sqrt( 1./(sqrt(2.)*self['GF']))

    @internal_parameter(depends_on=('MW','MZ'))
    def cw(self):
        '''Cosine of the Weinberg angle.'''
        return self['MW']/self['MZ']
    
    @internal_parameter(depends_on=('cw',))
    def sw(self):
        '''Sine of the Weinberg angle.'''
        return sqrt(1.-self['cw']**2)

    @internal_parameter(depends_on=('sw','MW','vev'))
    def ee(self):
        '''QED coupling constant.'''
        return 2.*self['MW']*self['sw']/self['vev']
    
    @internal_parameter(depends_on=('ee',))
    def aEW(self):
        '''QED coupling constant.'''
        return self['ee']**2/(4*pi)
    
    @internal_parameter(depends_on=('aS',))
    def gs(self):
        '''QCD coupling constant.'''
        return sqrt(4.*pi*self['aS'])
    
    @internal_parameter(depends_on=('ee', 'sw'))
    def gw(self):
        '''SU(2) gauge coupling.'''
        return self['ee']/self['sw']
    
    @internal_parameter(depends_on=('ee', 'cw'))
    def gp(self):
        '''U(1) hypercharge gauge coupling.'''
        return self['ee']/self['cw']
    
    @internal_parameter(depends_on=('MH', 'vev'))
    def lam(self):
        '''Higgs quartic coupling.'''
        return (self['MH']/self['vev'])**2/2.
    
    @internal_parameter()
    def MB(self):
        '''b-quark mass'''
        return 0.

class MW_MZ_GF_5F_Fixed(MW_MZ_GF_5F):
    '''Version of aEW_MZ_GF_5F with fixed values'''
    @internal_parameter()
    def MW(self):
        '''W mass [GeV].'''
        return 80.387 # +- 0.016
    
    @internal_parameter()
    def GF(self):
        '''Fermi constant [GeV^{-2}].'''
        return 1.1663787e-5
    
    @internal_parameter()
    def MZ(self):
        '''Z mass [GeV].'''
        return 91.1876 # +- 0.0021
    
    @internal_parameter()
    def aS(self):
        '''QCD fine structure constant at the Z mass.'''
        return 0.118 # +- 0.0010
    
    @internal_parameter()
    def MT(self):
        '''top mass [GeV].'''
        return 172.4 # +- 0.7 

    @internal_parameter()
    def MH(self):
        '''Higgs mass [GeV].'''
        return 125.1 # +- 0.14

    @internal_parameter()
    def aEW_0(self):
        '''
        EW fine structure constant in the Thompson limit, 0 momentum transfer. 
        Taken from PDG chatper 10: "Electroweak Model and Constraints on New 
        Physics", Sec. 10.2.2. World average from many measurements.
        '''
        return 1./137.03599084#(21)

    @internal_parameter()
    def dalpha_MZ_OS(self):
        '''
        Net contribution to the running of the EW fine structure constant from 
        q^2=0 to the Z-pole (hadronic & leptonic) in the MW scheme. Obtained by 
        inverting parametric \Delta\alpha(MZ) dependence of the 2-loop EW MW 
        calculation of hep-ph/0311148, as in 2102.02819. Includes errors in 
        determination of input parameters (same values as 2102.02819 used). 
        Slightly different value obtained from 2102.02819.
        '''
        return 0.05732 # +- 0.00095
    
    @internal_parameter(depends_on=('aEW_0', 'dalpha_MZ_OS'))
    def aEW_MZ_OS(self):
        '''EW fine structure constant at the Z mass in the On-Shell scheme. '''
        return self['aEW_0']/(1.- self['dalpha_MZ_OS'])
    
    @internal_parameter(depends_on=('aEW_MZ_OS',))
    def aEWM1_MZ_OS(self):
        '''1 over the EW fine structure constant at the Z mass in the On-Shell scheme.'''
        return 1./self['aEW_MZ_OS']

    @internal_parameter(depends_on=('dalpha_MZ_OS',))
    def dalpha_MZ_MSbar(self):
        '''
        Conversion of dalpha from On-Shell to MSbar scheme taken from PDG 
        chapter 10: "Electroweak Model and Constraints on New Physics". Eqn. 
        10.10 (typo in 2020 version, 2018 version (Eqn. 10.8) correct).
        '''
        return self['dalpha_MZ_OS'] + 0.007127#(2) 
            
    @internal_parameter(depends_on=('aEW_0', 'dalpha_MZ_MSbar'))
    def aEW_MZ_MSbar(self):
        '''EW fine structure constant at the Z mass in the MSbar scheme.'''
        return self['aEW_0']/(1.- self['dalpha_MZ_MSbar'])

    @internal_parameter(depends_on=('aEW_MZ_MSbar',))
    def aEWM1_MZ_MSbar(self):
        '''1 over the EW fine structure constant at the Z mass in the MSbar scheme.'''
        return 1./self['aEW_MZ_MSbar']
    